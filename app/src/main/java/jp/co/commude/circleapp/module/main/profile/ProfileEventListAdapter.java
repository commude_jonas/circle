package jp.co.commude.circleapp.module.main.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.comments.EventCommentsListActivity;
import jp.co.commude.circleapp.module.main.feeds.PublicEventItem;
import jp.co.commude.circleapp.customviews.RoundishImageView;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 21/03/2018.
 */

public class ProfileEventListAdapter extends RecyclerView.Adapter<ProfileEventListAdapter.PublicEventViewHolder>{
    
    Context context;
    ArrayList<PublicEventItem> publicEventItems;
    
    OnLikeListener onLikeListener;
    onLikeButtonListener onLikeButtonListener;
    onSelectEventListener onSelectEventListener;
    
    public ProfileEventListAdapter(Context context, ArrayList<PublicEventItem> publicEventItems) {
        super();
        this.context = context;
        this.publicEventItems = publicEventItems;
        this.onLikeListener = onLikeListener;
        this.onLikeButtonListener = onLikeButtonListener;
        this.onSelectEventListener = onSelectEventListener;
    }
    
    public ProfileEventListAdapter(Context context, ArrayList<PublicEventItem> publicEventItems, onSelectEventListener onSelectEventListener) {
        super();
        this.context = context;
        this.publicEventItems = publicEventItems;
        this.onLikeListener = onLikeListener;
        this.onLikeButtonListener = onLikeButtonListener;
        this.onSelectEventListener = onSelectEventListener;
    }
    
    public class PublicEventViewHolder extends RecyclerView.ViewHolder{
        
        RoundishImageView rivCover;
        TextView tvEventTitle, tvEventHost, tvEventDate, tvEventDesc,tvEventCapacity;
        ImageView ivComment,ivShare,ivJoin;
        FrameLayout cardview;
        LikeButton ivLike;
        
        FrameLayout flHeaderDetails;
        LinearLayout llHeaderDetails;
        TextView headerTextAddress, headerTextDate;
        TextView eventCategory;
        
        public PublicEventViewHolder(View view) {
            super(view);
            cardview = (FrameLayout) view.findViewById(R.id.cardview);
            rivCover = (RoundishImageView) view.findViewById(R.id.rivCover);
            tvEventTitle = (TextView) view.findViewById(R.id.tvEventTitle);
            tvEventHost = (TextView) view.findViewById(R.id.tvEventHost);
            tvEventDate = (TextView) view.findViewById(R.id.tvEventDate);
            tvEventDesc = (TextView) view.findViewById(R.id.tvEventDesc);
            tvEventCapacity = (TextView) view.findViewById(R.id.tvEventCapacity);
        }
        
    }
    
    @Override
    public PublicEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_vertical_event_profile_item,parent,false);
        
        return new ProfileEventListAdapter.PublicEventViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(PublicEventViewHolder holder, int position) {
        PublicEventItem publicEventItem = publicEventItems.get(position);
        
        holder.tvEventTitle.setText(publicEventItem.getEventTitle());
        holder.tvEventHost.setText(publicEventItem.getEventHost());
        holder.tvEventDate.setText(publicEventItem.getEventDate());
        holder.tvEventDesc.setText(publicEventItem.getEventDesc());
        holder.tvEventCapacity.setText(publicEventItem.getEventCapacity());
        
        Glide.with(context)
                .load(publicEventItem.getEventCover())
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .into(holder.rivCover);
        
        applyClickEvent(holder,position);
    }
    
    private void applyClickEvent(final PublicEventViewHolder holder, final int position) {
        
        holder.rivCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectEventListener.onSelectEvent(holder.rivCover,position);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return publicEventItems.size();
    }
    
    private void gotoCommentScreen(){
        
        final Activity activity = (Activity) context;
        Intent intent = new Intent(activity.getApplicationContext(),EventCommentsListActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        
    }
    
    public interface onLikeButtonListener{
        void onLike(boolean status, int position, LikeButton likeButton);
    }
    
    public interface onSelectEventListener{
        void shareEventtoSocial(int position);
        void onSelectCommentToEvent(int position);
        void onSelectEvent(View v, int position);
    }
}

