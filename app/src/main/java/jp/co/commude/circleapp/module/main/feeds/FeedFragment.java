package jp.co.commude.circleapp.module.main.feeds;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.transition.Explode;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.like.LikeButton;
import com.like.OnLikeListener;

import org.threeten.bp.LocalDate;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;
import jp.co.commude.circleapp.module.events.EventDetailsActivity;
import jp.co.commude.circleapp.module.search_feeds.SearchFeedActivity;
import jp.co.commude.circleapp.utility.AnimationUtils;
import jp.co.commude.circleapp.utility.CommonUtils;
import jp.co.commude.circleapp.utility.UIControlsUtils;
import me.everything.android.ui.overscroll.HorizontalOverScrollBounceEffectDecorator;
import me.everything.android.ui.overscroll.adapters.RecyclerViewOverScrollDecorAdapter;
import solar.blaz.date.week.WeekDatePicker;

import static com.daimajia.androidanimations.library.Techniques.SlideInLeft;
import static com.daimajia.androidanimations.library.Techniques.SlideInUp;
import static com.daimajia.androidanimations.library.Techniques.SlideOutDown;
import static com.daimajia.androidanimations.library.Techniques.SlideOutRight;
import static com.daimajia.androidanimations.library.Techniques.SlideOutUp;

/**
 * Created by vic_villanueva on 25/01/2018.
 */

public class FeedFragment extends Fragment
        implements OnLikeListener
        , PublicEventListAdapter.onLikeButtonListener
        , PublicEventListAdapter.onSelectEventListener
        , RecommendedEventListAdapter.OnSelectRecommendedEventListener, View.OnClickListener {
    
    private static final String TAG = "FeedFragment";
    RecyclerView rv_recommendedevent, rv_public_feeds;
    RecommendedEventListAdapter recommendedEventListAdapter;
    PublicEventListAdapter publicEventListAdapter;
    
    ArrayList<RecommendedEventItem> recommendedEventItems;
    ArrayList<PublicEventItem> publicEventItems;
    
    CoordinatorLayout coordinatorLayout;
    NestedScrollView nsvContainer;
    
    View date_line_view1, date_line_view2;
    ImageView ivDatePicker;
    LinearLayout containerDate;
    
    TextView tvDateDayOftheWeek, tvDateDisplay, tvNoOfEventsOnDate;
    
    LinearLayout mContainerRecommendedEvent;
    ImageView ivSearch;
    
    WeekDatePicker weekDatePicker;
    
    public FeedFragment() {
    }
    
    public static FeedFragment newInstance() {
        
        Bundle args = new Bundle();
        
        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feeds, container, false);
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    
        ivSearch = (ImageView) view.findViewById(R.id.ivSearch);
        
        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator);
        nsvContainer = (NestedScrollView) view.findViewById(R.id.nsvContainer);
    
        rv_recommendedevent = (RecyclerView) view.findViewById(R.id.rv_recommended_events);
        rv_public_feeds = (RecyclerView) view.findViewById(R.id.rv_public_feeds);
    
        mContainerRecommendedEvent = (LinearLayout) view.findViewById(R.id.mContainerRecommendedEvent);
    
        ivDatePicker = (ImageView) view.findViewById(R.id.ivDatePicker);
        tvDateDayOftheWeek = (TextView) view.findViewById(R.id.tvDateDayOftheWeek);
        tvDateDisplay = (TextView) view.findViewById(R.id.tvDateDisplay);
        tvNoOfEventsOnDate = (TextView) view.findViewById(R.id.tvEventNo);
        
        weekDatePicker = (WeekDatePicker) view.findViewById(R.id.date_picker);
        
        date_line_view1 = view.findViewById(R.id.date_line_view1);
        date_line_view2 = view.findViewById(R.id.date_line_view2);
        containerDate = (LinearLayout) view.findViewById(R.id.containerDate);
        
        ivSearch.setOnClickListener(this);
    
        tvDateDayOftheWeek.setText(UIControlsUtils.getDateDayOftheWeek(LocalDate.now()));
        tvDateDisplay.setText(UIControlsUtils.getDate(LocalDate.now()));
    
        /*Spannable pinkColorText = new SpannableString(tvNoOfEventsOnDate.getText().toString());
        pinkColorText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorRed0)), 4, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        */
        
        tvNoOfEventsOnDate.setText(UIControlsUtils.setTextSpanNewColor(getActivity(),tvNoOfEventsOnDate.getText().toString(), 4, 6));
    
        ivDatePicker.setOnClickListener(this);
    
        weekDatePicker.setDateIndicator(LocalDate.now(), true);
        weekDatePicker.setLimits(LocalDate.now().minusWeeks(1), null);
        weekDatePicker.selectDay(LocalDate.now());
        weekDatePicker.setOnDateSelectedListener(new WeekDatePicker.OnDateSelected() {
            @Override
            public void onDateSelected(LocalDate date) {
                tvDateDayOftheWeek.setText(UIControlsUtils.getDateDayOftheWeek(date));
                tvDateDisplay.setText(UIControlsUtils.getDate(date));
            }
        });
    
        recommendedEventItems = new ArrayList<RecommendedEventItem>();
        publicEventItems = new ArrayList<PublicEventItem>();
    
        recommendedEventListAdapter = new RecommendedEventListAdapter(getActivity(), recommendedEventItems, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_recommendedevent.setLayoutManager(layoutManager);
        rv_recommendedevent.setItemAnimator(new DefaultItemAnimator());
        rv_recommendedevent.setAdapter(recommendedEventListAdapter);
        recommendedEventListAdapter.notifyDataSetChanged();
    
        addRecords();
    
        publicEventListAdapter = new PublicEventListAdapter(getActivity(), publicEventItems, this, this, this);
    
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_public_feeds.setLayoutManager(layoutManager2);
        rv_public_feeds.setItemAnimator(new DefaultItemAnimator());
        rv_public_feeds.setAdapter(publicEventListAdapter);
        publicEventListAdapter.notifyDataSetChanged();
    
        new HorizontalOverScrollBounceEffectDecorator(new RecyclerViewOverScrollDecorAdapter(rv_recommendedevent));
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivSearch:
    
                //goToSearchView();
                startActivity(new Intent(getActivity(), SearchFeedActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.ivDatePicker:
                
                if (!containerDate.isShown()) {
                    ivDatePicker.setImageResource(R.drawable.ic_selected_calendar);
                    // @mContainerRecommendedEvent, @SlideOutUp
                    AnimationUtils.animationDatePicker2(getActivity(), mContainerRecommendedEvent, SlideOutUp, 0, containerDate, SlideInLeft, 1, 250, 0);
                    // @containerDate, @SlideInLeft
                    // @date_line_view2, @SlideInLeft
                    AnimationUtils.animationDatePicker2(getActivity(), date_line_view2, SlideInLeft, 1, tvNoOfEventsOnDate, SlideInUp, 1, 250, 500);
                    break;
                } else {
                    ivDatePicker.setImageResource(R.drawable.ic_unselected_calendar);
                    //@containerDate, @SlideOutRight
                    AnimationUtils.animationDatePicker2(getActivity(), containerDate, SlideOutRight, 0, tvNoOfEventsOnDate, SlideOutDown, 0, 250, 0);
                    //@mContainerRecommendedEvent, @SlideInLeft
                    //@date_line_view2, @SlideOutRight
                    AnimationUtils.animationDatePicker2(getActivity(), mContainerRecommendedEvent, SlideInLeft, 1, date_line_view2, SlideOutRight, 0, 250, 500);
                    
                    break;
                }
        }
    }
    
    @Override
    public void shareEventtoSocial(int position) {
        CommonUtils.showBottomShareEventToSocial(getActivity(), position);
    }
    /**
     * Event Listener to directly input a comment on the specific event
     */
    
    @Override
    public void onSelectCommentToEvent(int position) {
    
    }
    
    @Override
    public void onSelectEvent(View v, int position) {
        
        PublicEventItem publicEventItem = publicEventItems.get(position);
        Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
    
        View view = v;
        String transitionName = "myCover";
    
        ActivityOptions transiActivityOptions = ActivityOptions.makeSceneTransitionAnimation(getActivity(), view, transitionName);
        
        Explode explode = new Explode();
        explode.excludeTarget(android.R.id.statusBarBackground, true);
        explode.excludeTarget(android.R.id.navigationBarBackground, true);
        explode.excludeTarget(R.id.coordinatorLayout, true);
        explode.excludeTarget(R.id.customtoolbar, true);
        explode.excludeTarget(R.id.ivBackpress, true);
        explode.excludeTarget(R.id.ivComment, true);
        explode.excludeTarget(R.id.ivShare, true);
        explode.excludeTarget(R.id.frameLike, true);
        explode.excludeTarget(R.id.ivLike, true);
        explode.excludeTarget(R.id.otherdetails, true);
        explode.excludeTarget(R.id.tvDate, true);
        explode.excludeTarget(R.id.tvDesc, true);
        explode.excludeTarget(R.id.otherdetails, true);
    
        explode.excludeTarget(R.id.frameLayout2, true);
        explode.excludeTarget(R.id.customtoolbar, true);
        explode.excludeTarget(R.id.tvEventType, true);
        explode.excludeTarget(R.id.eventCard, true);
        explode.excludeTarget(R.id.map_container, true);
        explode.excludeTarget(R.id.tvMapAddress, true);
        explode.excludeTarget(R.id.firstborder, true);
        explode.excludeTarget(R.id.secondborder, true);
        explode.excludeTarget(R.id.thirdborder, true);
        explode.excludeTarget(R.id.rvHorizontalAlbum, true);
        explode.excludeTarget(R.id.rvHorizontalAlbumParticipant, true);
        explode.excludeTarget(R.id.albumcontainer, true);
        explode.excludeTarget(R.id.participantcontainer, true);
        explode.excludeTarget(R.id.commentcontainer, true);
    
        explode.excludeTarget(R.id.creator, true);
        explode.excludeTarget(R.id.ivMessage, true);
        explode.excludeTarget(R.id.tvUsername, true);
        explode.excludeTarget(R.id.tvDescription, true);
        explode.excludeTarget(R.id.ivProfilePicture, true);
        explode.excludeTarget(R.id.album, true);
        explode.excludeTarget(R.id.buttonStart, true);
    
        getActivity().getWindow().setEnterTransition(new Explode());
        getActivity().getWindow().setExitTransition(new Slide());
    
        getActivity().startActivity(intent, transiActivityOptions.toBundle());
    }
    
    /**
     * Event Listener of the lists of Recommended Events
     * Same function as public posted events - Feed Details this must be a dialog fragment style with bounce transition or
     * If it is an activity try not to make it scrollable with parallax effects on Image
     */
    
    @Override
    public void onSelectRecommendedEvent(View v, int position) {
        
        RecommendedEventItem publicEventItem = recommendedEventItems.get(position);
        Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
    
        intent.putExtra("intentImage", position);
        
        View view = v;
        String transitionName = "myCover";
        
        ActivityOptions transiActivityOptions = ActivityOptions.makeSceneTransitionAnimation(getActivity(), view, transitionName);
    
        Explode explode = new Explode();
        explode.excludeTarget(android.R.id.statusBarBackground, true);
        explode.excludeTarget(android.R.id.navigationBarBackground, true);
        explode.excludeTarget(R.id.coordinatorLayout, true);
        explode.excludeTarget(R.id.customtoolbar, true);
        explode.excludeTarget(R.id.ivBackpress, true);
        explode.excludeTarget(R.id.ivComment, true);
        explode.excludeTarget(R.id.ivShare, true);
        explode.excludeTarget(R.id.frameLike, true);
        explode.excludeTarget(R.id.ivLike, true);
        explode.excludeTarget(R.id.otherdetails, true);
        explode.excludeTarget(R.id.tvDate, true);
        explode.excludeTarget(R.id.tvDesc, true);
        explode.excludeTarget(R.id.otherdetails, true);
        
        explode.excludeTarget(R.id.frameLayout2, true);
        explode.excludeTarget(R.id.customtoolbar, true);
        explode.excludeTarget(R.id.tvEventType, true);
        explode.excludeTarget(R.id.eventCard, true);
        explode.excludeTarget(R.id.map_container, true);
        explode.excludeTarget(R.id.tvMapAddress, true);
        explode.excludeTarget(R.id.firstborder, true);
        explode.excludeTarget(R.id.secondborder, true);
        explode.excludeTarget(R.id.thirdborder, true);
        explode.excludeTarget(R.id.rvHorizontalAlbum, true);
        explode.excludeTarget(R.id.rvHorizontalAlbumParticipant, true);
        explode.excludeTarget(R.id.albumcontainer, true);
        explode.excludeTarget(R.id.participantcontainer, true);
        explode.excludeTarget(R.id.commentcontainer, true);
        
        explode.excludeTarget(R.id.creator, true);
        explode.excludeTarget(R.id.ivMessage, true);
        explode.excludeTarget(R.id.tvUsername, true);
        explode.excludeTarget(R.id.tvDescription, true);
        explode.excludeTarget(R.id.ivProfilePicture, true);
        explode.excludeTarget(R.id.album, true);
        explode.excludeTarget(R.id.buttonStart, true);
        
        getActivity().getWindow().setEnterTransition(new Explode());
        getActivity().getWindow().setExitTransition(new Slide());
        
        getActivity().startActivity(intent, transiActivityOptions.toBundle());
        
    }
    
    /**
     * implement like button event listeners with status changes
     */
    
    @Override
    public void liked(LikeButton likeButton) {
        likeButton.setLiked(true);
        publicEventListAdapter.notifyDataSetChanged();
    }
    
    @Override
    public void unLiked(LikeButton likeButton) {
        likeButton.setLiked(false);
        publicEventListAdapter.notifyDataSetChanged();
    }
    
    @Override
    public void onLike(boolean status, int position, LikeButton likeButton) {
        PublicEventItem publicEventItem = publicEventItems.get(position);
        if (publicEventItem.isLike()) {
            publicEventItem.setLike(false);
            likeButton.setLiked(false);
        } else {
            likeButton.setLiked(true);
            publicEventItem.setLike(true);
        }
        publicEventItems.set(position, publicEventItem);
    
        Log.d(TAG, position + " like is " + publicEventItem.isLike());
    }
    /**
     * end of event listener of status like button
     **/
    private void addRecords() {
        Globals.RecommendedEventItems(getActivity(), recommendedEventItems);
        Globals.PublicEventItems(getActivity(), publicEventItems);
    }
}