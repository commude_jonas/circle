package jp.co.commude.circleapp.module.main.notifications;

/**
 * Created by vic_villanueva on 26/02/2018.
 */

public class NotificationItem {
    
    String userName;
    String userDetails;
    String date;
    
    boolean read;
    boolean delete;
    
    int userProfilePic;
    
    public NotificationItem(String userName, String userDetails, String date, boolean read, int userProfilePic) {
        this.userName = userName;
        this.userDetails = userDetails;
        this.date = date;
        this.read = read;
        this.userProfilePic = userProfilePic;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getUserDetails() {
        return userDetails;
    }
    
    public void setUserDetails(String userDetails) {
        this.userDetails = userDetails;
    }
    
    public String getDate() {
        return date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    
    public boolean isRead() {
        return read;
    }
    
    public void setRead(boolean read) {
        this.read = read;
    }
    
    public boolean isDelete() {
        return delete;
    }
    
    public void setDelete(boolean delete) {
        this.delete = delete;
    }
    
    public int getUserProfilePic() {
        return userProfilePic;
    }
    
    public void setUserProfilePic(int userProfilePic) {
        this.userProfilePic = userProfilePic;
    }
}
