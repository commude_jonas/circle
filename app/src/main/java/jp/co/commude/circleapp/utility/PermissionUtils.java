package jp.co.commude.circleapp.utility;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.zip.Inflater;

import jp.co.commude.circleapp.R;

/**
 * Utility class for access to runtime permissions.
 */
public abstract class PermissionUtils {

    /**
     * Requests the fine location permission. If a rationale with an additional explanation should
     * be shown to the user, displays a dialog that triggers the request.
     */
    
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 0;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 1;
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 2;
    public static final int PHONE_CONTACTS_PERMISSION_REQUEST_CODE = 3;
    public static final int MULTIPLE_PERMISSION_REQUEST_CODE = 4;
    public static final int MULTIPLE_PERMISSION_LOCATION_REQUEST_CODE = 5;
    
    public static void checkPermission(Context context, int requestCode, String permission, String message, String error){
        
        if(Utils.hasLollipop()) {
            
            requestPermission((AppCompatActivity) context,
                    requestCode, permission, true,
                    message, error);
            //Toast.makeText(context, "requesting a permission", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(context, "You need to update to atleast an OS higher than Kitkat Android Version", Toast.LENGTH_SHORT).show();
            
        }
        
    }
    public static void checkPermission2(Context context, int requestCode, String permission, String message, String error){
        
        if(Utils.hasLollipop()) {
            
            requestPermission((FragmentActivity) context,
                    requestCode, permission, true,
                    message, error);
            //Toast.makeText(context, "requesting a permission", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(context, "You need to update to atleast an OS higher than Kitkat Android Version", Toast.LENGTH_SHORT).show();
            
        }
        
    }
    public static void checkMultiplePermission(Context context, int requestCode, String message, String error, String[] permission){
        if(Utils.hasLollipop()) {
    
            requestMultiplePermission((AppCompatActivity) context,
                    requestCode, true,
                    message, error, permission);
            ///Toast.makeText(context, "requesting a permission", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(context, "You need to update to atleast an OS higher than Kitkat Android Version", Toast.LENGTH_SHORT).show();
        
        }
    }
    
    public static void requestPermission(AppCompatActivity activity, int requestId,
                                         String permission, boolean finishActivity, String message, String error) {
    
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog.newInstance(message,error, permission,requestId, finishActivity)
                    .show(activity.getSupportFragmentManager(), "dialog");
        } else {
            // Location permission has not been granted yet, request it.
            ActivityCompat.requestPermissions(activity, new String[]{permission}, requestId);
        
        }
        
    }
    
    public static void requestMultiplePermission(AppCompatActivity activity, int requestId,
                                                 boolean finishActivity, String message, String error, String[] permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission[0].toString())
                && ActivityCompat.shouldShowRequestPermissionRationale(activity,permission[1].toString())) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog2.newInstance(message,error, requestId, finishActivity, permission)
                    .show(activity.getSupportFragmentManager(), "dialog");
        } else {
            // Location permission has not been granted yet, request it.
            ActivityCompat.requestPermissions(activity, permission, requestId);
        
        }
    }
    
    public static void requestPermission(FragmentActivity activity, int requestId,
                                         String permission, boolean finishActivity, String message, String error) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog.newInstance(message,error, permission,requestId, finishActivity)
                    .show(activity.getSupportFragmentManager(), "dialog");
        } else {
            // Location permission has not been granted yet, request it.
            ActivityCompat.requestPermissions(activity, new String[]{permission}, requestId);
            
        }
    }
    /**
     * Checks if the result contains a {@link PackageManager#PERMISSION_GRANTED} result for a
     * permission from a runtime permissions request.
     *
     * @see android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback
     */
    public static boolean isPermissionGranted(String[] grantPermissions, int[] grantResults) {
        for (int i = 0; i < grantPermissions.length; i++) {
                return grantResults[i] == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    /**
     * A dialog that displays a permission denied message.
     */
    public static class PermissionDeniedDialog extends DialogFragment {

        private static final String ARGUMENT_FINISH_ACTIVITY = "finish";
        private static final String ARGUMENT_MESSAGE_STRING = "message";

        private boolean mFinishActivity = false;
        private String mMessage = "";
        /**
         * Creates a new instance of this dialog and optionally finishes the calling Activity
         * when the 'Ok' button is clicked.
         */
        public static PermissionDeniedDialog newInstance(String message, boolean finishActivity) {
            Bundle arguments = new Bundle();
            arguments.putString(ARGUMENT_MESSAGE_STRING, message);
            arguments.putBoolean(ARGUMENT_FINISH_ACTIVITY, finishActivity);

            PermissionDeniedDialog dialog = new PermissionDeniedDialog();
            dialog.setArguments(arguments);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            mFinishActivity = getArguments().getBoolean(ARGUMENT_FINISH_ACTIVITY);
            mMessage = getArguments().getString(ARGUMENT_MESSAGE_STRING);
            return new AlertDialog.Builder(getActivity())
                    .setMessage(mMessage)
                    .setPositiveButton(android.R.string.ok, null)
                    .create();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            super.onDismiss(dialog);
            if (mFinishActivity) {
                Toast.makeText(getActivity(), R.string.permission_required_toast,
                        Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    }

    /**
     * A dialog that explains the use of the location permission and requests the necessary
     * permission.
     * <p>
     * The activity should implement
     * {@link android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback}
     * to handle permit or denial of this permission request.
     */
    public static class RationaleDialog extends DialogFragment {

        private static final String ARGUMENT_MESSAGE_STRING = "message";
        private static final String ARGUMENT_PERMISSION_ARRAY = "permission_array";
        private static final String ARGUMENT_PERMISSION = "permission";
        private static final String ARGUMENT_PERMISSION_REQUEST_CODE = "requestCode";

        private static final String ARGUMENT_FINISH_ACTIVITY = "finish";
        
        private static final String ARGUMENT_ERROR = "error";
        
        
        private boolean mFinishActivity = false;
        private String mMessage = "";
        private String mError = "";

        /**
         * Creates a new instance of a dialog displaying the rationale for the use of the location
         * permission.
         * <p>
         * The permission is requested after clicking 'ok'.
         *
         * @param requestCode    Id of the request that is used to request the permission. It is
         *                       returned to the
         *                       {@link android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback}.
         * @param finishActivity Whether the calling Activity should be finished if the dialog is
         *                       cancelled.
         */
        public static RationaleDialog newInstance(String message, String error, String permission, int requestCode, boolean finishActivity) {
            Bundle arguments = new Bundle();
            arguments.putString(ARGUMENT_MESSAGE_STRING, message);
            arguments.putString(ARGUMENT_ERROR,error);
            arguments.putString(ARGUMENT_PERMISSION,permission);
            arguments.putInt(ARGUMENT_PERMISSION_REQUEST_CODE, requestCode);
            arguments.putBoolean(ARGUMENT_FINISH_ACTIVITY, finishActivity);
            RationaleDialog dialog = new RationaleDialog();
            dialog.setArguments(arguments);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Bundle arguments = getArguments();
            final String requestPermission = arguments.getString(ARGUMENT_PERMISSION);
            
            final int requestCode = arguments.getInt(ARGUMENT_PERMISSION_REQUEST_CODE);
            mFinishActivity = arguments.getBoolean(ARGUMENT_FINISH_ACTIVITY);
            mMessage = arguments.getString(ARGUMENT_MESSAGE_STRING);
            mError = arguments.getString(ARGUMENT_ERROR);
    
            return new AlertDialog.Builder(getActivity())
                    .setMessage(mMessage)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // After click on Ok, request the permission.
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{requestPermission},
                                    requestCode);
                            // Do not finish the Activity while requesting permission.
                            mFinishActivity = false;
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .create();
            
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            super.onDismiss(dialog);
            if (mFinishActivity) {
                Toast.makeText(getActivity(),
                        mError,
                        Toast.LENGTH_SHORT)
                        .show();
                //getActivity().finish();
            }
        }
    }
    
    
    public static class RationaleDialog2 extends DialogFragment {
        
        private static final String ARGUMENT_MESSAGE_STRING = "message";
        private static final String ARGUMENT_PERMISSION_ARRAY = "permission_array";
        private static final String ARGUMENT_PERMISSION = "permission";
        private static final String ARGUMENT_PERMISSION_REQUEST_CODE = "requestCode";
        
        private static final String ARGUMENT_FINISH_ACTIVITY = "finish";
        
        private static final String ARGUMENT_ERROR = "error";
        
        
        private boolean mFinishActivity = false;
        private String mMessage = "";
        private String mError = "";
        
        /**
         * Creates a new instance of a dialog displaying the rationale for the use of the location
         * permission.
         * <p>
         * The permission is requested after clicking 'ok'.
         *
         * @param requestCode    Id of the request that is used to request the permission. It is
         *                       returned to the
         *                       {@link android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback}.
         * @param finishActivity Whether the calling Activity should be finished if the dialog is
         *                       cancelled.
         */
        public static RationaleDialog2 newInstance(String message, String error, int requestCode, boolean finishActivity, String[] permission) {
            Bundle arguments = new Bundle();
            arguments.putString(ARGUMENT_MESSAGE_STRING, message);
            arguments.putString(ARGUMENT_ERROR,error);
            arguments.putStringArray(ARGUMENT_PERMISSION_ARRAY,permission);
            arguments.putInt(ARGUMENT_PERMISSION_REQUEST_CODE, requestCode);
            arguments.putBoolean(ARGUMENT_FINISH_ACTIVITY, finishActivity);
            RationaleDialog2 dialog = new RationaleDialog2();
            dialog.setArguments(arguments);
            return dialog;
        }
        
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Bundle arguments = getArguments();
            final String[] requestMultiplePermission = arguments.getStringArray(ARGUMENT_PERMISSION_ARRAY);
            
            final int requestCode = arguments.getInt(ARGUMENT_PERMISSION_REQUEST_CODE);
            mFinishActivity = arguments.getBoolean(ARGUMENT_FINISH_ACTIVITY);
            mMessage = arguments.getString(ARGUMENT_MESSAGE_STRING);
            mError = arguments.getString(ARGUMENT_ERROR);
            
            return new AlertDialog.Builder(getActivity())
                    .setMessage(mMessage)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // After click on Ok, request the permission.
                            ActivityCompat.requestPermissions(getActivity(),
                                    requestMultiplePermission,
                                    requestCode);
                            // Do not finish the Activity while requesting permission.
                            mFinishActivity = false;
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .create();
            
        }
        
        @Override
        public void onDismiss(DialogInterface dialog) {
            super.onDismiss(dialog);
            if (mFinishActivity) {
                Toast.makeText(getActivity(),
                        mError,
                        Toast.LENGTH_SHORT)
                        .show();
                //getActivity().finish();
            }
        }
    }
}
