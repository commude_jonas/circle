package jp.co.commude.circleapp.module.main.friends;

/**
 * Created by vic_villanueva on 21/02/2018.
 */

public class RecommendedFriendsItem {
    
    public int userPicture;
    public String userName;
    public String noEvents;
    
    public boolean friend;
    
    public RecommendedFriendsItem(int userPicture, String userName, String noEvents, boolean friend) {
        this.userPicture = userPicture;
        this.userName = userName;
        this.noEvents = noEvents;
        this.friend = friend;
    }
    
    public int getUserPicture() {
        return userPicture;
    }
    
    public void setUserPicture(int userPicture) {
        this.userPicture = userPicture;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getNoEvents() {
        return noEvents;
    }
    
    public void setNoEvents(String noEvents) {
        this.noEvents = noEvents;
    }
    
    public boolean isFriend() {
        return friend;
    }
    
    public void setFriend(boolean friend) {
        this.friend = friend;
    }
}
