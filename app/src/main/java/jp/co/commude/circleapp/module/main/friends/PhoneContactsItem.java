package jp.co.commude.circleapp.module.main.friends;

/**
 * Created by vic_villanueva on 21/02/2018.
 */

public class PhoneContactsItem {
    
    public int userPicture;
    public String userName;
    
    public boolean user;
    
    public PhoneContactsItem(int userPicture, String userName, boolean user) {
        this.userPicture = userPicture;
        this.userName = userName;
        this.user = user;
    }
    
    public int getUserPicture() {
        return userPicture;
    }
    
    public void setUserPicture(int userPicture) {
        this.userPicture = userPicture;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public boolean isUser() {
        return user;
    }
    
    public void setUser(boolean user) {
        this.user = user;
    }
}
