package jp.co.commude.circleapp.module.confirm;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.category.UserCategoryActivity;

/**
 * Created by vic_villanueva on 31/01/2018.
 */

public class UserPasswordConfirmationPageActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {
    
    TextView tvAlertMessage;
    EditText etUserPassword, etConfirmUserPassword;
    ConstraintLayout register2;
    ImageView ivBackPress;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_password_confirmation);
    
        ivBackPress = (ImageView) findViewById(R.id.ivBackpress);
        ivBackPress.setOnClickListener(this);
        tvAlertMessage = (TextView) findViewById(R.id.tvAlertMessage);
        etUserPassword = (EditText) findViewById(R.id.etUserPassword);
        etConfirmUserPassword = (EditText) findViewById(R.id.etConfirmUserPassword);
        register2 = (ConstraintLayout) findViewById(R.id.register2);
        
        etUserPassword.setOnClickListener(this);
        etConfirmUserPassword.setOnClickListener(this);
        
        etUserPassword.setOnFocusChangeListener(this);
        etConfirmUserPassword.setOnFocusChangeListener(this);
        
        register2.setOnClickListener(this);
        
    }
    
    @Override
    public void onClick(View v) {
        
        if(v.getId() == R.id.register2) {
            if (!etUserPassword.getText().toString().equals(etConfirmUserPassword.getText().toString()) ||
                    etUserPassword.getText().toString().isEmpty() || etConfirmUserPassword.getText().toString().isEmpty() ||
                    etUserPassword.getText().toString().trim().matches("") || etConfirmUserPassword.getText().toString().trim().matches("")) {
        
                YoYo.with(Techniques.Shake).playOn(etUserPassword);
                YoYo.with(Techniques.Shake).playOn(etConfirmUserPassword);
        
                YoYo.with(Techniques.Shake)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                tvAlertMessage.setVisibility(View.VISIBLE);
                            }
                    
                            @Override
                            public void onAnimationEnd(Animator animation) {
                            }
                    
                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }
                    
                            @Override
                            public void onAnimationRepeat(Animator animation) {
                        
                            }
                        }).playOn(tvAlertMessage);
                Log.d("Animate Shake", "TRUE");
        
            } else {
        
                //Toast.makeText(this, "password matched!", Toast.LENGTH_SHORT).show();
        
                startActivity(new Intent(UserPasswordConfirmationPageActivity.this, UserCategoryActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finishAfterTransition();
            }
        } else if (v.getId() == R.id.ivBackpress){
            onBackPressed();
        } else {
            if(tvAlertMessage.isShown()) {
                YoYo.with(Techniques.FadeOut).duration(300)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }
                    
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                tvAlertMessage.setVisibility(View.GONE);
                            }
                    
                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }
                    
                            @Override
                            public void onAnimationRepeat(Animator animation) {
                        
                            }
                        }).playOn(tvAlertMessage);
                Log.d("Animate Fade Out", "TRUE");
            }
        }
    }
    
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()){
            case R.id.etUserPassword:
                if(hasFocus) {
                    etUserPassword.setHintTextColor(getResources().getColor(R.color.color_pink_2));
                    etUserPassword.setTextColor(getResources().getColor(R.color.colorRed0));
                } else {
                    etUserPassword.setHintTextColor(getResources().getColor(R.color.colorBlack1));
                    etUserPassword.setTextColor(getResources().getColor(R.color.colorBlack1));
                }
                break;
            case R.id.etConfirmUserPassword:
                if(hasFocus) {
                    etConfirmUserPassword.setHintTextColor(getResources().getColor(R.color.color_pink_2));
                    etConfirmUserPassword.setTextColor(getResources().getColor(R.color.colorRed0));
                } else {
                    etConfirmUserPassword.setHintTextColor(getResources().getColor(R.color.colorBlack1));
                    etConfirmUserPassword.setTextColor(getResources().getColor(R.color.colorBlack1));
                }
                break;
        }
        
        if(hasFocus) {
            YoYo.with(Techniques.FadeOut).duration(300)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }
    
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                tvAlertMessage.setVisibility(View.GONE);
                            }
    
                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }
    
                            @Override
                            public void onAnimationRepeat(Animator animation) {
        
                            }
                        }).playOn(tvAlertMessage);
                Log.d("Animate Fade Out", "TRUE");
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UserPasswordConfirmationPageActivity.this,UserConfirmationPageActivity.class));
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
        finishAfterTransition();
    }
    
}
