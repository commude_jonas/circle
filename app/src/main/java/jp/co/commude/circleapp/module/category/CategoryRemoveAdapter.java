package jp.co.commude.circleapp.module.category;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.UIControlsUtils;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 27/02/2018.
 */

public class CategoryRemoveAdapter extends RecyclerView.Adapter<CategoryRemoveAdapter.CategoryAddedViewHolder> {
    
    Context context;
    ArrayList<CategoryItem> categoryItems;
    
    private SparseBooleanArray selectedItems;
    
    private static int currentSelectedIndex = -1;
    
    public interface CategoryRemoveListener {
        void onAddedCategory(int position);
    }
    
    CategoryRemoveListener categoryRemoveListener;
    
    public CategoryRemoveAdapter(Context context, ArrayList<CategoryItem> categoryItems, CategoryRemoveListener categoryRemoveListener) {
        this.context = context;
        this.categoryItems = categoryItems;
        selectedItems = new SparseBooleanArray();
        this.categoryRemoveListener = categoryRemoveListener;
    }
    
    public class CategoryAddedViewHolder extends RecyclerView.ViewHolder {
    
        public ImageView ivCategoryPic;
        public TextView tvCategoryName;
        public ImageView ivToggle;
        
        public RelativeLayout categoryContainer;
        
        public CategoryAddedViewHolder(View view) {
            super(view);
            ivCategoryPic = (ImageView) view.findViewById(R.id.itemImage);
            tvCategoryName = (TextView) view.findViewById(R.id.itemText);
            ivToggle = (ImageView) view.findViewById(R.id.itemToggle);
            categoryContainer = (RelativeLayout) view.findViewById(R.id.category_container);
        }
    }
    
    @Override
    public CategoryAddedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryAddedViewHolder(LayoutInflater.from(context).inflate(R.layout.view_category_item_remove,parent,false));
    }
    
    @Override
    public void onBindViewHolder(CategoryAddedViewHolder holder, final int position) {
        CategoryItem categoryItem = categoryItems.get(position);
    
        holder.tvCategoryName.setText(categoryItem.getCategoryName());
    
        Bitmap ShrinkedImage = new UIControlsUtils().generateScaledBitmap(context,categoryItem.getCategoryImgrsc());
        
        Glide.with(context)
                .load(ShrinkedImage)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.noTransformation())
                .into(holder.ivCategoryPic);
    
        holder.itemView.setActivated(selectedItems.get(position, false));
        if(categoryRemoveListener != null) {
            holder.ivCategoryPic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        categoryRemoveListener.onAddedCategory(position);
                    }
                });
        }
        
        if(categoryItem.isSelected()){
            holder.categoryContainer.setVisibility(View.GONE);
        } else {
            holder.categoryContainer.setVisibility(View.VISIBLE);
        }
        
        //applySelected(holder, position);
    
    }
    
    private void applySelected(CategoryAddedViewHolder holder, int position) {
            holder.ivToggle.setVisibility(View.VISIBLE);
    }
    
    @Override
    public int getItemCount() {
        return categoryItems.size();
    }
    
    public int getSelectedItemCount(){
        return selectedItems.size();
    }
    
    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }
    
    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }
    
    public void removeData(int position) {
        categoryItems.remove(position);
        resetCurrentIndex();
        notifyItemChanged(position);
    }
    
    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }
}
