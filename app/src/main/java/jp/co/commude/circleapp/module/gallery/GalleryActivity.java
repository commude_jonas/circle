package jp.co.commude.circleapp.module.gallery;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import jp.co.commude.circleapp.R;

/**
 * Created by vic_villanueva on 23/02/2018.
 */

public class GalleryActivity extends AppCompatActivity
        implements View.OnClickListener, GalleryImageListAdapter.OnSelectImageListener {
    
    private static final String TAG = "GalleryActivity";
    ImageView ivBackpress;
    TextView tvCancel;
    RecyclerView rvGalleryPhotos;
    Button btPhotoSelected;
    private Cursor cursor;
    
    private int columnIndex;
    
    GalleryImageListAdapter galleryImageListAdapter;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_gallery);
        
        rvGalleryPhotos = (RecyclerView) findViewById(R.id.rvGalleryPhotos);
    
        // Set up an array of the Thumbnail Image ID column we want
        String[] projection = {MediaStore.Images.Thumbnails._ID};
        
        // Create the cursor pointing to the SDCard
        cursor = getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                projection, // Which columns to return
                null,       // Return all rows
                null,
                MediaStore.Images.Thumbnails.IMAGE_ID);
        // Get the column index of the Thumbnails Image ID
        
        columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
    
        galleryImageListAdapter = new GalleryImageListAdapter(GalleryActivity.this, cursor,columnIndex, this);
        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 4, GridLayoutManager.HORIZONTAL|GridLayoutManager.VERTICAL, true);
        rvGalleryPhotos.setLayoutManager(layoutManager);
        rvGalleryPhotos.setHasFixedSize(true);
        rvGalleryPhotos.setItemAnimator(new DefaultItemAnimator());
        rvGalleryPhotos.setAdapter(galleryImageListAdapter);
        galleryImageListAdapter.notifyDataSetChanged();
        
        TextView tvNoofPictures = (TextView) findViewById(R.id.tvNoofPictures);
        tvNoofPictures.setText(galleryImageListAdapter.getItemCount()+"");
        
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        tvCancel = (TextView) findViewById(R.id.tvCancel);
        btPhotoSelected = (Button) findViewById(R.id.btPhotoSelected);
        ivBackpress.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
    
        btPhotoSelected.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.tvCancel:
                onBackPressed();
                break;
            case R.id.btPhotoSelected:
                onBackPressed();
                break;
        }
        
    }
    
    @Override
    public void onImageSelected(int position) {
    
        galleryImageListAdapter.toggleSelection(position);
        
        if(galleryImageListAdapter.getSelectedItemCount() < 20){
            Log.d(TAG, "アップロード (" +galleryImageListAdapter.getSelectedItemCount()+"/20)");
        } else {
            Toast.makeText(this, "You have exceed to the numbers of photo to upload", Toast.LENGTH_SHORT).show();
        }
        
        btPhotoSelected.setText("アップロード (" +galleryImageListAdapter.getSelectedItemCount()+"/20)");
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    
}
