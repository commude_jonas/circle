package jp.co.commude.circleapp.module.album;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.CommonUtils;
import jp.co.commude.circleapp.utility.PermissionUtils;

/**
 * Created by vic_villanueva on 26/02/2018.
 */

public class AlbumActivity extends AppCompatActivity implements View.OnClickListener{
    
    ImageView ivBackpress;
    TextView tvEdit,tvSave;
    
    private static int navIndex = 0;
    private static final String ADD =  "add";
    private static final String DELETE =  "delete";
    private static String CURRENT_TAG = ADD;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        setContentView(R.layout.activity_album_edit);
        
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        tvEdit = (TextView) findViewById(R.id.tvEdit);
        tvSave = (TextView) findViewById(R.id.tvSave);
        
        ivBackpress.setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        
        if(savedInstanceState == null) {
            navIndex = 0;
            CURRENT_TAG = ADD;
            loadFragment();
        }
        
    }
    
    private void checkPermission(){
        PermissionUtils.checkMultiplePermission(this, PermissionUtils.MULTIPLE_PERMISSION_REQUEST_CODE,getString(R.string.dialog_storage_permission_message),
                getString(R.string.dialog_storage_permission_message), new String[] {
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA});
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        checkPermission();
    }
    
    private void loadFragment(){
        CommonUtils.loadFragment(this, R.id.albumframe, getFragment(), CURRENT_TAG);
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.ivAddPhoto:
                CommonUtils.showBottomSheetCameraLibrary(AlbumActivity.this);
                break;
            case R.id.tvEdit:
                navIndex = 1;
                CURRENT_TAG = DELETE;
                tvSave.setVisibility(View.VISIBLE);
                tvEdit.setVisibility(View.GONE);
                break;
            case R.id.tvSave:
                navIndex = 0;
                CURRENT_TAG = ADD;
                tvEdit.setVisibility(View.VISIBLE);
                tvSave.setVisibility(View.GONE);
                break;
        }
        loadFragment();
    }
    
    public Fragment getFragment(){
        switch (navIndex) {
            case 0:
                AlbumAddPhotoFragment albumAddPhotoFragment = new AlbumAddPhotoFragment();
                return albumAddPhotoFragment;
            case 1:
                AlbumEditPhotoFragment albumEditPhotoFragment = new AlbumEditPhotoFragment();
                return albumEditPhotoFragment;
            default:
                return null;
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
}
