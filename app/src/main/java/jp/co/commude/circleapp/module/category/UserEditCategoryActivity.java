package jp.co.commude.circleapp.module.category;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;

/**
 * Created by vic_villanueva on 09/03/2018.
 */

public class UserEditCategoryActivity extends AppCompatActivity implements View.OnClickListener, CategoryRemoveAdapter.CategoryRemoveListener {
    
    RecyclerView rvCurrentCategory,rvAdditionalCategory;
    
    CategoryRemoveAdapter categoryRemoveAdapter;
    
    public ArrayList<CategoryItem> categoryItems;
    
    ImageView ivBackpress;
    TextView tvEdit,tvSave;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_category_edit2);
        
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        tvEdit = (TextView) findViewById(R.id.tvEdit);
        tvSave = (TextView) findViewById(R.id.tvSave);
        
        ivBackpress.setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        tvSave.setOnClickListener(this);
    
        rvCurrentCategory = (RecyclerView) findViewById(R.id.rvCurrentCategory);
        rvAdditionalCategory = (RecyclerView) findViewById(R.id.rvAdditionalCategory);
        categoryItems = new ArrayList<CategoryItem>();
        Globals.CategoryItems(this,categoryItems, R.array.str_res, 4,0);
        categoryRemoveAdapter = new CategoryRemoveAdapter(this,categoryItems,this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),3);
        rvCurrentCategory.setLayoutManager(layoutManager);
        rvCurrentCategory.setItemAnimator(new DefaultItemAnimator());
        rvCurrentCategory.setAdapter(categoryRemoveAdapter);
        categoryRemoveAdapter.notifyDataSetChanged();
        
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.tvEdit:
                break;
            case R.id.tvSave:
    
                onBackPressed();
                break;
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    
    public static void open(Activity activity){
        activity.startActivity(new Intent(activity, UserEditCategoryActivity.class));
    }
    
    @Override
    public void onAddedCategory(int position) {
        //toggleSelection(position);
        
        if (categoryRemoveAdapter.getSelectedItemCount() > 0) {
        
        } else {
            categoryItems.remove(position);
            Toast.makeText(UserEditCategoryActivity.this, "new size"+categoryItems.size(), Toast.LENGTH_SHORT).show();
            categoryRemoveAdapter.notifyItemRemoved(position);
            categoryRemoveAdapter.notifyItemRangeChanged(position,categoryItems.size());
            
        }
    }
    
}