package jp.co.commude.circleapp.module.user_settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import jp.co.commude.circleapp.R;

/**
 * Created by vic_villanueva on 20/02/2018.
 */

public class UserSettingsActivity extends AppCompatActivity implements View.OnClickListener {
    
    ImageView ivBackpress;
    Switch sPushNotif, sPublicEvent;
    
    TextView tvDirectMessage, tvPrivateSettings, tvLoginAuth, tvAppVersion, tvTermUse, tvPrivacyPolicy;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        setContentView(R.layout.activity_user_settings);
        
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        sPushNotif = (Switch) findViewById(R.id.sPushNotif);
        sPublicEvent = (Switch) findViewById(R.id.sPublicEvent);
        
        tvDirectMessage = (TextView) findViewById(R.id.tvDirectMessage);
        tvPrivateSettings = (TextView) findViewById(R.id.tvPrivateSettings);
        tvLoginAuth = (TextView) findViewById(R.id.tvLoginAuth);
        tvAppVersion = (TextView) findViewById(R.id.tvAppVersion);
        tvTermUse = (TextView) findViewById(R.id.tvTermUse);
        tvPrivacyPolicy = (TextView) findViewById(R.id.tvPrivacyPolicy);
    
        tvDirectMessage.setOnClickListener(this);
        tvPrivateSettings.setOnClickListener(this);
        tvLoginAuth.setOnClickListener(this);
        tvAppVersion.setOnClickListener(this);
        tvTermUse.setOnClickListener(this);
        tvPrivacyPolicy.setOnClickListener(this);
        
        ivBackpress.setOnClickListener(this);
        
        sPushNotif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Toast.makeText(UserSettingsActivity.this, "push notification enabled", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(UserSettingsActivity.this, "push notification disabled", Toast.LENGTH_SHORT).show();
            }
        });
        
        sPublicEvent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Toast.makeText(UserSettingsActivity.this, "public events enabled", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(UserSettingsActivity.this, "public events disabled", Toast.LENGTH_SHORT).show();
            }
        });
        
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.tvDirectMessage:
                Toast.makeText(this, "tap", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvPrivateSettings:
                Toast.makeText(this, "tap", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvLoginAuth:
                Toast.makeText(this, "tap", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvAppVersion:
                Toast.makeText(this, "tap", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvTermUse:
                Toast.makeText(this, "tap", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvPrivacyPolicy:
                Toast.makeText(this, "tap", Toast.LENGTH_SHORT).show();
                break;
        }
        
    }
    
    public static void open(Activity act){
        Intent intent = new Intent(act, UserSettingsActivity.class);
        act.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        act.startActivity(intent);
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
}
