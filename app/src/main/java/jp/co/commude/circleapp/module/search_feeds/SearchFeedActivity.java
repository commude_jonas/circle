package jp.co.commude.circleapp.module.search_feeds;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;
import jp.co.commude.circleapp.module.category.CategoryItem;
import jp.co.commude.circleapp.module.main.MainActivity;
import jp.co.commude.circleapp.module.search_map.SearchOnMapActivity;
import jp.co.commude.circleapp.utility.PermissionUtils;

/**
 * Created by vic_villanueva on 25/01/2018.
 */

public class SearchFeedActivity extends AppCompatActivity implements SearchItemListAdapter.QuickSearchHistoryListener, SearchCategoryListAdapter.CategoryListener {
    
    RecyclerView rv_search_history, rv_search_bycategory;
    
    ArrayList<String> searchItems;
    ArrayList<CategoryItem> categoryItems;
    
    SearchItemListAdapter searchItemListAdapter;
    SearchCategoryListAdapter searchCategoryListAdapter;
    
    TextView tvSearchViaLocation;
    ImageView ivDeleteSearchHistory;
    ImageView ivBackPress;
    
    View border;
    String[] searches = {"音楽芝居", "デザイン"};
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_event);
    
        tvSearchViaLocation = (TextView) findViewById(R.id.ivSearchMap);
        ivDeleteSearchHistory = (ImageView) findViewById(R.id.ivDeleteSearchHistory);
        ivBackPress = (ImageView) findViewById(R.id.ivBackpress);
    
        rv_search_history = (RecyclerView) findViewById(R.id.rv_search_history);
        rv_search_bycategory = (RecyclerView) findViewById(R.id.rv_search_bycategory);
        border = (View) findViewById(R.id.border);
        searchItems = new ArrayList<String>();
        categoryItems = new ArrayList<CategoryItem>();
        
        if(searches.length == 0) {
            border.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < searches.length; i++) {
                searchItems.add(searches[i]);
            }
            border.setVisibility(View.VISIBLE);
        }
    
        searchItemListAdapter = new SearchItemListAdapter(this,searchItems,this);
    
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rv_search_history.setLayoutManager(layoutManager);
        rv_search_history.setItemAnimator(new DefaultItemAnimator());
        rv_search_history.setAdapter(searchItemListAdapter);
        searchItemListAdapter.notifyDataSetChanged();
        
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                new CategoryImageLoaderView().execute();
            }
        }, 300);
        
        searchCategoryListAdapter = new SearchCategoryListAdapter(this, categoryItems,this);
        RecyclerView.LayoutManager layoutManager1 = new GridLayoutManager(getApplicationContext(), 3);
        
        rv_search_bycategory.setLayoutManager(layoutManager1);
        rv_search_bycategory.setItemAnimator(new DefaultItemAnimator());
        rv_search_bycategory.setAdapter(searchCategoryListAdapter);
        searchCategoryListAdapter.notifyDataSetChanged();
        
        tvSearchViaLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(SearchFeedActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(SearchFeedActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    /** Create permission prompt **/
        
                    PermissionUtils.checkMultiplePermission(SearchFeedActivity.this, PermissionUtils.MULTIPLE_PERMISSION_LOCATION_REQUEST_CODE,getString(R.string.dialog_location_permission_message),
                            getString(R.string.dialog_location_permission_message), new String[] {
                                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION});
        
                } else {
    
                    startActivity(new Intent(SearchFeedActivity.this, SearchOnMapActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        });
        
        ivBackPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ivDeleteSearchHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog();
                
            }
        });
    }
    
    private void deleteDialog(){
    
        final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.TransparentDialogTheme));
    
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        View view = getLayoutInflater().inflate(R.layout.dialog_delete_search, null);
        dialog.setContentView(view);
        dialog.show();
        
        TextView tvOk = (TextView) view.findViewById(R.id.tvOk);
        TextView tvCancel = (TextView) view.findViewById(R.id.tvCancel);
        
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchItemListAdapter.removeAllData();
                searchItemListAdapter.notifyDataSetChanged();
                border.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });
        
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // go to another activity
        Intent intent = new Intent(new Intent(SearchFeedActivity.this,MainActivity.class));
        intent.putExtra("fragment",0);
        intent.putExtra("fragmentName","feeds");
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finishAfterTransition();
    }
    
    @Override
    public void onLoadQuickSearch(String searchString) {
    
    }
    
    @Override
    public void onCategoryClicked(int position) {
        searchCategoryListAdapter.toggleSelection(position);
    }
    
    public class CategoryImageLoaderView extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params) {
            Globals.CategoryItems(SearchFeedActivity.this,categoryItems,R.array.str_res);
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            searchCategoryListAdapter.notifyDataSetChanged();
        }
    }
    
}
