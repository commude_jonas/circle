package jp.co.commude.circleapp.module.main.friends;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 21/02/2018.
 */

public class PhoneContactsAdapter extends RecyclerView.Adapter<PhoneContactsAdapter.PhoneContactsViewHolder> {
    
    Context context;
    ArrayList<PhoneContactsItem> phoneContactsItems;
    
    PhoneContactSelectListener phoneContactSelectListener;
    
    public PhoneContactsAdapter(Context context, ArrayList<PhoneContactsItem> phoneContactsItems, PhoneContactSelectListener phoneContactSelectListener) {
        this.context = context;
        this.phoneContactsItems = phoneContactsItems;
        this.phoneContactSelectListener = phoneContactSelectListener;
    }
    
    public class PhoneContactsViewHolder extends RecyclerView.ViewHolder {
    
        public LinearLayout conFriend;
        public ImageView ivUserPicture;
        public TextView tvUserName;
        
        public ConstraintLayout customFollowButton;
        public ImageView ivFriendStatus;
        public TextView tvFriendStatus;
        
        public PhoneContactsViewHolder(View view) {
            super(view);
            conFriend = view.findViewById(R.id.conFriend);
            ivUserPicture = view.findViewById(R.id.ivUserProfile);
            tvUserName = view.findViewById(R.id.tvUserName);
    
            customFollowButton = view.findViewById(R.id.customFollowButton);
            tvFriendStatus = view.findViewById(R.id.tvFriendStatus);
            ivFriendStatus = view.findViewById(R.id.ivFriendStatus);
            
        }
    }
    
    @Override
    public PhoneContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhoneContactsViewHolder(LayoutInflater.from(context).inflate(R.layout.view_vertical_friend_item,parent,false));
    }
    
    @Override
    public void onBindViewHolder(PhoneContactsViewHolder holder, int position) {
        
        PhoneContactsItem phoneContactsItem = phoneContactsItems.get(position);
        
        Glide.with(context)
                .load(phoneContactsItem.getUserPicture())
                .thumbnail(0.5f)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivUserPicture);
        
        holder.tvUserName.setText(phoneContactsItem.getUserName());
        
        //new UIControlsUtils().resizeImageByResolution(context,holder.customFollowButton, 0.32, 0.07);
        checkUserExist(phoneContactsItem, holder);
        applyClickEvent(holder,position);
    
    }
    
    private void checkUserExist(PhoneContactsItem phoneContactsItem, PhoneContactsViewHolder holder) {
        if(phoneContactsItem.isUser()){
            holder.customFollowButton.setBackground(context.getResources().getDrawable(R.drawable.follow_user_bg));
            holder.tvFriendStatus.setTextColor(context.getResources().getColor(R.color.colorRed0));
            holder.tvFriendStatus.setText(context.getString(R.string.str_follow_text2));
            holder.ivFriendStatus.setImageResource(R.drawable.ic_icon_plus_pink);
            holder.ivFriendStatus.setColorFilter(context.getResources().getColor(R.color.colorRed0));
        
        } else {
            holder.customFollowButton.setBackground(context.getResources().getDrawable(R.drawable.invite_user_bg));
            holder.tvFriendStatus.setText(context.getString(R.string.str_invite_text));
            holder.tvFriendStatus.setTextColor(context.getResources().getColor(R.color.colorGray3));
            holder.ivFriendStatus.setImageResource(R.drawable.ic_icon_plus_pink);
            holder.ivFriendStatus.setColorFilter(context.getResources().getColor(R.color.colorGray3));
        }
    
    }
    
    public void applyClickEvent(final PhoneContactsViewHolder holder, final int position){
        
        holder.tvFriendStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Selected", Toast.LENGTH_SHORT).show();
                phoneContactSelectListener.onPhoneContactSelect(holder.customFollowButton, holder.tvFriendStatus, holder.ivFriendStatus, position);
                notifyItemChanged(position);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return phoneContactsItems.size();
    }
    
    
    
    public interface PhoneContactSelectListener {
        void onPhoneContactSelect(View v, View v2, View v3, int position);
    }
}
