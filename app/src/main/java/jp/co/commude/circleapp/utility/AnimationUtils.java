package jp.co.commude.circleapp.utility;

import android.animation.Animator;
import android.content.Context;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

/**
 * Created by vic_villanueva on 09/03/2018.
 */

public class AnimationUtils {
    
    public static YoYo.YoYoString animationDatePicker(final Context context, final View view, Techniques techniques, long duration, long delay, int hideShow){
        final int STATE_ = hideShow;
        return YoYo.with(techniques)
                .duration(duration)
                .delay(delay)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }
            
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        switch (STATE_) {
                            case 0:
                                view.setVisibility(View.GONE);
                                break;
                            case 1:
                                view.setVisibility(View.VISIBLE);
                                break;
                            default:
                                break;
                        }
                
                    }
            
                    @Override
                    public void onAnimationCancel(Animator animation) {
                        Toast.makeText(context, "canceled previous animation", Toast.LENGTH_SHORT).show();
                    }
            
                    @Override
                    public void onAnimationRepeat(Animator animation) {
                
                    }
                })
                .playOn(view);
    }
    
    public static YoYo.YoYoString animationDatePicker2(final Context context, final View view, Techniques techniques, int hideShow, final View view2, final Techniques techniques2, final int hideShow2, final long duration, final long delay){
        final int STATE_ = hideShow;
        return YoYo.with(techniques)
                .duration(duration)
                .delay(delay)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }
            
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        switch (STATE_) {
                            case 0:
                                view.setVisibility(View.GONE);
                                break;
                            case 1:
                                view.setVisibility(View.VISIBLE);
                                break;
                            default:
                                break;
                        }
                
                        animationDatePicker(context,view2,techniques2,duration,0,hideShow2);
                
                    }
            
                    @Override
                    public void onAnimationCancel(Animator animation) {
                        Toast.makeText(context, "canceled previous animation", Toast.LENGTH_SHORT).show();
                    }
            
                    @Override
                    public void onAnimationRepeat(Animator animation) {
                
                    }
                })
                .playOn(view);
    }
}
