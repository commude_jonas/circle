package jp.co.commude.circleapp.module.main.profile;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.category.UserAddCategoryActivity;
import jp.co.commude.circleapp.module.followers.FollowersActivity;
import jp.co.commude.circleapp.module.followers.FollowingActivity;
import jp.co.commude.circleapp.module.main.MainActivity;
import jp.co.commude.circleapp.module.profile.UserEventFragment;
import jp.co.commude.circleapp.module.profile.UserEditProfileActivity;
import jp.co.commude.circleapp.module.user_settings.UserSettingsActivity;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 23/02/2018.
 */

public class ProfileFragment extends Fragment
        implements View.OnClickListener, ViewPager.OnPageChangeListener {
    
    public ProfileFragment() {
    }
    
    public static ProfileFragment newInstance() {
        
        Bundle args = new Bundle();
        
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    private FragmentActivity myContext;
    
    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    
    AppBarLayout appBarLayout;
    
    ViewPager vpEventPager;
    ImageView ivBackPress;
    ImageView ivEditProfile, ivSettings;
    ImageView ivProfilePicture,ivProfilePictureBig;
    
    LinearLayout conFollowing, conFollowers, conEvents;
    
    HorizontalScrollView hvEventsTypes;
    TextView tvPage1,tvPage2,tvPage3,tvPage4;
    
    NavigationFeedCategoryAdapter mAdapter;
    
    View vBorder;
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile,container,false);
        return view;
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    
        appBarLayout = (AppBarLayout) view.findViewById(R.id.apladdevent);
        vBorder = view.findViewById(R.id.vBorder);
        
        ivEditProfile = (ImageView) view.findViewById(R.id.ivEditProfile);
        ivSettings = (ImageView) view.findViewById(R.id.ivSettings);
        ivProfilePicture = (ImageView) view.findViewById(R.id.ivProfilePicture);
        ivProfilePictureBig = (ImageView) view.findViewById(R.id.ivProfilePictureBig);
    
        conFollowing = (LinearLayout) view.findViewById(R.id.conFollowing);
        conFollowers = (LinearLayout) view.findViewById(R.id.conFollowers);
        conEvents = (LinearLayout) view.findViewById(R.id.conCategories);
    
        hvEventsTypes = (HorizontalScrollView) view.findViewById(R.id.hvEventsTypes);
        
        tvPage1 = (TextView) view.findViewById(R.id.tvPage1);
        tvPage2 = (TextView) view.findViewById(R.id.tvPage2);
        tvPage3 = (TextView) view.findViewById(R.id.tvPage3);
        tvPage4 = (TextView) view.findViewById(R.id.tvPage4);
        
        vpEventPager = (ViewPager) view.findViewById(R.id.vpEventPager);
        
        ivEditProfile.setOnClickListener(this);
        ivSettings.setOnClickListener(this);
        
        conFollowing.setOnClickListener(this);
        conFollowers.setOnClickListener(this);
        conEvents.setOnClickListener(this);
        
        tvPage1.setOnClickListener(this);
        tvPage2.setOnClickListener(this);
        tvPage3.setOnClickListener(this);
        tvPage4.setOnClickListener(this);
        
        //new loadAnimation().execute();
        Glide.with(this)
                .load(R.drawable.profile_picture)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(ivProfilePicture);
    
        Glide.with(this)
                .load(R.drawable.profile_picture)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(ivProfilePictureBig);
        
        mAdapter = new NavigationFeedCategoryAdapter(getActivity().getSupportFragmentManager());
        
        vpEventPager.setAdapter(mAdapter);
        vpEventPager.setOnPageChangeListener(this);
        
        profileHeader();
        
    }
    
    private void profileHeader(){
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    
                    ivSettings.setImageTintList(null);
                    ivSettings.setImageTintList(getResources().getColorStateList(R.color.colorBlack2));
                
                    ivProfilePicture.setVisibility(View.VISIBLE);
                    vBorder.setBackgroundColor(getResources().getColor(R.color.colorGray8));
                
                } else if (verticalOffset == 0) {
                    // Expanded
                    ivSettings.setImageTintList(null);
                    ivSettings.setImageTintList(getResources().getColorStateList(R.color.colorWhite0));
    
                    vBorder.setBackgroundColor(getResources().getColor(R.color.transparent));
                    ivProfilePicture.setVisibility(View.GONE);
                } else {
                
                    ivProfilePicture.setVisibility(View.GONE);
                }
            }
        });
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                getActivity().onBackPressed();
                MainActivity.openDefault(getActivity(), 0, "feeds");
                break;
            case R.id.ivSettings:
                UserSettingsActivity.open(getActivity());
                break;
            case R.id.ivEditProfile:
                UserEditProfileActivity.openEditProfile(getActivity());
                break;
            case R.id.conFollowers:
                FollowersActivity.open(getActivity());
                break;
            case R.id.conFollowing:
                FollowingActivity.open(getActivity());
                break;
            case R.id.conCategories:
                UserAddCategoryActivity.open(getActivity());
                break;
            case R.id.tvPage1:
                vpEventPager.setCurrentItem(0,true);
                break;
            case R.id.tvPage2:
                vpEventPager.setCurrentItem(1,true);
                break;
            case R.id.tvPage3:
                vpEventPager.setCurrentItem(2,true);
                break;
            case R.id.tvPage4:
                vpEventPager.setCurrentItem(3,true);
                break;
        }
        
    }
    
    private void OnScrollPosition(int position){
        final View child = ((LinearLayout) hvEventsTypes.getChildAt(0)).getChildAt(position);
        int scrollTo = child.getLeft();
        hvEventsTypes.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        hvEventsTypes.setSmoothScrollingEnabled(true);
        hvEventsTypes.scrollTo(scrollTo,0);
    }
    
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        switch (position){
            case 0:
                OnScrollPosition(0);
                tvPage1.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage2.setBackgroundResource(R.drawable.tab_bg);
                tvPage3.setBackgroundResource(R.drawable.tab_bg);
                tvPage4.setBackgroundResource(R.drawable.tab_bg);
                
                tvPage1.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                tvPage2.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage3.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                
                break;
            case 1:
                OnScrollPosition(1);
                tvPage2.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage1.setBackgroundResource(R.drawable.tab_bg);
                tvPage3.setBackgroundResource(R.drawable.tab_bg);
                tvPage4.setBackgroundResource(R.drawable.tab_bg);
    
                tvPage1.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage2.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                tvPage3.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                break;
            case 2:
                OnScrollPosition(2);
                tvPage3.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage1.setBackgroundResource(R.drawable.tab_bg);
                tvPage2.setBackgroundResource(R.drawable.tab_bg);
                tvPage4.setBackgroundResource(R.drawable.tab_bg);
    
                tvPage1.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage3.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                tvPage2.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                break;
            case 3:
                OnScrollPosition(3);
                tvPage4.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage1.setBackgroundResource(R.drawable.tab_bg);
                tvPage2.setBackgroundResource(R.drawable.tab_bg);
                tvPage3.setBackgroundResource(R.drawable.tab_bg);
    
                tvPage1.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                tvPage2.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage3.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                break;
            default:
    
                OnScrollPosition(0);
                tvPage1.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage2.setBackgroundResource(R.drawable.tab_bg);
                tvPage3.setBackgroundResource(R.drawable.tab_bg);
                tvPage4.setBackgroundResource(R.drawable.tab_bg);
    
                tvPage2.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage1.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                tvPage3.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getActivity().getResources().getColor(R.color.colorBlack3));
                break;
        }
    }
    
    @Override
    public void onPageSelected(int position) {
    
    }
    
    @Override
    public void onPageScrollStateChanged(int state) {
    
    }
    
    public class NavigationFeedCategoryAdapter extends FragmentStatePagerAdapter {
        private final int PAGE_NUM = 4;
        
        public NavigationFeedCategoryAdapter(FragmentManager fm) {
            super(fm);
        }
        
        @Override
        public Fragment getItem(int position) {
            return new UserEventFragment();
        }
        
        @Override
        public int getCount() {
            return PAGE_NUM;
        }
        
    }
    
}
