package jp.co.commude.circleapp.module.profile;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Explode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.HashMap;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.database.SessionManager;
import jp.co.commude.circleapp.module.main.MainActivity;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 20/02/2018.
 */

public class UserEditProfileActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener {
    
    ImageView ivBackPress;
    TextView tvSettings;
    
    static ImageView ivProfile;
    EditText etUserName;
    EditText etBio;
    
    SessionManager sessionManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit_profile);
    
        sessionManager = new SessionManager(this);
        
        ivBackPress = (ImageView) findViewById(R.id.ivBackpress);
        tvSettings = (TextView) findViewById(R.id.tvSettings);
        
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etBio = (EditText) findViewById(R.id.etBio);
        
        Glide.with(this)
                .load(R.drawable.profile_picture)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(ivProfile);
        
        ivBackPress.setOnClickListener(this);
        tvSettings.setOnClickListener(this);
        etUserName.addTextChangedListener(this);
        etBio.addTextChangedListener(this);
        
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        
        // get user data from session
        HashMap<String, String> user = sessionManager.getUserDetails();
    
        // name
        String name = user.get(SessionManager.USER_NAME);
        // email
        String bio = user.get(SessionManager.USER_BIO);
    
        Log.d("details",""+name+" "+bio);
        
        sessionManager.openFirstInstance();
        
    }
    
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    
    }
    
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    
    }
    
    @Override
    public void afterTextChanged(Editable s) {
        /*if(!etUserName.isFocused() || !etBio.isFocused()){
            Toast.makeText(this, "Saved Changes", Toast.LENGTH_SHORT).show();
            String username = etUserName.getText().toString();
            String bio = etBio.getText().toString();
            sessionManager.saveUserProfileDetails(username,bio);
        }*/
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.tvSettings:
                onBackPressed();
                break;
        }
    }
    
    public static void openEditProfile(Activity activity){
        Intent intent = new Intent(activity, UserEditProfileActivity.class);
        
        View view = activity.findViewById(R.id.ivProfilePictureBig);
        ActivityOptions transiActivityOptions = ActivityOptions.makeSceneTransitionAnimation(activity, view , "myImage");
    
        Explode explode = new Explode();
        activity.getWindow().setEnterTransition(explode);
        activity.getWindow().setExitTransition(explode);
    
        activity.startActivity(intent, transiActivityOptions.toBundle());
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.openDefault(UserEditProfileActivity.this, 4, "feeds");
    }
}
