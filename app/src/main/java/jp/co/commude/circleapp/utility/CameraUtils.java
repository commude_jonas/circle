package jp.co.commude.circleapp.utility;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by vic_villanueva on 27/02/2018.
 */

public class CameraUtils {
    
    /** Check if this device has a camera */
    public static boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
}
