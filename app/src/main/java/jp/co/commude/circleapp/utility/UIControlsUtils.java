package jp.co.commude.circleapp.utility;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.print.PrintAttributes;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import org.threeten.bp.LocalDate;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.ImageResolution;
import jp.co.commude.circleapp.model.Resolution;

/**
 * Created by vic_villanueva on 23/01/2018.
 */

public class UIControlsUtils {

    public UIControlsUtils() {
    }
    public Bitmap generateScaledBitmap(Context context, int Drawable){

        Activity mActivity = (Activity) context;
        WindowManager mWindowManager = mActivity.getWindowManager();
        Display mDisplay = mWindowManager.getDefaultDisplay();
        int displayWidth = mDisplay.getWidth();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 3;
        BitmapFactory.decodeResource(mActivity.getResources(), Drawable, options);

        options.inSampleSize = calculateInSampleSize(options,160,160);
        options.inJustDecodeBounds = false;
        Bitmap scaledBitmap = BitmapFactory.decodeResource(mActivity.getResources(), Drawable, options);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos);
        byte[] bitmapdata = bos.toByteArray();

        Log.d("Scaled Bitmap", scaledBitmap.getByteCount()+" " + scaledBitmap.toString() + " bitmap height: " + scaledBitmap.getHeight() + " bitmap width: " + scaledBitmap.getWidth());


        return scaledBitmap;

    }


    public Bitmap generateScaledBitmap2(Context context, int Drawable){

        Activity mActivity = (Activity) context;
        WindowManager mWindowManager = mActivity.getWindowManager();
        Display mDisplay = mWindowManager.getDefaultDisplay();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 3;
        BitmapFactory.decodeResource(mActivity.getResources(), Drawable, options);

        options.inSampleSize = calculateInSampleSize(options,100,100);
        options.inJustDecodeBounds = false;
        Bitmap scaledBitmap = BitmapFactory.decodeResource(mActivity.getResources(), Drawable, options);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos);
        byte[] bitmapdata = bos.toByteArray();

        Log.d("Scaled Bitmap", scaledBitmap.getByteCount()+" " + scaledBitmap.toString() + " bitmap height: " + scaledBitmap.getHeight() + " bitmap width: " + scaledBitmap.getWidth());

        return scaledBitmap;

    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public Bitmap transformRoundedBitmap(Bitmap bitmap){
        int cornerRadius = 100;
        Bitmap insBitmap = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(insBitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);

        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        RectF rectF = new RectF(rect);
        canvas.drawRoundRect(rectF, cornerRadius, cornerRadius, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, 0, 0, paint);

        // Free the native object associated with this bitmap.
        bitmap.recycle();

        // Return the circular bitmap
        return insBitmap;

    }

    public void resizeView(Context context, View view, Double width, Double height){

        int maxX, maxY;

        Display mdisp = ((Activity) context).getWindowManager().getDefaultDisplay();
        Point mdispSize = new Point();
        mdisp.getSize(mdispSize);
        maxX = mdispSize.x;
        maxY = mdispSize.y;

        view.getLayoutParams().width = (int) (maxX * width);
        view.getLayoutParams().height = (int) (maxY * height);
    
        Log.i("NEW DISPLAY", "width " + view.getLayoutParams().width +" height "+view.getLayoutParams().height);

    }
    
    public int getMax(Context context, String axis){

        Display mdisp = ((Activity) context).getWindowManager().getDefaultDisplay();
        Point mdispSize = new Point();
        mdisp.getSize(mdispSize);

        Log.i("Display", "x is " + mdispSize.x + " and y is" + mdispSize.y);

        if(axis.contains("x")) return mdispSize.x;
        else if (axis.contains("y")) return mdispSize.y;
        else return 0;

    }

    public void resizeByResolution(Context context, View view){
        
        for (Resolution resolution: resolutionArrayList(context)) {
            if(resolution.MaxX == getMax(context, "x") && resolution.MaxY == getMax(context, "y"))
                resizeView(context, view, resolution.dX, resolution.dY);
            else resizeView(context, view, resolution.dX, resolution.dY);
        }

    }
    
    public void resizeFrame(Context context, View view){
        
        for (Resolution resolution: resolutionArrayList3(context)) {
            if(resolution.MaxX == getMax(context, "x") && resolution.MaxY == getMax(context, "y"))
                resizeView(context, view, resolution.dX, resolution.dY);
            else resizeView(context, view, resolution.dX, resolution.dY);
        }
        
    }
    
    private ArrayList<Resolution> resolutionArrayList3(Context context){
        ArrayList<Resolution> resDisplay = new ArrayList<>();
        
        resDisplay.add(new Resolution(768, 1184, 1.00, 0.33));
        resDisplay.add(new Resolution(768, 1232, 1.00, 0.20));
        resDisplay.add(new Resolution(720, 1280, 1.00, 0.33)); //
        resDisplay.add(new Resolution(1080,1920, 1.00, 0.33));//lenovo k4
        resDisplay.add(new Resolution(1440,2392, 0.95, 0.35));// nexus 6
        resDisplay.add(new Resolution(1600,2464, 1.00, 0.20));
        resDisplay.add(new Resolution(getScreenWidthSize(context),getScreenHeightSize(context),0.32,0.25));
        
        return resDisplay;
    }
    
    public void resizeOnMap(Context context, View view){
        
        for (Resolution resolution: resolutionArrayList2(context)) {
            if(resolution.MaxX == getMax(context, "x") && resolution.MaxY == getMax(context, "y"))
                resizeView(context, view, resolution.dX, resolution.dY);
            else resizeView(context, view, resolution.dX, resolution.dY);
        }
        
    }
    
    private ArrayList<Resolution> resolutionArrayList2(Context context){
        ArrayList<Resolution> resDisplay = new ArrayList<>();
        
        resDisplay.add(new Resolution(768, 1184, 1.00, 0.33));
        resDisplay.add(new Resolution(768, 1232, 1.00, 0.20));
        resDisplay.add(new Resolution(720, 1280, 1.00, 0.33)); //
        resDisplay.add(new Resolution(1080,1920, 1.00, 0.33));//lenovo k4
        resDisplay.add(new Resolution(1440,2392, 0.95, 0.35));// nexus 6
        resDisplay.add(new Resolution(1600,2464, 1.00, 0.20));
        resDisplay.add(new Resolution(getScreenWidthSize(context),getScreenHeightSize(context),0.85,0.27));
        
        return resDisplay;
    }
    
    private ArrayList<Resolution> resolutionArrayList(Context context){
        ArrayList<Resolution> resDisplay = new ArrayList<>();
    
        resDisplay.add(new Resolution(768, 1184, 1.00, 0.33));
        resDisplay.add(new Resolution(768, 1232, 1.00, 0.20));
        resDisplay.add(new Resolution(720, 1280, 1.00, 0.33)); //
        resDisplay.add(new Resolution(1080,1920, 1.00, 0.33));//lenovo k4
        resDisplay.add(new Resolution(1440,2392, 0.95, 0.35));// nexus 6
        resDisplay.add(new Resolution(1600,2464, 1.00, 0.20));
        resDisplay.add(new Resolution(getScreenWidthSize(context),getScreenHeightSize(context),0.95,0.375));
        
        return resDisplay;
    }
    
    
    public void resizeImageByResolution(Context context, View view, double dX, double dY){
        
        for (ImageResolution imageResolution: imageAppResolutionArrayList(context, dX, dY)) {
            if(imageResolution.MaxX == getMax(context, "x") && imageResolution.MaxY == getMax(context, "y"))
                resizeView(context, view, imageResolution.dX, imageResolution.dY);
            else resizeView(context, view, imageResolution.dX, imageResolution.dY);
        }
        
    }
    
    private ArrayList<ImageResolution> imageAppResolutionArrayList(Context context, double dX, double dY){
        ArrayList<ImageResolution> resDisplay = new ArrayList<>();
        
        resDisplay.add(new ImageResolution(getScreenWidthSize(context),getScreenHeightSize(context), dX, dY));
        
        return resDisplay;
    }
    
    
    public static int getScreenWidthSize(Context context){
        Activity activity = (Activity) context;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        
        return width;
    }
    
    public static int getScreenHeightSize(Context context){
        Activity activity = (Activity) context;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        
        return height;
    }
    
    
    public static String getScreenSize(Context context){
        Activity activity = (Activity) context;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        
        return "Size of screen device is "+width+" W x "+height+" H";
    }
    
    private static final String TAG = "SCREEN DISPLAY";
    public static void getDeviceScreenResolution(Context context){
        Activity activity = (Activity) context;
        Display display = activity.getWindowManager().getDefaultDisplay();
        String displayName = display.getName();  // minSdkVersion=17+
        Log.i(TAG, "displayName  = " + displayName);

// display size in pixels
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Log.i(TAG, "width        = " + width);
        Log.i(TAG, "height       = " + height);

// pixels, dpi
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int heightPixels = metrics.heightPixels;
        int widthPixels = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        float xdpi = metrics.xdpi;
        float ydpi = metrics.ydpi;
        Log.i(TAG, "widthPixels  = " + widthPixels);
        Log.i(TAG, "heightPixels = " + heightPixels);
        Log.i(TAG, "densityDpi   = " + densityDpi);
        Log.i(TAG, "xdpi         = " + xdpi);
        Log.i(TAG, "ydpi         = " + ydpi);

// deprecated
        int screenHeight = display.getHeight();
        int screenWidth = display.getWidth();
        Log.i(TAG, "screenHeight = " + screenHeight);
        Log.i(TAG, "screenWidth  = " + screenWidth);

// orientation (either ORIENTATION_LANDSCAPE, ORIENTATION_PORTRAIT)
        int orientation = activity.getResources().getConfiguration().orientation;
        Log.i(TAG, "orientation  = " + orientation);
    }
    
    
    
    public String getDayinAWeek(int count){
        switch (count){
            case 1: return "SUN";
            case 2: return "MON";
            case 3: return "TUE";
            case 4: return "WED";
            case 5: return "THU";
            case 6: return "FRI";
            case 7: return "SAT";
            default:
                return "";
        }
    }
    
    
    public String getMonthinYear(int count){
        switch (count){
            case 0: return "JAN";
            case 1: return "FEB";
            case 2: return "MAR";
            case 3: return "APR";
            case 4: return "MAY";
            case 5: return "JUN";
            case 6: return "JUL";
            case 7: return "AUG";
            case 8: return "SEP";
            case 9: return "OCT";
            case 10: return "NOV";
            case 11: return "DEC";
            
            default:
                return "";
        }
    }
    
    public static String getMonthStringinAYear(int count){
        String[] MONTH = {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
        return MONTH[count];
    }
    public static String getDayStringinAWeek(int count){
        String[] DAY = {"SUN","MON","TUE","WED","THUR","FRI","SAT"};
        return DAY[count];
    }
    
    public static void changeColor(String line){
        Pattern numberPat = Pattern.compile("\\d+");
        Matcher matcher1 = numberPat.matcher(line);
    
        Pattern stringPat = Pattern.compile("What is the square of", Pattern.CASE_INSENSITIVE);
        Matcher matcher2 = stringPat.matcher(line);
    
        if (matcher1.find() && matcher2.find())
        {
            int number = Integer.parseInt(matcher1.group());
        }
    }
    
    public static String getDate(LocalDate date){
        
        int dayOftheMonth = date.getDayOfMonth();
        int month = date.getMonthValue();
        String newMonthFormat = "";
        String newDayOftheMonthFormat= "";
        
        if(Integer.toString(month).length() == 1){
            newMonthFormat = "0"+month;
        } else newMonthFormat = month+"";
        
        if(Integer.toString(dayOftheMonth).length() == 1){
            newDayOftheMonthFormat = "0"+dayOftheMonth;
        } else newDayOftheMonthFormat = dayOftheMonth+"";
        
        String monthString = date.getMonth().toString();
        int year = date.getYear();
        
        String dayOftheWeek = date.getDayOfWeek().toString();
        
        String displayNewDateString = year+"/"+newMonthFormat+"/"+newDayOftheMonthFormat;
        Log.d("DATE SELECTED :", displayNewDateString);
        
        return displayNewDateString;
    }
    public static String getDateDayOftheWeek(LocalDate date){
        
        String dayOftheWeek = date.getDayOfWeek().toString();
        Log.d("DAY OF THE WEEK :", dayOftheWeek);
        
        return dayOftheWeek;
    }
    
    public static SpannableString setTextSpanNewColor(Context context, String textString, int start, int end){
    
        SpannableString pinkColorText = new SpannableString(textString);
        pinkColorText.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorRed0)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return pinkColorText;
    }
}
