package jp.co.commude.circleapp.model;

/**
 * Created by vic_villanueva on 31/01/2018.
 */


public class ImageResolution {
    
    public int MaxX;
    public int MaxY;
    public double dX;
    public double dY;
    
    public ImageResolution(int maxX, int maxY, double dX, double dY) {
        MaxX = maxX;
        MaxY = maxY;
        this.dX = dX;
        this.dY = dY;
    }

    /*
    *
    * 720 x 1280
    * 1440 x 2392
    *
    * */
    
}