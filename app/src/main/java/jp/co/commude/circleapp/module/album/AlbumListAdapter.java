package jp.co.commude.circleapp.module.album;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.UIControlsUtils;

/**
 * Created by vic_villanueva on 05/02/2018.
 */

public class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.AlbumListViewHolder> {
    
    Context context;
    ArrayList<AlbumPictureItem> albumPictureItems;
    AlbumPhotoListener albumPhotoListener;
    
    private SparseBooleanArray selectedItems;
    
    private static int currentSelectedIndex = -1;
    
    int viewType = 0;
    public AlbumListAdapter(Context context, ArrayList<AlbumPictureItem> albumPictureItems, int viewType) {
        this.context = context;
        this.albumPictureItems = albumPictureItems;
        selectedItems = new SparseBooleanArray();
        this.viewType = viewType;
    }
    
    public AlbumListAdapter(Context context, ArrayList<AlbumPictureItem> albumPictureItems, AlbumPhotoListener albumPhotoListener, int viewType) {
        this.context = context;
        this.albumPictureItems = albumPictureItems;
        this.albumPhotoListener = albumPhotoListener;
        this.viewType = viewType;
        selectedItems = new SparseBooleanArray();
    }
    
    public class AlbumListViewHolder extends RecyclerView.ViewHolder {
        
        public ImageView ivAlbumPicture;
        public ImageView ivToggle;
        public ViewGroup squareLayout;
        
        public AlbumListViewHolder(View view) {
            super(view);
            ivAlbumPicture = (ImageView) view.findViewById(R.id.ivAlbumPicture);
            ivToggle = (ImageView) view.findViewById(R.id.ivToggle);
            squareLayout =(ViewGroup) view.findViewById(R.id.constraints);
        }
    }
    
    @Override
    public AlbumListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlbumListViewHolder(LayoutInflater.from(context).inflate(R.layout.album_picture_item, parent,false));
    }
    
    @Override
    public void onBindViewHolder(final AlbumListViewHolder holder, final int position) {
        AlbumPictureItem albumPictureItem = albumPictureItems.get(position);
        
        Bitmap ShrinkedImage = new UIControlsUtils().generateScaledBitmap2(context,albumPictureItem.getPictureImg());
        
        if(viewType == 0){
            new UIControlsUtils().resizeImageByResolution(context, holder.squareLayout, 0.30,0.30);
        }
        
        Glide.with(context)
                .load(ShrinkedImage)
                .thumbnail(0.5f)
                .apply(RequestOptions.noTransformation())
                .into(holder.ivAlbumPicture);
        
        if(albumPhotoListener != null) {
            
            holder.itemView.setActivated(selectedItems.get(position, false));
            
            holder.ivAlbumPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    albumPhotoListener.onPhotoSelected(position);
                }
            });
            
        } else {}
    
        applySelected(holder, position);
    
    }
    
    private void applySelected(AlbumListViewHolder holder, int position) {
        if (selectedItems.get(position, false)) {
            holder.ivToggle.setVisibility(View.VISIBLE);
        } else {
            holder.ivToggle.setVisibility(View.GONE);
        }
    }
    
    @Override
    public int getItemCount() {
        return albumPictureItems.size();
    }
    
    public int getSelectedItemCount(){
        return selectedItems.size();
    }
    
    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }
    
    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }
    
    public void removeData(int position) {
        albumPictureItems.remove(position);
        resetCurrentIndex();
        selectedItems.clear();
    }
    
    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }
    
    
    public interface AlbumPhotoListener {
        void onPhotoSelected(int position);
    }
    
    /*public abstract class VisibleToggleClickListener implements View.OnClickListener {
        
        private boolean mVisible;
        
        @Override
        public void onClick(View v) {
            mVisible = !mVisible;
            changeVisibility(mVisible);
        }
        
        protected abstract void changeVisibility(boolean visible);
        
    }*/
}
