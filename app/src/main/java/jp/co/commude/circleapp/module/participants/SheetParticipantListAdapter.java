package jp.co.commude.circleapp.module.participants;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.UIControlsUtils;

/**
 * Created by fabonan.jonas on 06/02/2018.
 */

public class SheetParticipantListAdapter extends RecyclerView.Adapter<SheetParticipantListAdapter.ParticipantViewHolder> {
    
    Context context;
    ArrayList<ParticipantItem> participantItems;
    
    OnParticipantsProfileListeners onParticipantsListeners;
    int displaySize;
    
    public SheetParticipantListAdapter(Context context, ArrayList<ParticipantItem> participantItems, OnParticipantsProfileListeners onParticipantsListeners) {
        this.context = context;
        this.participantItems = participantItems;
        this.onParticipantsListeners = onParticipantsListeners;
    }
    
    public static class ParticipantViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivParticipantProfile;
        public TextView tvParticipantCount;
        public RelativeLayout squareLayout;
        
        public ParticipantViewHolder(View view) {
            super(view);
            ivParticipantProfile = (ImageView) view.findViewById(R.id.ivParticipantProfile);
            tvParticipantCount = (TextView) view.findViewById(R.id.tvParticipant);
            squareLayout =(RelativeLayout) view.findViewById(R.id.constraints);
        }
    }
    
    @Override
    public ParticipantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_user_participant_item, parent, false);
            
            return new ParticipantViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(ParticipantViewHolder holder, int position) {
        ParticipantItem participantItem = participantItems.get(position);
        
        Bitmap ShrinkedImage = new UIControlsUtils().generateScaledBitmap2(context,participantItem.getParticipantProfile());
        
       // new UIControlsUtils().resizeImageByResolution(context, holder.squareLayout, 0.15,0.15);
        Glide.get(context).onLowMemory();
        Glide.with(context)
                .load(ShrinkedImage)
                .thumbnail(0.5f)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivParticipantProfile);
        
        applyClickEvent(holder,position);
    }
    
    private void applyClickEvent(final ParticipantViewHolder holder, final int position) {
    
        holder.ivParticipantProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onParticipantsListeners.viewProfile(position);
            }
        });
        
    }
    
    @Override
    public int getItemCount() {
        return participantItems.size();
    }
    
    public void setDisplaySize(int displaySize) {
        this.displaySize = displaySize;
        notifyDataSetChanged();
    }
    
    public interface OnParticipantsProfileListeners{
            void viewProfile(int position);
    }
}
