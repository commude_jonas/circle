package jp.co.commude.circleapp.module.followers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;
import jp.co.commude.circleapp.module.profile.UserProfileActivity;

/**
 * Created by vic_villanueva on 19/02/2018.
 */

public class FollowersActivity extends AppCompatActivity implements FollowersListAdapter.FollowUserListener,
        View.OnClickListener{
    
    RecyclerView rvFollowersList;
    FollowersListAdapter followersListAdapter;
    ArrayList<FollowersItem> followersItems;
    
    ImageView ivBackPress;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        
        setContentView(R.layout.activity_feed_followers);
        ivBackPress = (ImageView) findViewById(R.id.ivBackpress);
        ivBackPress.setOnClickListener(this);
        
        rvFollowersList = (RecyclerView) findViewById(R.id.rvFollowersList);
        followersItems = new ArrayList<FollowersItem>();
        Globals.FollowerItems(followersItems,false);
        
        followersListAdapter = new FollowersListAdapter(this, followersItems, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        rvFollowersList.setLayoutManager(layoutManager);
        rvFollowersList.setItemAnimator(new DefaultItemAnimator());
        rvFollowersList.setHasFixedSize(true);
        rvFollowersList.setAdapter(followersListAdapter);
        followersListAdapter.setDisplaySize(8);
        followersListAdapter.notifyDataSetChanged();
        
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
        }
        
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
    
    @Override
    public void onFollowUser(View v, View v2, View v3, int position) {
        FollowersItem followersItem = followersItems.get(position);
        
        TextView tvMutualStatus = (TextView) v2;
        ImageView ivMutualStatus = (ImageView) v3;
        
        if(followersItem.isFriend()){
            followersItem.setFriend(false);
            v.setBackground(getResources().getDrawable(R.drawable.following_user_bg));
            tvMutualStatus.setTextColor(getResources().getColor(R.color.colorPrimary));
            tvMutualStatus.setText(getString(R.string.str_following_text));
            ivMutualStatus.setImageResource(R.drawable.ic_check_followers);
            ivMutualStatus.setColorFilter(getResources().getColor(R.color.colorPrimary));
            
        } else {
            followersItem.setFriend(true);
            v.setBackground(getResources().getDrawable(R.drawable.follow_user_bg));
            tvMutualStatus.setTextColor(getResources().getColor(R.color.colorRed0));
            tvMutualStatus.setText(getString(R.string.str_follow_text2));
            ivMutualStatus.setImageResource(R.drawable.ic_icon_plus_pink);
        }
        
        followersItems.set(position,followersItem);
        followersListAdapter.notifyItemChanged(position);
    }
    
    @Override
    public void onViewProfile(int position) {
        UserProfileActivity.open(FollowersActivity.this);
    }
    
    public static void open(Activity act){
        Intent intent = new Intent(act, FollowersActivity.class);
        act.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        act.startActivity(new Intent(act,FollowersActivity.class));
    }
    
}


