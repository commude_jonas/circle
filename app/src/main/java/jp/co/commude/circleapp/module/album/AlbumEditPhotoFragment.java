package jp.co.commude.circleapp.module.album;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.transition.Slide;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import jp.co.commude.circleapp.R;

public class AlbumEditPhotoFragment extends Fragment implements View.OnClickListener, AlbumListAdapter.AlbumPhotoListener {
    
        ArrayList<AlbumPictureItem> albumPictureItems;
        AlbumListAdapter albumListAdapter;
        RecyclerView rvAlbum;
        LinearLayout mLinearLayout;
        
        ImageView ivDeletePhoto;
        
        public AlbumEditPhotoFragment() {
        }
        
        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_album_edit_photo,container,false);
        }
        
        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            
            mLinearLayout = (LinearLayout) view.findViewById(R.id.mControl2);
            
            rvAlbum = (RecyclerView) view.findViewById(R.id.rvGridAlbum);
            
            albumPictureItems = new ArrayList<AlbumPictureItem>();
        
            albumPictureItems.add(new AlbumPictureItem(R.drawable.summer));
            albumPictureItems.add(new AlbumPictureItem(R.drawable.strawberry));
            albumPictureItems.add(new AlbumPictureItem(R.drawable.palay));
            albumPictureItems.add(new AlbumPictureItem(R.drawable.lente));
        
            albumListAdapter = new AlbumListAdapter(getActivity(),albumPictureItems,this, 1);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),3);
        
            rvAlbum.setLayoutManager(layoutManager);
            rvAlbum.setItemAnimator(new DefaultItemAnimator());
            rvAlbum.setAdapter(albumListAdapter);
            albumListAdapter.notifyDataSetChanged();
            
            if(albumListAdapter.getSelectedItemCount() == 0){
                mLinearLayout.setVisibility(View.GONE);
            } else {
                mLinearLayout.setVisibility(View.VISIBLE);
            }
    
            ivDeletePhoto = (ImageView) view.findViewById(R.id.ivDeletePhoto);
            ivDeletePhoto.setOnClickListener(this);
            
        }
    
        @Override
        public void onPhotoSelected(int position) {
            toggleSelection(position);
            
            if (albumListAdapter.getSelectedItemCount() > 0) {
                //enableActionMode(position);
            } else {
                // read the message which removes bold from the row
                AlbumPictureItem albumPictureItem =  albumPictureItems.get(position);
                albumPictureItem.setSelected(true);
                albumPictureItems.set(position, albumPictureItem);
                albumListAdapter.notifyItemChanged(position);
                Toast.makeText(getActivity(), "Selected", Toast.LENGTH_SHORT).show();
            }
    
            TransitionManager.beginDelayedTransition(mLinearLayout, new Slide(Gravity.BOTTOM).setDuration(200));
            if(albumListAdapter.getSelectedItemCount() == 0){
                mLinearLayout.setVisibility(View.GONE);
            } else {
                mLinearLayout.setVisibility(View.VISIBLE);
            }
            
        }
        
        void toggleSelection(int position){
            albumListAdapter.toggleSelection(position);
        }
        
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivDeletePhoto:
                    //delete
                    showBottomSheetDeletePhoto();
                    break;
            }
        }
    
    
        public void showBottomSheetDeletePhoto(){
            
            //R.layout.bottomsheet_add_photo_event
            View view = getActivity().getLayoutInflater().inflate(R.layout.bottomsheet_delete_photo, null);
    
            final BottomSheetDialog dialog = new BottomSheetDialog(getActivity(), R.style.TransparentTheme);
            dialog.setContentView(view);
            dialog.show();
    
            TextView tvDelete = (TextView) dialog.findViewById(R.id.tvDelete);
            TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
    
            tvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deletePhotos();
                    dialog.dismiss();
                }
            });
            
            tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            
        }
        
        // deleting the messages from recycler view
        private void deletePhotos() {
            List<Integer> selectedItemPositions =
                    albumListAdapter.getSelectedItems();
            for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
                albumListAdapter.removeData(selectedItemPositions.get(i));
            }
            albumListAdapter.notifyDataSetChanged();
        }
    }