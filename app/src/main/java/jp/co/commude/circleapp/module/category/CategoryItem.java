package jp.co.commude.circleapp.module.category;

import android.graphics.Bitmap;

/**
 * Created by vic_villanueva on 19/01/2018.
 */

public class CategoryItem {

    String categoryName;
    int categoryImgrsc;
    boolean isSelected;
    
    Bitmap shrinkImage;
    
    
    public CategoryItem(String categoryName) {
        this.categoryName = categoryName;
    }
    
    public CategoryItem(String categoryName, int categoryImgrsc) {
        this.categoryName = categoryName;
        this.categoryImgrsc = categoryImgrsc;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryImgrsc() {
        return categoryImgrsc;
    }

    public void setCategoryImgrsc(int categoryImgrsc) {
        this.categoryImgrsc = categoryImgrsc;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    
    public Bitmap getShrinkImage() {
        return shrinkImage;
    }
    
    public void setShrinkImage(Bitmap shrinkImage) {
        this.shrinkImage = shrinkImage;
    }
}
