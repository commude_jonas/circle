package jp.co.commude.circleapp.module.search_feeds;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;

/**
 * Created by vic_villanueva on 26/01/2018.
 */

public class SearchItemListAdapter extends RecyclerView.Adapter<SearchItemListAdapter.SearchViewHolder> {
    
    Context context;
    ArrayList<String> searchItems;
    
    private SparseBooleanArray selectedItems;
    
    QuickSearchHistoryListener quickSearchHistoryListener;
    
    public SearchItemListAdapter(Context context, ArrayList<String> searchItems, QuickSearchHistoryListener quickSearchHistoryListener) {
        this.context = context;
        this.searchItems = searchItems;
        this.quickSearchHistoryListener = quickSearchHistoryListener;
        this.selectedItems = new SparseBooleanArray();
    }
    
    public class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    
        public TextView tvSearchItem;
        
        public SearchViewHolder(View view) {
            super(view);
            tvSearchItem =(TextView) view.findViewById(R.id.tvSearchItem);
        }
    
        @Override
        public void onClick(View v) {
            quickSearchHistoryListener.onLoadQuickSearch(tvSearchItem.getText().toString());
        }
    }
    
    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_search,parent,false);
        
        return new SearchViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(SearchViewHolder holder, int position) {
        String searchItem = searchItems.get(position).toString();
        holder.tvSearchItem.setText(searchItem);
    }
    
    @Override
    public int getItemCount() {
        return searchItems.size();
    }
    
    public void removeAllData(){
        /*for(int i=0;i<=searchItems.size();i++){
            searchItems.remove(i);
        }*/
        
        searchItems.clear();
    }
    
    public interface QuickSearchHistoryListener {
        void onLoadQuickSearch(String searchString);
    }
    
    
}
