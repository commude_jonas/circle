package jp.co.commude.circleapp.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

/**
 * Created by vic_villanueva on 20/02/2018.
 */

public class SessionManager {

    SharedPreferences pref;
    //Editor for Shared Pref
    Editor editor;
    //Context
    Context _context;
    
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "user_circle_app";
    public static final String USER_NAME = "user_name";
    public static final String USER_BIO = "user_bio";
    public static final String isEDITED = "isEdited";
    
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    
    public void saveUserProfileDetails(String name, String bio){
        editor.putBoolean(isEDITED, true);
        editor.putString(USER_NAME, name);
        editor.putString(USER_BIO,bio);
        editor.commit();
    }
    
    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(USER_NAME, pref.getString(USER_NAME, null));
        
        // user email id
        user.put(USER_BIO, pref.getString(USER_BIO, null));
        
        // return user
        return user;
    }
    
    public void openFirstInstance(){
        if(isEdited()){
            editor.putBoolean(isEDITED,false);
        }
    }
    
    //public void storeCategoryLike
    
    public boolean isEdited(){
        return pref.getBoolean(isEDITED, false);
    }
    
}
