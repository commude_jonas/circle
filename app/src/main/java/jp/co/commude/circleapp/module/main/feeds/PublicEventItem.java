package jp.co.commude.circleapp.module.main.feeds;

/**
 * Created by vic_villanueva on 24/01/2018.
 */

public class PublicEventItem {
    
    int eventCover;
    String eventTitle;
    String eventHost;
    String eventDate;
    String eventDesc;
    String eventCapacity;
    
    boolean like;
    
    public PublicEventItem(int eventCover, String eventTitle, String eventHost, String eventDate, String eventDesc, String eventCapacity) {
        this.eventCover = eventCover;
        this.eventTitle = eventTitle;
        this.eventHost = eventHost;
        this.eventDate = eventDate;
        this.eventDesc = eventDesc;
        this.eventCapacity = eventCapacity;
    }
    
    public int getEventCover() {
        return eventCover;
    }
    
    public void setEventCover(int eventCover) {
        this.eventCover = eventCover;
    }
    
    public String getEventTitle() {
        return eventTitle;
    }
    
    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }
    
    public String getEventHost() {
        return eventHost;
    }
    
    public void setEventHost(String eventHost) {
        this.eventHost = eventHost;
    }
    
    public String getEventDate() {
        return eventDate;
    }
    
    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }
    
    public String getEventDesc() {
        return eventDesc;
    }
    
    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }
    
    public String getEventCapacity() {
        return eventCapacity;
    }
    
    public void setEventCapacity(String eventCapacity) {
        this.eventCapacity = eventCapacity;
    }
    
    public boolean isLike() {
        return like;
    }
    
    public void setLike(boolean like) {
        this.like = like;
    }
}
