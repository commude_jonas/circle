package jp.co.commude.circleapp.module.main.friends;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 21/02/2018.
 */

public class RecommendedFriendsAdapter extends RecyclerView.Adapter<RecommendedFriendsAdapter.FriendsViewHolder> {
    
    Context context;
    ArrayList<RecommendedFriendsItem> recommendedFriendsItems;
    RecommendedFriendSelectListener recommendedFriendSelectListener;
    public RecommendedFriendsAdapter(Context context, ArrayList<RecommendedFriendsItem> recommendedFriendsItems, RecommendedFriendSelectListener recommendedFriendSelectListener) {
        this.context = context;
        this.recommendedFriendsItems = recommendedFriendsItems;
        this.recommendedFriendSelectListener = recommendedFriendSelectListener;
    }
    
    public class FriendsViewHolder extends RecyclerView.ViewHolder {
    
        public ImageView ivUserPicture;
        public TextView tvUserName;
        public TextView tvNoEvents;
    
        public ConstraintLayout conFriend;
        public ConstraintLayout customFollowButton;
        public ImageView ivFriendStatus;
        public TextView tvFriendStatus;
        
        
        public FriendsViewHolder(View view) {
            super(view);
    
            conFriend = view.findViewById(R.id.conFriend);
            
            ivUserPicture = view.findViewById(R.id.ivUserProfile);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvNoEvents = view.findViewById(R.id.tvNoEvents);
    
            /**
             *  Custom Views **/
            customFollowButton = view.findViewById(R.id.customFollowButton);
            ivFriendStatus = view.findViewById(R.id.ivFriendStatus);
            tvFriendStatus = view.findViewById(R.id.tvFriendStatus);
            
        }
    }
    
    @Override
    public FriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FriendsViewHolder(LayoutInflater.from(context).inflate(R.layout.view_horizontal_friend_item,parent,false));
    }
    
    @Override
    public void onBindViewHolder(FriendsViewHolder holder, int position) {
    
        RecommendedFriendsItem recommendedFriendsItem = recommendedFriendsItems.get(position);
    
        Glide.with(context)
                .load(recommendedFriendsItem.getUserPicture())
                .thumbnail(0.5f)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivUserPicture);
        
        holder.tvUserName.setText(recommendedFriendsItem.getUserName());
        holder.tvNoEvents.setText(recommendedFriendsItem.getNoEvents());
        //new UIControlsUtils().resizeFrame(context,holder.frameFriend);
        
        checkMutualStatus(recommendedFriendsItem, holder);
        applyClickEvent(holder, position);
        
    }
    
    private void checkMutualStatus(RecommendedFriendsItem recommendedFriendsItem, FriendsViewHolder holder){
        if(recommendedFriendsItem.isFriend()){
            holder.customFollowButton.setBackground(context.getResources().getDrawable(R.drawable.following_user_bg));
            holder.tvFriendStatus.setText(context.getString(R.string.str_following_text));
            holder.tvFriendStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.ivFriendStatus.setImageResource(R.drawable.ic_check_followers);
            holder.ivFriendStatus.setColorFilter(context.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.customFollowButton.setBackground(context.getResources().getDrawable(R.drawable.follow_user_bg));
            holder.tvFriendStatus.setText(context.getString(R.string.str_follow_text2));
            holder.tvFriendStatus.setTextColor(context.getResources().getColor(R.color.colorRed0));
            holder.ivFriendStatus.setImageResource(R.drawable.ic_icon_plus_pink);
        }
    }
    
    public void applyClickEvent(final FriendsViewHolder holder, final int position){
    
        holder.customFollowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recommendedFriendSelectListener.onRecommendedFriendSelect(holder.customFollowButton, holder.tvFriendStatus, holder.ivFriendStatus,  position);
                notifyItemChanged(position);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return recommendedFriendsItems.size();
    }
    
    public interface RecommendedFriendSelectListener {
        void onRecommendedFriendSelect(View v, View v2, View v3, int position);
    }
}
