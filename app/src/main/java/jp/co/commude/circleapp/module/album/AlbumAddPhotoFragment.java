package jp.co.commude.circleapp.module.album;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.CommonUtils;
import jp.co.commude.circleapp.utility.PermissionUtils;

public class AlbumAddPhotoFragment extends Fragment implements View.OnClickListener {
    
    ArrayList<AlbumPictureItem> albumPictureItems;
    AlbumListAdapter albumListAdapter;
    RecyclerView rvAlbum;
    ImageView ivAddPhoto;
    
    public AlbumAddPhotoFragment() {
    }
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_album_add_photo,container,false);
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        rvAlbum = (RecyclerView) view.findViewById(R.id.rvGridAlbum);
        
        ivAddPhoto = (ImageView) view.findViewById(R.id.ivAddPhoto);
        albumPictureItems = new ArrayList<AlbumPictureItem>();
        
        albumPictureItems.add(new AlbumPictureItem(R.drawable.summer));
        albumPictureItems.add(new AlbumPictureItem(R.drawable.strawberry));
        albumPictureItems.add(new AlbumPictureItem(R.drawable.palay));
        albumPictureItems.add(new AlbumPictureItem(R.drawable.lente));
        
        albumListAdapter = new AlbumListAdapter(getActivity(),albumPictureItems,1);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),3);
        
        rvAlbum.setLayoutManager(layoutManager);
        rvAlbum.setItemAnimator(new DefaultItemAnimator());
        rvAlbum.setAdapter(albumListAdapter);
        albumListAdapter.notifyDataSetChanged();
        
        ivAddPhoto.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivAddPhoto:
                // Location permission has not been granted yet, request it.
                checkPermission();
                
                break;
        }
    }
    
    private void checkPermission(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            
            PermissionUtils.checkMultiplePermission(getActivity(), PermissionUtils.MULTIPLE_PERMISSION_REQUEST_CODE,getString(R.string.dialog_storage_permission_message),
                    getString(R.string.dialog_storage_permission_message), new String[] {
                            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA});
        } else {
            
            CommonUtils.showBottomSheetCameraLibrary(getActivity());
        }
    }
}