package jp.co.commude.circleapp.module.search_map;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.events.EventDetailsActivity;
import jp.co.commude.circleapp.utility.AndroidMapUtils;
import jp.co.commude.circleapp.utility.PermissionUtils;

public class SearchOnMapActivity extends AppCompatActivity
        implements OnMapReadyCallback, EventOnMapAdapter.OnSelectEventListener, View.OnClickListener {
    
    public static final String TAG = "SearchOnMapActivity";
    
    private GoogleMap mMap;
    protected GeoDataClient mGeoDataClient;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    
    private FusedLocationProviderClient mFusedLocationClient;
    
    double lat, lng;
    LatLngBounds bound;
    
    AutoCompleteTextView actSearch;
    RecyclerView rvEventViews;
    PlaceAutocompleteAdapter mAdapter;
    ArrayList<EventonMapItem> eventonMapItems;
    
    boolean isGPSEnabled, isNetworkEnabled;
    
    ImageView ivBackpress;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_on_maps);
        
        /** Instantiate services  **/
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        
        mapFragment.getMapAsync(this);
        
        // Construct a GeoDataClient for the Google Places API for Android.
        mGeoDataClient = Places.getGeoDataClient(this, null);
        
        initActivity();
    }
    
    private void initActivity(){
        
        actSearch = (AutoCompleteTextView) findViewById(R.id.etSearchView);
        
        bound = AndroidMapUtils.boundsWithCenterAndLatLngDistance(new LatLng(lat,lng), 10000,10000);
        
        mAdapter = new PlaceAutocompleteAdapter(this, mGeoDataClient, bound, null);
        actSearch.setAdapter(mAdapter);
        
        rvEventViews = (RecyclerView) findViewById(R.id.rvEventViews);
        rvEventViews.setHasFixedSize(true);
        
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        rvEventViews.setLayoutManager(linearLayoutManager);
        
        eventonMapItems = new ArrayList<>();
        
        eventonMapItems.add(new EventonMapItem(R.color.colorRed2,getString(R.string.str_event_type),getString(R.string.str_event_title_in_mesg),getString(R.string.str_time_field_default)));
        eventonMapItems.add(new EventonMapItem(R.color.bg_action_mode,getString(R.string.str_event_type),getString(R.string.str_event_title_in_mesg),getString(R.string.str_time_field_default)));
        eventonMapItems.add(new EventonMapItem(R.color.color_static,getString(R.string.str_event_type),getString(R.string.str_event_title_in_mesg),getString(R.string.str_time_field_default)));
        eventonMapItems.add(new EventonMapItem(R.color.color_pressed,getString(R.string.str_event_type),getString(R.string.str_event_title_in_mesg),getString(R.string.str_time_field_default)));
        
        EventOnMapAdapter eventOnMapAdapter = new EventOnMapAdapter(this,eventonMapItems,this);
        rvEventViews.setAdapter(eventOnMapAdapter);
        rvEventViews.setHasFixedSize(true);
        rvEventViews.setItemAnimator(new DefaultItemAnimator());
        eventOnMapAdapter.notifyDataSetChanged();
        
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        
        ivBackpress.setOnClickListener(this);
    }
    
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackpress:
                onBackPressed();
                break;
        }
        
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    
    private OnCompleteListener<PlaceBufferResponse> mUpdatePlaceDetailsCallback
            = new OnCompleteListener<PlaceBufferResponse>() {
        @Override
        public void onComplete(Task<PlaceBufferResponse> task) {
            try {
                PlaceBufferResponse places = task.getResult();
                
                // Get the Place object from the buffer.
                final Place place = places.get(0);
                
                // Format details of the place for display and show it in a TextView.
                /*mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
                        place.getId(), place.getAddress(), place.getPhoneNumber(),
                        place.getWebsiteUri()));*/
                
                // Display the third party attributions if set.
                final CharSequence thirdPartyAttribution = places.getAttributions();
                if (thirdPartyAttribution == null) {
                    //mPlaceDetailsAttribution.setVisibility(View.GONE);
                } else {
                    /*mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
                    mPlaceDetailsAttribution.setText(
                            Html.fromHtml(thirdPartyAttribution.toString()));*/
                }
                
                Log.i(TAG, "Place details received: " + place.getName());
                
                places.release();
            } catch (RuntimeRemoteException e) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete.", e);
                return;
            }
        }
    };
    
    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Log.e(TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
    
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            /** Create permission prompt **/
            PermissionUtils.checkMultiplePermission(this, PermissionUtils.MULTIPLE_PERMISSION_LOCATION_REQUEST_CODE,getString(R.string.dialog_location_permission_message),
                    getString(R.string.dialog_location_permission_message), new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION});
            
        }   else if (googleMap != null ){
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
                    
                    /*LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE); Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if(location != null) { lat = location.getLatitude(); lng = location.getLongitude(); }*/
            
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                lat = location.getLatitude();
                                lng = location.getLongitude();
                                bound = AndroidMapUtils.boundsWithCenterAndLatLngDistance(new LatLng(lat,lng), 10000,10000);
                            }
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng), 8));
                            mMap.addMarker(new MarkerOptions()
                                    .snippet("")
                                    .position(new LatLng(14.5481417,121.0468227))
                                    .icon(AndroidMapUtils.vectorToBitmap(SearchOnMapActivity.this,R.drawable.ic_maps_and_flags, getResources().getColor(R.color.colorRed0))));
                            
                            mMap.addMarker(new MarkerOptions()
                                    .snippet("")
                                    .position(new LatLng(14.5631377,121.0269513))
                                    .icon(AndroidMapUtils.vectorToBitmap(SearchOnMapActivity.this,R.drawable.ic_maps_and_flags, getResources().getColor(R.color.colorRed0))));
                            
                            mMap.addMarker(new MarkerOptions()
                                    .snippet("")
                                    .position(new LatLng(14.5392919,120.978066))
                                    .icon(AndroidMapUtils.vectorToBitmap(SearchOnMapActivity.this,R.drawable.ic_maps_and_flags, getResources().getColor(R.color.colorRed0))));
                            
                            mMap.addMarker(new MarkerOptions()
                                    .snippet("")
                                    .position(new LatLng(34.689369,135.846434))
                                    .icon(AndroidMapUtils.vectorToBitmap(SearchOnMapActivity.this,R.drawable.ic_maps_and_flags, getResources().getColor(R.color.colorRed0))));
                        }
                    });
        }
    }
    
    public void loadMap(){
    
    }
    
    @Override
    public void onSelectEvent(View v, int position) {
        EventonMapItem eventonMapItem = eventonMapItems.get(position);
        
        getWindow().setAllowEnterTransitionOverlap(true);
        getWindow().setAllowReturnTransitionOverlap(true);
        
        View constraintLayout = v;
        String transitionName = getString(R.string.set_trans_to_feed);
        
        ActivityOptions transiActivityOptions = ActivityOptions.makeSceneTransitionAnimation(this, constraintLayout, transitionName);
        
        Transition transitionSlideDown = TransitionInflater.from(getApplicationContext()).inflateTransition(R.transition.slide_from_bottom);
        
        getWindow().setEnterTransition(transitionSlideDown);
        Intent intent = new Intent(this, EventDetailsActivity.class);
        startActivity(intent,transiActivityOptions.toBundle());
    }
}
