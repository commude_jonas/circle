package jp.co.commude.circleapp.module.message;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.message.ChatMessageItem;

/**
 * Created by vic_villanueva on 28/02/2018.
 */

public class ChatMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    
    Context context;
    ArrayList<ChatMessageItem> chatMessageItems;
    
    public ChatMessageAdapter(Context context, ArrayList<ChatMessageItem> chatMessageItems) {
        this.context = context;
        this.chatMessageItems = chatMessageItems;
    }
    
    public static class SenderViewHolder extends RecyclerView.ViewHolder {
    
        TextView tvMessageDetails;
        TextView tvSender;
        TextView tvDateSent;
        ConstraintLayout constraintLayout;
        
        public SenderViewHolder(View itemView) {
            super(itemView);
            tvMessageDetails =  itemView.findViewById(R.id.tvMessageDetails);
            tvDateSent =  itemView.findViewById(R.id.tvDateSent);
            constraintLayout = itemView.findViewById(R.id.containerMsg);
        }
    }
    public static class RecepientViewHolder extends RecyclerView.ViewHolder {
        
        TextView tvMessageDetails;
        TextView tvRecepient;
        TextView tvDateSent;
        ConstraintLayout constraintLayout;
        
        public RecepientViewHolder(View itemView) {
            super(itemView);
            tvMessageDetails =  itemView.findViewById(R.id.tvMessageDetails);
            tvDateSent =  itemView.findViewById(R.id.tvDateSent);
            constraintLayout = itemView.findViewById(R.id.containerMsg);
        }
    }
    
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ChatMessageItem.MSG_TYPE_SENDER:
                return new SenderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_chat_1,parent,false));
            case ChatMessageItem.MSG_TYPE_RECEPIENT:
                return new RecepientViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_chat_2,parent,false));
             
                default:
                    return null;
        }
    }
    
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatMessageItem chatMessageItem = chatMessageItems.get(position);
        if (chatMessageItem != null) {
            switch (holder.getItemViewType()) {
                case ChatMessageItem.MSG_TYPE_SENDER:
                    SenderViewHolder viewHolder1 = (SenderViewHolder) holder;
            
                    viewHolder1.tvMessageDetails.setText(chatMessageItem.getMessageDetails());
                    viewHolder1.tvDateSent.setText(chatMessageItem.getDateSent());
            
                    break;
                case ChatMessageItem.MSG_TYPE_RECEPIENT:
                    RecepientViewHolder viewHolder2 = (RecepientViewHolder) holder;
            
                    viewHolder2.tvMessageDetails.setText(chatMessageItem.getMessageDetails());
                    viewHolder2.tvDateSent.setText(chatMessageItem.getDateSent());
                    break;
            }
        }
    }
    
    @Override
    public int getItemViewType(int position) {
    
        ChatMessageItem chatMessageItem = chatMessageItems.get(position);
        
        switch (chatMessageItem.getMessageType()){
            case 0:
                return ChatMessageItem.MSG_TYPE_SENDER;
            case 1:
                return ChatMessageItem.MSG_TYPE_RECEPIENT;
                
            default:
                return  -1;
        }
    }
    
    @Override
    public int getItemCount() {
        return chatMessageItems == null ? 0 : chatMessageItems.size();
    }
}
