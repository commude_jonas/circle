package jp.co.commude.circleapp.module.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.transition.Fade;
import android.support.transition.TransitionManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.splash_first_screen.FirstTimeUserActivity;
import jp.co.commude.circleapp.module.main.MainActivity;
import jp.co.commude.circleapp.module.registration.UserRegistrationPageActivity;

/**
 * Created by vic_villanueva on 18/01/2018.
 */

public class UserLoginPageActivity extends AppCompatActivity
        implements View.OnClickListener,View.OnFocusChangeListener {

    Toolbar toolbar;
    
    ConstraintLayout login;
    
    InputMethodManager input;
    
    TextView btRegister;
    EditText etUserEmail, etPassword;
    ImageView ivBackPress,ivLoginLogo;
    
    LinearLayout containerSocial;
    RelativeLayout containerRegistration;
    
    ConstraintLayout parentLayout;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        
        ivBackPress = (ImageView) findViewById(R.id.ivBackpress);
        ivLoginLogo = (ImageView) findViewById(R.id.ivLoginLogo);
        
        //new UIControlsUtils().resizeImageByResolution(this, ivLoginLogo, 1.0, 1.0);
        
        etUserEmail = (EditText) findViewById(R.id.etUserEmail);
        etPassword = (EditText) findViewById(R.id.etUserPassword);
        
        login = (ConstraintLayout) findViewById(R.id.login);
        
        btRegister = (TextView) findViewById(R.id.btRegisterHere);

        ivBackPress.setOnClickListener(this);
        etUserEmail.setOnFocusChangeListener(this);
        etPassword.setOnFocusChangeListener(this);
        login.setOnClickListener(this);
        btRegister.setOnClickListener(this);
        
        containerSocial = (LinearLayout) findViewById(R.id.containerSocial);
        containerRegistration = (RelativeLayout) findViewById(R.id.containerRegistration);
    
        input = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        
        parentLayout = (ConstraintLayout) findViewById(R.id.parentLayout);
    }
    
    private void showImplicitKeyboard(final View view, boolean hasFocus){
        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = parentLayout.getRootView().getHeight() - parentLayout.getHeight();
                if (heightDiff > dpToPx(UserLoginPageActivity.this, 200)) { // if more than 200 dp, it's probably a keyboard...
                    input.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT|InputMethodManager.SHOW_FORCED);
                } else {
                
                }
            }
        });
    }
    
    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()){
            case R.id.etUserEmail:
                if(hasFocus){
                    showImplicitKeyboard(etUserEmail, true);
                    etUserEmail.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                    etUserEmail.setTextColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    etUserEmail.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                    etUserEmail.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                
                break;
            case R.id.etUserPassword:
                if(hasFocus){
                    showImplicitKeyboard(etUserEmail, true);
                    etPassword.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                    etPassword.setTextColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    etPassword.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                    etPassword.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                break;
                default:
                    return;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                return;
            case R.id.login:
                startActivity(new Intent(UserLoginPageActivity.this,MainActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finishAfterTransition();
                return;
            case R.id.btRegisterHere:
                startActivity(new Intent(UserLoginPageActivity.this,UserRegistrationPageActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finishAfterTransition();
                return;
                default:
                    return;
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UserLoginPageActivity.this,FirstTimeUserActivity.class));
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finishAfterTransition();
    }
    
}
