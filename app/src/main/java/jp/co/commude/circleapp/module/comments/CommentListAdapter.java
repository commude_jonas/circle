package jp.co.commude.circleapp.module.comments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 06/02/2018.
 */

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.CommentViewHolder> {
    
    Context context;
    ArrayList<CommentItem> commentItems;
    int displaySize;
    public CommentListAdapter(Context context, ArrayList<CommentItem> commentItems) {
        this.context = context;
        this.commentItems = commentItems;
    }
    
    public class CommentViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUsername,tvUserComment,tvUserCommentTime;
        public ImageView ivUserPicture;
        public CommentViewHolder(View view) {
            super(view);
            tvUsername = (TextView) view.findViewById(R.id.tvUsername);
            tvUserComment = (TextView) view.findViewById(R.id.tvUserComment);
            tvUserCommentTime = (TextView) view.findViewById(R.id.tvUserCommentTime);
            ivUserPicture = (ImageView) view.findViewById(R.id.ivProfilePicture);
            
        }
    }
    
    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    
        View view = LayoutInflater.from(context).inflate(R.layout.comment_list_item,parent,false);
        
        return new CommentViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        CommentItem commentItem = commentItems.get(position);
        holder.tvUsername.setText(commentItem.getUserName());
        holder.tvUserComment.setText(commentItem.getUserComment());
        holder.tvUserCommentTime.setText(commentItem.getUserCommentTime());
        
        Glide.with(context)
                .load(commentItem.getUserPicture())
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivUserPicture);
        
    }
    
    @Override
    public int getItemCount() {
        if(displaySize > commentItems.size()) {
            return commentItems.size();
        }
        return displaySize;
    }
    
    public void setDisplaySize(int displaySize) {
        this.displaySize = displaySize;
        notifyDataSetChanged();
    }
    
}
