package jp.co.commude.circleapp.module.search_feeds;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.category.CategoryItem;
import jp.co.commude.circleapp.utility.UIControlsUtils;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 19/01/2018.
 */

public class SearchCategoryListAdapter extends RecyclerView.Adapter<SearchCategoryListAdapter.ItemViewHolder> {

    Context context;
    ArrayList<CategoryItem> categoryItems;
    CategoryListener categoryListener;

    private SparseBooleanArray selectedItems;

    private static int currentSelectedIndex = -1;

    public SearchCategoryListAdapter(Context context, ArrayList<CategoryItem> categoryItems, CategoryListener categoryListener) {
        this.context = context;
        this.categoryItems = categoryItems;
        this.categoryListener = categoryListener;
        selectedItems = new SparseBooleanArray();
    }

    public static class ItemViewHolder extends  RecyclerView.ViewHolder {

        public ImageView ivCategoryPic;
        public TextView tvCategoryName;
        public ImageView ivToggle;

        public ItemViewHolder(View view) {
            super(view);
            ivCategoryPic = (ImageView) view.findViewById(R.id.itemImage);
            tvCategoryName = (TextView) view.findViewById(R.id.itemText);
            ivToggle = (ImageView) view.findViewById(R.id.itemToggle);

        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.view_user_category_item,parent,false);
            return new ItemViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {

        CategoryItem categoryItem = categoryItems.get(position);
        
        holder.tvCategoryName.setText(categoryItem.getCategoryName());
        
        Bitmap ShrinkedImage = new UIControlsUtils().generateScaledBitmap(context,categoryItem.getCategoryImgrsc());
        Glide.with(context)
                .load(ShrinkedImage)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .into(holder.ivCategoryPic);
    
        holder.itemView.setActivated(selectedItems.get(position, false));
        
        //new CategoryImageLoaderView(context,holder.ivCategoryPic,categoryItem.getCategoryImgrsc()).execute();
        applySelectedCategory(holder, position);
        applyClickEvent(holder,position);
        
    }

    @Override
    public int getItemCount() {
        return categoryItems.size();
    }

    public int getSelectedItemCount(){
        return selectedItems.size();
    }
    
    private void applyClickEvent(ItemViewHolder viewHolder, final int position){
        viewHolder.tvCategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryListener.onCategoryClicked(position);
            }
        });
    }
    
    private void applySelectedCategory(ItemViewHolder holder, int position) {
        if (!selectedItems.get(position, false)) {
            holder.ivToggle.setVisibility(View.GONE);
            holder.ivCategoryPic.setColorFilter(ContextCompat.getColor(context, R.color.color_gray_4033), PorterDuff.Mode.SRC_ATOP);
            /*R.color.color_gray_4033);*/
            
            //holder.tvCategoryName.setBackgroundResource(R.drawable.text_view_background);
            holder.tvCategoryName.setTypeface(null, Typeface.NORMAL);
            holder.tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.colorWhiteF0));
        } else {
            holder.ivToggle.setVisibility(View.VISIBLE);
            holder.ivCategoryPic.setColorFilter(ContextCompat.getColor(context, R.color.colorRedTransparent0), android.graphics.PorterDuff.Mode.SRC_ATOP);
            //holder.ivCategoryPic.setColorFilter(R.color.color_red_59FF);
            //holder.tvCategoryName.setBackgroundResource(R.drawable.text_view_background_selected);
            holder.tvCategoryName.setTypeface(null, Typeface.BOLD);
            holder.tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.colorWhiteF0));
        }
    }

    private void applySelected(ItemViewHolder holder, int position) {
        if (selectedItems.get(position, false)) {
            holder.ivToggle.setVisibility(View.VISIBLE);
        } else {
            holder.ivToggle.setVisibility(View.GONE);
        }
    }

    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            //animationItemsIndex.delete(pos);
        } else {
            selectedItems.put(pos, true);
            //animationItemsIndex.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        categoryItems.remove(position);
        resetCurrentIndex();
    }

    public void removeAllData(){
        for(int i=0;i<categoryItems.size();i++){
            categoryItems.remove(i);
        }
    }
    
    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public interface CategoryListener {
        void onCategoryClicked(int position);
    }

}
