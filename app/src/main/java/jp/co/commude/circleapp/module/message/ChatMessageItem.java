package jp.co.commude.circleapp.module.message;

/**
 * Created by vic_villanueva on 28/02/2018.
 */

public class ChatMessageItem {
    
    public static final int MSG_TYPE_SENDER = 0;
    public static final int MSG_TYPE_RECEPIENT = 1;
    
    int messageID;
    int messageType;
    String messageAddress;
    String messageDetails;
    String dateSent;
    String username;
    
    boolean read;
    
    public ChatMessageItem(int messageType, int messageID, String messageDetails, String dateSent) {
        this.messageType = messageType;
        this.messageID = messageID;
        this.messageDetails = messageDetails;
        this.dateSent = dateSent;
    }
    
    public ChatMessageItem(int messageType, int messageID, String messageAddress, String messageDetails, String dateSent, String username) {
        this.messageID = messageID;
        this.messageType = messageType;
        this.messageAddress = messageAddress;
        this.messageDetails = messageDetails;
        this.dateSent = dateSent;
        this.username = username;
    }
    
    public int getMessageID() {
        return messageID;
    }
    
    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }
    
    public int getMessageType() {
        return messageType;
    }
    
    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }
    
    public String getMessageAddress() {
        return messageAddress;
    }
    
    public void setMessageAddress(String messageAddress) {
        this.messageAddress = messageAddress;
    }
    
    public String getMessageDetails() {
        return messageDetails;
    }
    
    public void setMessageDetails(String messageDetails) {
        this.messageDetails = messageDetails;
    }
    
    public String getDateSent() {
        return dateSent;
    }
    
    public void setDateSent(String dateSent) {
        this.dateSent = dateSent;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public boolean isRead() {
        return read;
    }
    
    public void setRead(boolean read) {
        this.read = read;
    }
}
