package jp.co.commude.circleapp.customviews;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by vic_villanueva on 24/01/2018.
 */

public class CustomEditTextView extends android.support.v7.widget.AppCompatEditText {

    int[][] states = new int[][] {
            new int[] { android.R.attr.state_enabled}, // enabled
            new int[] {-android.R.attr.state_enabled}, // disabled
            new int[] {-android.R.attr.state_checked}, // unchecked
            new int[] { android.R.attr.state_pressed}  // pressed
    };

    int[] colors = new int[] {
            Color.BLACK,
            Color.RED,
            Color.GREEN,
            Color.BLUE
    };

    public CustomEditTextView(Context context) {
        super(context);
        this.setTextColor(new ColorStateList(states,colors));
    }

    public CustomEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTextColor(new ColorStateList(states,colors));
    }

    public CustomEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTextColor(new ColorStateList(states,colors));

    }
}
