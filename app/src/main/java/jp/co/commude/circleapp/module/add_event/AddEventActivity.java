package jp.co.commude.circleapp.module.add_event;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.beloo.widget.chipslayoutmanager.gravity.IChildGravityResolver;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;
import jp.co.commude.circleapp.module.camera.CustomCameraActivity;
import jp.co.commude.circleapp.module.category.CategoryItem;
import jp.co.commude.circleapp.module.gallery.GalleryActivity;
import jp.co.commude.circleapp.module.main.MainActivity;
import jp.co.commude.circleapp.utility.CameraUtils;
import jp.co.commude.circleapp.utility.CommonUtils;
import jp.co.commude.circleapp.utility.PermissionUtils;

/**
 * Created by vic_villanueva on 22/02/2018.
 */

public class AddEventActivity extends AppCompatActivity implements View.OnClickListener,
        CategoryChipsListAdapter.ChipSelectListener, CategoryChipsAddedAdapter.ChipUnselectListener {
    
    private static final String TAG = "AddEventActivity";
    
    public static final int RESULT_INTENT_CAMERA = 0;
    public static final int RESULT_INTENT_LIBRARY = 1;
    public static final int REQUEST_MAP_LOCATION = 2;
    
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    
    ImageView ivBackpress;
    ImageView ivChooseCategory;
    
    // view container add event
    View addEventPhotos;
    ImageView ivAddEvent;
    
    RecyclerView rvCategories;
    
    TextView tvComplete;
    
    //Layout Manager
    ChipsLayoutManager chipsLayoutManager;
    LinearLayoutManager linearLayoutManager;
    
    //View Models inflater
    CategoryChipsAddedAdapter categoryChipsAddedAdapter;
    CategoryChipsListAdapter categoryChipsListAdapter;
    
    ArrayList<CategoryItem> categoryItems;
    ArrayList<CategoryItem> categoryItems2;
    
    TextView tvSelectedPlace;
    
    //BottomSheet Views
    BottomSheetDialog dialog;
    View bottomsheetView;
    
    ImageView ivCancel;
    RecyclerView rvCategoryChoices;
    
    TextView tvStartDate, tvEndDate;
    TextView tvStartDayOftheWeek1, tvEndDayoftheWeek2, tvTime1, tvTime2;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        setContentView(R.layout.activity_add_event_page);
        
        categoryItems = new ArrayList<>();
        categoryItems2 = new ArrayList<>();
        
        chipsLayoutManager = ChipsLayoutManager.newBuilder(this)
                .setScrollingEnabled(true)
                //set maximum views count in a particular row
                .setMaxViewsInRow(3)
                //set gravity resolver where you can determine gravity for item in position.
                //This method have priority over previous one
                .setGravityResolver(new IChildGravityResolver() {
                    @Override
                    public int getItemGravity(int position) {
                        return Gravity.CENTER;
                    }
                })
                .setOrientation(ChipsLayoutManager.HORIZONTAL)
                //row strategy for views in completed row, could be
                //STRATEGY_DEFAULT, STRATEGY_FILL_VIEW, STRATEGY_FILL_SPACE or STRATEGY_CENTER
                .setRowStrategy(ChipsLayoutManager.STRATEGY_DEFAULT)
                .build();
    
        categoryChipsAddedAdapter = new CategoryChipsAddedAdapter(this, categoryItems,this);
        
        init();
        
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        checkPermission();
    }
    
    private void checkPermission(){
        PermissionUtils.checkMultiplePermission(this, PermissionUtils.MULTIPLE_PERMISSION_REQUEST_CODE,getString(R.string.dialog_storage_permission_message),
                getString(R.string.dialog_storage_permission_message), new String[] {
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA});
    }
    
    void init(){
        
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
    
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        tvComplete = (TextView) findViewById(R.id.tvComplete);
    
        //addEventPhotos = findViewById(R.id.addEventPhotos);
        ivAddEvent = (ImageView) findViewById(R.id.ivAddEvent);
        ivChooseCategory = (ImageView) findViewById(R.id.ivChooseCategory);
        
        rvCategories = (RecyclerView) findViewById(R.id.rvCategories);
        
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.apladdevent);
    
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if ((collapsingToolbarLayout.getHeight() + verticalOffset) < (2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout))) {
                    ivBackpress.setColorFilter(getResources().getColor(R.color.colorBlack2), PorterDuff.Mode.SRC_ATOP);
                    tvComplete.setTextColor(getResources().getColor(R.color.colorBlack2));
                } else {
                    ivBackpress.setColorFilter(getResources().getColor(R.color.colorWhite0), PorterDuff.Mode.SRC_ATOP);
                    tvComplete.setTextColor(getResources().getColor(R.color.colorWhite0));
                }
            }
        });
    
        tvSelectedPlace = (TextView) findViewById(R.id.tvSelectedPlace);
        
        ivBackpress.setOnClickListener(this);
        tvComplete.setOnClickListener(this);
        
        ivAddEvent.setOnClickListener(this);
        //addEventPhotos.setOnClickListener(this);
    
        ivChooseCategory.setOnClickListener(this);
    
        rvCategories.setLayoutManager(chipsLayoutManager);
        rvCategories.setHasFixedSize(true);
        //ViewCompat.setLayoutDirection(rvCategories, ViewCompat.LAYOUT_DIRECTION_RTL);
        rvCategories.setItemAnimator(new DefaultItemAnimator());
        rvCategories.setAdapter(categoryChipsAddedAdapter);
        categoryChipsAddedAdapter.notifyDataSetChanged();
        
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvEndDate = (TextView) findViewById(R.id.tvEndDate);
        
        tvStartDayOftheWeek1 = (TextView) findViewById(R.id.tvDateDayOftheWeek1);
        tvEndDayoftheWeek2 = (TextView) findViewById(R.id.tvDateDayOftheWeek2);
    
        tvTime1 = (TextView) findViewById(R.id.time1);
        tvTime2 = (TextView) findViewById(R.id.time2);
        
        tvStartDayOftheWeek1.setOnClickListener(this);
        tvEndDayoftheWeek2.setOnClickListener(this);
        tvTime1.setOnClickListener(this);
        tvTime2.setOnClickListener(this);
    
        tvStartDate.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
    }
    
    private void showBottomCategoryChoices(){
    
        bottomsheetView = getLayoutInflater().inflate(R.layout.bottomsheet_category_choices, null);
    
        dialog = new BottomSheetDialog(this);
        dialog.setContentView(bottomsheetView);
        dialog.show();
    
        ivCancel = (ImageView) dialog.findViewById(R.id.ivCancel);
        rvCategoryChoices = (RecyclerView) dialog.findViewById(R.id.rvCategoriesChoices);
    
        ivCancel.setOnClickListener(this);
        
        String[] strCat = Globals.string_array_resources(this,R.array.str_res);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    
        categoryItems2 = new ArrayList<>();
        
        if(categoryItems.size() == 0){
            for(int i=0;i<strCat.length;i++){
                categoryItems2.add(new CategoryItem(strCat[i]));
            }
        } else {
    
            List<String> catName = new LinkedList<>();
            for (CategoryItem categoryName : categoryItems) {
                catName.add(categoryName.getCategoryName());
            }
            
            for(int j=0;j<strCat.length;j++) {
                
                if(catName.contains(strCat[j].toString())){
                
                } else {
                    categoryItems2.add(new CategoryItem(strCat[j]));
                }
            }
            
        }
        
        categoryChipsListAdapter = new CategoryChipsListAdapter(this, categoryItems2, this);
    
        rvCategoryChoices.setHasFixedSize(true);
        rvCategoryChoices.setLayoutManager(linearLayoutManager);
        rvCategoryChoices.setItemAnimator(new DefaultItemAnimator());
        rvCategoryChoices.setAdapter(categoryChipsListAdapter);
        
        categoryChipsListAdapter.notifyDataSetChanged();
        
    }
    
    public void showDialogDatePicker(){
        
        dialog = new BottomSheetDialog(this, R.style.DialogBottomsheet);
        
        bottomsheetView = getLayoutInflater().inflate(R.layout.bottomsheet_date_picker, null);
        DatePicker datePicker1 = (DatePicker) bottomsheetView.findViewById(R.id.datePicker2);
        
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        
        datePicker1.updateDate(year,month,day);
        
        dialog.setContentView(bottomsheetView);
        dialog.show();
    }
    
    public void showDialogTimePicker(){
        
        dialog = new BottomSheetDialog(this, R.style.DialogBottomsheet);
        
        bottomsheetView = getLayoutInflater().inflate(R.layout.bottomsheet_time_picker, null);
        TimePicker timePicker = (TimePicker) bottomsheetView.findViewById(R.id.timePicker1);
    
        dialog.setContentView(bottomsheetView);
        dialog.show();
    }
    
    @Override
    public void onChipSelected(int position) {
        
        if (categoryChipsListAdapter.getSelectedItemCount() > 0) {
        
        } else {
            // read the message which removes bold from the row
            CategoryItem categoryItem2 = categoryItems2.get(position);
            categoryItem2.setSelected(true);
            categoryItems2.set(position, categoryItem2);
            categoryChipsListAdapter.notifyDataSetChanged();
            
            categoryItems.add(new CategoryItem(categoryItem2.getCategoryName()));
            categoryChipsAddedAdapter.notifyDataSetChanged();
            
        }
    }
    
    @Override
    public void onChipUnselected(int position) {
        categoryItems.remove(position);
        categoryChipsAddedAdapter.notifyDataSetChanged();
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.tvComplete:
                onBackPressed();
                break;
            case R.id.addEventPhotos:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
        
                    PermissionUtils.checkMultiplePermission(this, PermissionUtils.MULTIPLE_PERMISSION_REQUEST_CODE,getString(R.string.dialog_storage_permission_message),
                            getString(R.string.dialog_storage_permission_message), new String[] {
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA});
                } else {
        
                    CommonUtils.showBottomSheetCameraLibrary(this);
                }
                
                break;
                
            case R.id.ivAddEvent:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
        
                    PermissionUtils.checkMultiplePermission(this, PermissionUtils.MULTIPLE_PERMISSION_REQUEST_CODE,getString(R.string.dialog_storage_permission_message),
                            getString(R.string.dialog_storage_permission_message), new String[] {
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA});
                } else {
        
                    CommonUtils.showBottomSheetCameraLibrary(this);
                }
                break;
                
            case R.id.ivChooseCategory:
                showBottomCategoryChoices();
                break;
            case R.id.tvGoCamera:
                if(CameraUtils.checkCameraHardware(this)) {
                    Intent intent = new Intent(AddEventActivity.this, CustomCameraActivity.class);
                    startActivityForResult(intent, RESULT_INTENT_CAMERA);
                    Log.d(TAG, "Camera is already granted!");
                } else {
                    Toast.makeText(this, "Camera is not available for now", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Camera is not available for now");
                }
                break;
            case R.id.tvGoLibrary:
                Intent intent = new Intent(this, GalleryActivity.class);
                startActivityForResult(intent, RESULT_INTENT_LIBRARY);
                break;
            case R.id.tvCancel:
                dialog.dismiss();
                break;
            case R.id.ivCancel:
                dialog.dismiss();
                break;
            case R.id.tvStartDate:
                showDialogDatePicker();
                break;
            case R.id.tvEndDate:
                showDialogDatePicker();
                break;
            case R.id.tvDateDayOftheWeek1:
                showDialogDatePicker();
                break;
            case R.id.tvDateDayOftheWeek2:
                showDialogDatePicker();
                break;
            case R.id.time1:
                showDialogTimePicker();
                break;
            case R.id.time2:
                showDialogTimePicker();
                break;
        }
        
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("fragment",0);
        intent.putExtra("fragmentName", Globals.FEEDS);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        startActivity(intent);
        finishAfterTransition();
    }
    
    public void onMarkerPin(View v){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        
                PermissionUtils.checkMultiplePermission(this, PermissionUtils.MULTIPLE_PERMISSION_LOCATION_REQUEST_CODE,
                        getString(R.string.dialog_storage_permission_message), getString(R.string.dialog_storage_permission_message),
                        new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION});
        } else {
            Intent intent = new Intent(this, PickLocationOnMapActivity.class);
            startActivityForResult(intent,REQUEST_MAP_LOCATION);
        }
        
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0){
        
        } else if(requestCode == 1){
        
        } else if(requestCode == 2) {
            String map_location = data.getExtras().get(PickLocationOnMapActivity.MAP_LOCATION).toString();
            tvSelectedPlace.setText(map_location);
        } else {
        
        }
    }
    
    public static void open(Activity act){
        act.startActivity(new Intent(act, AddEventActivity.class));
    }
    
}
