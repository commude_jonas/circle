package jp.co.commude.circleapp.module.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.confirm.UserConfirmationPageActivity;
import jp.co.commude.circleapp.module.login.UserLoginPageActivity;

/**
 * Created by vic_villanueva on 18/01/2018.
 */

public class UserRegistrationPageActivity extends AppCompatActivity implements View.OnClickListener,View.OnFocusChangeListener {

    Toolbar toolbar;
    ImageView ivBackpress;
    ConstraintLayout btRegister;
    EditText etUsername, etUserEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
    
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        btRegister = (ConstraintLayout) findViewById(R.id.register);
        
        etUsername = (EditText) findViewById(R.id.etUsername);
        etUserEmail = (EditText) findViewById(R.id.etUserEmail);
        
        ivBackpress.setOnClickListener(this);
        btRegister.setOnClickListener(this);
        
        etUsername.setOnFocusChangeListener(this);
        etUserEmail.setOnFocusChangeListener(this);
        
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()){
            case R.id.etUserEmail:
                if(hasFocus){
                    etUserEmail.setHintTextColor(getResources().getColor(R.color.color_pink_2));
                    etUserEmail.setTextColor(getResources().getColor(R.color.colorRed0));
                } else {
                    etUserEmail.setHintTextColor(getResources().getColor(R.color.colorGray1));
                    etUserEmail.setTextColor(getResources().getColor(R.color.colorGray1));
                }
                break;
            case R.id.etUsername:
                if(hasFocus){
                    etUsername.setHintTextColor(getResources().getColor(R.color.color_pink_2));
                    etUsername.setTextColor(getResources().getColor(R.color.colorRed0));
                } else {
                    etUsername.setHintTextColor(getResources().getColor(R.color.colorGray1));
                    etUsername.setTextColor(getResources().getColor(R.color.colorGray1));
                }
                break;
            default:
                return;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                return;
            case R.id.register:
                startActivity(new Intent(UserRegistrationPageActivity.this,UserConfirmationPageActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finishAfterTransition();
                return;
                default:
                    return;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UserRegistrationPageActivity.this,UserLoginPageActivity.class));
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
        finishAfterTransition();
    }
}
