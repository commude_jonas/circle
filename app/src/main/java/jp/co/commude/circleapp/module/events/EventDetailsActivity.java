package jp.co.commude.circleapp.module.events;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.like.LikeButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;
import jp.co.commude.circleapp.module.album.AlbumActivity;
import jp.co.commude.circleapp.module.album.AlbumListAdapter;
import jp.co.commude.circleapp.module.album.AlbumPictureItem;
import jp.co.commude.circleapp.module.comments.CommentItem;
import jp.co.commude.circleapp.module.comments.CommentListAdapter;
import jp.co.commude.circleapp.module.comments.EventCommentsListActivity;
import jp.co.commude.circleapp.module.main.MainActivity;
import jp.co.commude.circleapp.module.message.MessageActivity;
import jp.co.commude.circleapp.module.participants.ParticipantItem;
import jp.co.commude.circleapp.module.participants.ParticipantListAdapter;
import jp.co.commude.circleapp.module.participants.SheetParticipantListAdapter;
import jp.co.commude.circleapp.module.profile.UserProfileActivity;
import jp.co.commude.circleapp.utility.AndroidMapUtils;
import jp.co.commude.circleapp.utility.PermissionUtils;
import jp.co.commude.circleapp.utility.SpacesItemDecoration;
import jp.co.commude.circleapp.utility.SpanningLinearLayoutManager;
import jp.co.commude.circleapp.utility.UIControlsUtils;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class EventDetailsActivity extends AppCompatActivity
        implements ParticipantListAdapter.OnParticipantsListeners, View.OnClickListener, SheetParticipantListAdapter.OnParticipantsProfileListeners {
    
    CoordinatorLayout coordinatorLayout;
    
    MapView mapview;
    
    private static final String MAPVIEW_BUNDLE_KEY = "AIzaSyDj-bcqH5pdZmS0bzHuveSPxDzY1u6NQv4";
    
    LocationManager locationManager;
    double lat, lng;
    
    boolean isGPSEnabled, isNetworkEnabled;
    private FusedLocationProviderClient mFusedLocationClient;
    
    ImageView ivProfilePicture;
    RecyclerView rvAlbumPictureList;
    AlbumListAdapter albumListAdapter;
    ArrayList<AlbumPictureItem> albumPictureItems;
    
    RecyclerView rvParticipantList;
    ParticipantListAdapter participantListAdapter;
    ArrayList<ParticipantItem> participantItems;
    
    RecyclerView rvCommentList;
    CommentListAdapter commentListAdapter;
    ArrayList<CommentItem> commentItems;
    
    ImageView ivCoverPhoto;
    TextView eventType;
    TextView tvJoinEvent;
    
    
    ImageView ivGotoAlbum, ivGoToComment;
    TextView tvComment;
    TextView tvAlbum;
    TextView tvMapAddress;
    
    ImageView ivBackPress, ivMessage, ivComment, ivShare;
    LikeButton ivLike;
    
    Bundle savedInstance;
    
    Handler handler;
    int intentBackground;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_details);
        savedInstance = savedInstanceState;
        init();
        
        Intent intent = getIntent();
        intentBackground = intent.getIntExtra("intentImage", 0);
        
        new UIControlsUtils().resizeImageByResolution(this, mapview, 1.0, 0.20);
        
        /** load profile image */
        //new loadAnimation().execute();
        Glide.with(EventDetailsActivity.this)
                .load(R.drawable.profile_picture)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(ivProfilePicture);
    
        /** load static data for album pictures **/
        albumPictureItems = new ArrayList<AlbumPictureItem>();
        participantItems = new ArrayList<ParticipantItem>();
        commentItems = new ArrayList<CommentItem>();
        
        handler = new Handler();
        handler.removeCallbacks(LoadingInstance);
        handler.postDelayed(LoadingInstance, 2000);
        
        tvAlbum.setOnClickListener(this);
        tvComment.setOnClickListener(this);
        tvJoinEvent.setOnClickListener(this);
        ivBackPress.setOnClickListener(this);
        ivMessage.setOnClickListener(this);
        ivComment.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        
        ivGotoAlbum.setOnClickListener(this);
        ivGoToComment.setOnClickListener(this);
    }
    
    Runnable LoadingInstance = new Runnable() {
        @Override
        public void run() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //uddate UI
                            requestServiceConfig();
                    
                            PermissionUtils.checkMultiplePermission(EventDetailsActivity.this, PermissionUtils.MULTIPLE_PERMISSION_LOCATION_REQUEST_CODE,getString(R.string.dialog_location_permission_message),
                                    getString(R.string.dialog_location_permission_message), new String[] {
                                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION});
                    
                            listLoaders();
                    
                        }
                    });
            
                }
            }).start();
        }
    };
    
    @Override
    protected void onStart() {
        super.onStart();
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(LoadingInstance);
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionUtils.MULTIPLE_PERMISSION_LOCATION_REQUEST_CODE) {
            boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            if (locationAccepted) {
                loadMap(locationAccepted);
            } else {
                Toast.makeText(this, "location is not visible!", Toast.LENGTH_SHORT).show();
            }
        }
        
    }
    
    private void loadMap(final boolean load) {
        mapview.onCreate(savedInstance);
        mapview.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(final GoogleMap googleMap) {
    
                if (load) {
                    if(googleMap != null) {
                        googleMap.setMyLocationEnabled(true);
                        googleMap.getUiSettings().setZoomGesturesEnabled(false);
                        googleMap.getUiSettings().setMapToolbarEnabled(false);
                        
                        /*LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE); Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if(location != null) { lat = location.getLatitude(); lng = location.getLongitude(); }*/
    
                        mFusedLocationClient.getLastLocation()
                                .addOnSuccessListener(EventDetailsActivity.this, new OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {
                                        // Got last known location. In some rare situations this can be null.
                                        if (location != null) {
                                            lat = location.getLatitude();
                                            lng = location.getLongitude();
                                        }
                                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 13));
    
                                        googleMap.addMarker(new MarkerOptions()
                                                .snippet("")
                                                .position(new LatLng(lat, lng))
                                                .icon(AndroidMapUtils.vectorToBitmap(EventDetailsActivity.this, R.drawable.ic_maps_and_flags, getResources().getColor(R.color.colorRed0))));
    
                                        //myLocation();
                                    }
                                });
                    } else {
                        Toast.makeText(EventDetailsActivity.this, "Location not found!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        
        mapview.onStart();
    
    }
    
    private void requestServiceConfig(){
        /** Instantiate services  **/
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        
    
    }
    
    void init(){
        
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
    
        ivCoverPhoto = (ImageView) findViewById(R.id.ivCoverPhoto);
        
        eventType = (TextView) findViewById(R.id.tvEventType);
        ivProfilePicture = (ImageView) findViewById(R.id.ivProfilePicture);
        
        /** Map View */
        mapview = (MapView) findViewById(R.id.map_container);
        tvMapAddress = (TextView) findViewById(R.id.tvMapAddress);
    
        /** RecyclerViews */
        rvAlbumPictureList = (RecyclerView) findViewById(R.id.rvHorizontalAlbum);
        rvParticipantList = (RecyclerView) findViewById(R.id.rvHorizontalAlbumParticipant);
        rvCommentList = (RecyclerView) findViewById(R.id.rvCommentList);
    
        /** Top Buttons **/
        ivBackPress = (ImageView) findViewById(R.id.ivBackpress);
        ivComment = (ImageView) findViewById(R.id.ivComment);
        ivShare = (ImageView) findViewById(R.id.ivShare);
        ivLike = (LikeButton) findViewById(R.id.ivLike);
        
        ivLike.setUnlikeDrawable(getResources().getDrawable(R.drawable.ic_white_heart));
        ivLike.setLikeDrawable(getResources().getDrawable(R.drawable.ic_filled_heart));
    
        /** message mail and comment **/
        ivMessage = (ImageView) findViewById(R.id.ivMessage);
        tvAlbum = (TextView) findViewById(R.id.tvAlbum);
        tvComment = (TextView) findViewById(R.id.tvComment);
        tvJoinEvent = (TextView) findViewById(R.id.tvJoinEvent);
        
        ivCoverPhoto.setImageResource(Globals.IMG_RECOMMENDED[intentBackground]);
        
        ivGotoAlbum = (ImageView) findViewById(R.id.ivGotoAlbum);
        ivGoToComment = (ImageView) findViewById(R.id.ivGotoComment);
        
        
        
    }
    
    
    private void listLoaders(){
    
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                //
                new ListLoader().execute();
            }
        }, 300);
        
    }
    
    private void listlayoutManager(){
        /** build views and model **/
        albumListAdapter = new AlbumListAdapter(this, albumPictureItems,0);
        LinearLayoutManager albumManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        rvAlbumPictureList.setLayoutManager(albumManager);
        rvAlbumPictureList.setItemAnimator(new DefaultItemAnimator());
        rvAlbumPictureList.setHasFixedSize(true);
        rvAlbumPictureList.setAdapter(albumListAdapter);
    
        participantListAdapter = new ParticipantListAdapter(this, participantItems, this);
        SpanningLinearLayoutManager participantManager = new SpanningLinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        rvParticipantList.setHasFixedSize(true);
        rvParticipantList.setLayoutManager(participantManager);
        rvParticipantList.setItemAnimator(new DefaultItemAnimator());
        rvParticipantList.setAdapter(participantListAdapter);
        participantListAdapter.notifyDataSetChanged();
        participantListAdapter.setDisplaySize(10);
    
        commentListAdapter = new CommentListAdapter(this, commentItems);
        LinearLayoutManager commentManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        //
        rvCommentList.setLayoutManager(commentManager);
        rvCommentList.setItemAnimator(new DefaultItemAnimator());
        rvCommentList.setAdapter(commentListAdapter);
        commentListAdapter.notifyDataSetChanged();
        commentListAdapter.setDisplaySize(2);
    
    }
    
    private void myLocation(){
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
    
        try {
            addresses = geocoder.getFromLocation( lat,lng,1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    
        String address = addresses.get(0).getAddressLine(0);
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
    
        String country = addresses.get(0).getCountryName();
        String postalcode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
        
        tvMapAddress.setText(address+", "+knownName);
    
    }
    
    public boolean isGPSEnabled() {
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return isGPSEnabled || isNetworkEnabled;
        
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }
        
        mapview.onSaveInstanceState(outState);
    }
    
    
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapview.onLowMemory();
    }
    
    @Override
    public void viewParticipants(int position) {
        if(position == 8){
            viewParticipants();
        }
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackpress:
                MainActivity.openDefault(EventDetailsActivity.this, 0, "feeds");
                break;
            case R.id.ivMessage:
                startActivity(new Intent(EventDetailsActivity.this, MessageActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                break;
            case R.id.ivComment:
                startActivity(new Intent(EventDetailsActivity.this,EventCommentsListActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                break;
            case R.id.ivShare:
                shareEventtoSocial();
                break;
            case R.id.tvAlbum:
                startActivity(new Intent(EventDetailsActivity.this,AlbumActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finish();
                break;
            case R.id.tvComment:
                startActivity(new Intent(EventDetailsActivity.this,EventCommentsListActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                break;
                
            case R.id.ivGotoAlbum:
                startActivity(new Intent(EventDetailsActivity.this,AlbumActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finish();
                break;
            case R.id.ivGotoComment:
                startActivity(new Intent(EventDetailsActivity.this,EventCommentsListActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                break;
            case R.id.btJoinEvent:
                final Toast toast = Toast.makeText(EventDetailsActivity.this, "Joined", Toast.LENGTH_SHORT);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        toast.show();
            
                    }
                }, 3000);
                break;
                
        }
    }
    
    public void shareEventtoSocial(){
        
        //PublicEventItem publicEventItem = publicEventItems.get(position);
        final BottomSheetDialog dialog;
        
        View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_social, null);
        
        ImageView ivTwitter, ivFacebook, ivGooglePlus;
        TextView btCancel;
        
        ivTwitter = (ImageView)view.findViewById(R.id.ibTwitter);
        ivFacebook = (ImageView)view.findViewById(R.id.ibFacebook);
        ivGooglePlus = (ImageView)view.findViewById(R.id.ibGooglePlus);
        btCancel = (TextView) view.findViewById(R.id.btCancel);
        
        dialog = new BottomSheetDialog(EventDetailsActivity.this, R.style.TransparentTheme);
        dialog.setContentView(view);
        dialog.show();
        
        ivTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(EventDetailsActivity.this, "Twitter shared", Toast.LENGTH_SHORT).show();
            }
        });
        
        ivFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(EventDetailsActivity.this,  "Facebook shared", Toast.LENGTH_SHORT).show();
            }
        });
        
        ivGooglePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(EventDetailsActivity.this, "Google Plus shared", Toast.LENGTH_SHORT).show();
            }
        });
        
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(EventDetailsActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }
        });
        
    }
    RecyclerView rvGridParticipants;
    public void viewParticipants(){
        final BottomSheetDialog dialog;
        
        View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_participants, null);
        
        
        rvGridParticipants = view.findViewById(R.id.rvGridParticipants);
        // run AsyncTask here.
        listsheetLoaders();
        
        //participantGridAdapter.setDisplaySize(participantItems.size());
        
        dialog = new BottomSheetDialog(EventDetailsActivity.this, R.style.TransparentTheme);
        dialog.setContentView(view);
        dialog.show();
        
        Button btJoinEvent = (Button) view.findViewById(R.id.btJoinEvent);
        btJoinEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Toast toast = Toast.makeText(EventDetailsActivity.this, "Joined", Toast.LENGTH_SHORT);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                    
                        // run AsyncTask here.
                    
                        toast.show();
                    
                    }
                }, 3000);
                
                dialog.dismiss();
            }
        });
    }
    
    private void listsheetLoaders(){
        
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                //
                new ListSheetLoader().execute();
            }
        }, 300);
        
    }
    
    @Override
    public void viewProfile(int position) {
        UserProfileActivity.open(EventDetailsActivity.this);
    }
    
    public class ListLoader extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params) {
            Globals.AlbumPictures(EventDetailsActivity.this,albumPictureItems);
            Globals.ParticipantItems(participantItems);
            Globals.CommentItems(EventDetailsActivity.this,commentItems,R.array.str_com_name,R.array.str_com_comment,R.array.str_com_date);
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            listlayoutManager();
        }
    }
    
    public class ListSheetLoader extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params) {
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            SheetParticipantListAdapter participantGridAdapter = new SheetParticipantListAdapter(EventDetailsActivity.this, participantItems,EventDetailsActivity.this);
            GridLayoutManager participantManager = new GridLayoutManager(getApplicationContext(),7);
            rvGridParticipants.setHasFixedSize(true);
            rvGridParticipants.setLayoutManager(participantManager);
            rvGridParticipants.setItemAnimator(new DefaultItemAnimator());
    
            rvGridParticipants.addItemDecoration(new SpacesItemDecoration(12));
    
            rvGridParticipants.setAdapter(participantGridAdapter);
            participantGridAdapter.notifyDataSetChanged();
        }
    }
    
}
