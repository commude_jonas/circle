package jp.co.commude.circleapp.module.splash_first_screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.UIControlsUtils;

/**
 * Created by vic_villanueva on 18/01/2018.
 */

public class SplashScreenActivity extends AppCompatActivity {


    ImageView imgSplashScreen;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_app_splash);
        
        UIControlsUtils.getDeviceScreenResolution(this);
        Log.d("SCREEN SIZE : ", UIControlsUtils.getScreenSize(this));
    
        imgSplashScreen = (ImageView) findViewById(R.id.imgSplashScreen);
    
        new UIControlsUtils().resizeImageByResolution(this, imgSplashScreen, 0.35, 0.35);
    
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
            
                // run AsyncTask here.
                startActivity(new Intent(SplashScreenActivity.this,FirstTimeUserActivity.class));
                
                finish();
            
            }
        }, 3000);
        
    }
    
}
