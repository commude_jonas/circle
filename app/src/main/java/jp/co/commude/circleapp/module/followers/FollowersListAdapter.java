package jp.co.commude.circleapp.module.followers;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 19/02/2018.
 */

public class FollowersListAdapter extends RecyclerView.Adapter<FollowersListAdapter.FollowersViewHolder> {
    
    Context context;
    ArrayList<FollowersItem> followersItems;
    int displaySize;
    
    FollowUserListener followUserListener;
    public FollowersListAdapter(Context context, ArrayList<FollowersItem> followersItems, FollowUserListener followUserListener) {
        this.context = context;
        this.followersItems = followersItems;
        this.followUserListener = followUserListener;
    }
    
    public class FollowersViewHolder extends RecyclerView.ViewHolder {
        
        public ImageView ivUserPicture;
        public TextView tvUserName;
        
        public ConstraintLayout conFollow;
        public TextView tvMutualStatus;
        public ImageView ivMutualStatus;
    
        public FollowersViewHolder(View view) {
            super(view);
        
            ivUserPicture = view.findViewById(R.id.ivUserProfile);
            tvUserName = view.findViewById(R.id.tvUserName);
            
            conFollow = view.findViewById(R.id.conFollow);
            tvMutualStatus = view.findViewById(R.id.tvMutualStatus);
            ivMutualStatus = view.findViewById(R.id.ivMutualStatus);
            
        }
    }
    
    @Override
    public FollowersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        
        View view = LayoutInflater.from(context).inflate(R.layout.view_follower_item,parent,false);
        
        return new FollowersViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(FollowersViewHolder holder, int position) {
        FollowersItem followersItem = followersItems.get(position);
    
        Glide.with(context)
                .load(followersItem.getFollowerImg())
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivUserPicture);
    
        holder.tvUserName.setText(followersItem.getFollowerName());
    
        if(followersItem.isFriend()){
            holder.conFollow.setBackground(context.getResources().getDrawable(R.drawable.following_user_bg));
            holder.tvMutualStatus.setText(context.getString(R.string.str_following_text));
            holder.tvMutualStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.ivMutualStatus.setImageResource(R.drawable.ic_check_followers);
            holder.ivMutualStatus.setColorFilter(context.getResources().getColor(R.color.colorPrimary));
    
        } else {
            //holder.btAddFriend.setBackground(context.getResources().getDrawable(R.drawable.follow_user_bg));
            holder.conFollow.setBackground(context.getResources().getDrawable(R.drawable.follow_user_bg));
            holder.tvMutualStatus.setText(context.getString(R.string.str_follow_text2));
            holder.tvMutualStatus.setTextColor(context.getResources().getColor(R.color.colorRed0));
            holder.ivMutualStatus.setImageResource(R.drawable.ic_icon_plus_pink);
        }
    
        applyClickEvent(holder,position);
        
    }
    
    public void applyClickEvent(final FollowersViewHolder holder, final int position){
        
        holder.tvMutualStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Selected", Toast.LENGTH_SHORT).show();
                followUserListener.onFollowUser(holder.conFollow, holder.tvMutualStatus, holder.ivMutualStatus, position);
            }
        });
        
        holder.ivUserPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followUserListener.onViewProfile(position);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        if(displaySize > followersItems.size()) {
            return followersItems.size();
        }
        return displaySize;
    }
    
    public void setDisplaySize(int displaySize) {
        this.displaySize = displaySize;
        notifyDataSetChanged();
    }
    
    public interface FollowUserListener {
        
        void onFollowUser(View v, View v2, View v3, int position);
        void onViewProfile(int position);
    }
    
}