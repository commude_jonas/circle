package jp.co.commude.circleapp.module.camera;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.add_event.AddEventActivity;
import jp.co.commude.circleapp.customviews.AutofitSurfaceView;

import static android.hardware.Camera.getCameraInfo;

/**
 * Created by vic_villanueva on 26/02/2018.
 */

public class CustomCameraActivity extends AppCompatActivity implements View.OnClickListener {
    
    ImageView ivCaptured, ivCapturedDone;
    ImageView ivRetakePicture, ivCancelPicture;
    ImageView ivCameraSwitch;
    
    FrameLayout camera_preview;
    FrameLayout cameraControl1, cameraControl2;
    
    private AutofitSurfaceView autofitSurfaceView;
    private Camera camera;
    Intent intent;
    
    private static final String TAG = "CustomCameraActivity";
    private static final String CAMERA_TAG = "camera_callback";
    
    Camera.CameraInfo currentCamInfo;
    int currentCameraId = 0;
    
    private boolean cameraFront = false;
    
    static final int MEDIA_TYPE_IMAGE = 1;
    static final int MEDIA_TYPE_VIDEO = 2;
    
    @Override
    protected void onStart() {
        super.onStart();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        camera.stopPreview();
    }
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_camera);
        
        ivCaptured = (ImageView) findViewById(R.id.ivCameraCaptured);
        ivCapturedDone = (ImageView) findViewById(R.id.ivCameraCapturedDone);
        
        ivRetakePicture = (ImageView) findViewById(R.id.ivCameraRetake);
        ivCancelPicture = (ImageView) findViewById(R.id.ivCameraBack);
        
        ivCameraSwitch = (ImageView) findViewById(R.id.ivCameraSwitch);
        
        cameraControl1 = (FrameLayout) findViewById(R.id.cameraControl1);
        cameraControl2 = (FrameLayout) findViewById(R.id.cameraControl2);
        
        camera = getCameraInstance(currentCameraId);
        currentCamInfo = new Camera.CameraInfo();
        
        camera_preview = (FrameLayout) findViewById(R.id.camera_preview);
        autofitSurfaceView = new AutofitSurfaceView(CustomCameraActivity.this, camera);
        camera_preview.addView(autofitSurfaceView);
        startCamera(currentCameraId);
        
        ivCaptured.setOnClickListener(this);
        ivCapturedDone.setOnClickListener(this);
        ivRetakePicture.setOnClickListener(this);
        ivCancelPicture.setOnClickListener(this);
        ivCameraSwitch.setOnClickListener(this);
        
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCameraCaptured:
                
                camera.takePicture(null,null,mPicture);
                ivCameraSwitch.setVisibility(View.INVISIBLE);
                cameraControl1.setVisibility(View.GONE);
                cameraControl2.setVisibility(View.VISIBLE);
                break;
            
            case R.id.ivCameraCapturedDone:
                
                setResult(AddEventActivity.RESULT_INTENT_CAMERA,intent);
                finish();
                break;
            
            case R.id.ivCameraRetake:
                
                retakePicture();
                ivCameraSwitch.setVisibility(View.VISIBLE);
                cameraControl1.setVisibility(View.VISIBLE);
                cameraControl2.setVisibility(View.GONE);
                Toast.makeText(CustomCameraActivity.this, "Retake Captured Photos", Toast.LENGTH_SHORT).show();
                break;
            
            case R.id.ivCameraBack:
                
                onBackPressed();
                break;
            
            case R.id.ivCameraSwitch:
                
                changeCameraType();
                
                break;
        }
        
    }
    
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            
            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null){
                //
                return;
            }
            
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                
                /**Trying this code **/
                Bitmap bm=null;
                
                // COnverting ByteArray to Bitmap - >Rotate and Convert back to Data
                if (data != null) {
                    int screenWidth = getResources().getDisplayMetrics().widthPixels;
                    int screenHeight = getResources().getDisplayMetrics().heightPixels;
                    bm = BitmapFactory.decodeByteArray(data, 0, (data != null) ? data.length : 0);
                    
                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                        // Notice that width and height are reversed
                        Bitmap scaled = Bitmap.createScaledBitmap(bm, screenHeight, screenWidth, true);
                        int w = scaled.getWidth();
                        int h = scaled.getHeight();
                        // Setting post rotate to 90
                        Matrix mtx = new Matrix();
                        
                        int CameraEyeValue = setPhotoOrientation(CustomCameraActivity.this, cameraFront==true ? 1:0); // CameraID = 1 : front 0:back
                        if(cameraFront) { // As Front camera is Mirrored so Fliping the Orientation
                            if (CameraEyeValue == 270) {
                                mtx.postRotate(90);
                            } else if (CameraEyeValue == 90) {
                                mtx.postRotate(270);
                            }
                        }else{
                            mtx.postRotate(CameraEyeValue); // CameraEyeValue is default to Display Rotation
                        }
                        
                        bm = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);
                    }else{// LANDSCAPE MODE
                        //No need to reverse width and height
                        Bitmap scaled = Bitmap.createScaledBitmap(bm, screenWidth, screenHeight, true);
                        bm=scaled;
                    }
                }
                // COnverting the Die photo to Bitmap
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                
                fos.write(byteArray);
                fos.close();
                
            } catch (FileNotFoundException e) {
                Log.d(CAMERA_TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(CAMERA_TAG, "Error accessing file: " + e.getMessage());
            }
        }
    };
    
    //* A safe way to get an instance of the Camera object.
    public Camera getCameraInstance(int currentCameraId){
        Camera c = null;
        try {
            c = Camera.open(currentCameraId);
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }
    
    public void startCamera(int currentCameraId){
        
        try {
            camera.setPreviewDisplay(autofitSurfaceView.getHolder());
            
            setCameraDisplayOrientation(this,currentCameraId,camera);
            camera.startPreview();// attempt to get a Camera instance
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void retakePicture(){
        autofitSurfaceView.surfaceDestroyed(autofitSurfaceView.getHolder());
        autofitSurfaceView.getHolder().removeCallback(autofitSurfaceView);
        autofitSurfaceView.destroyDrawingCache();
        camera.stopPreview();
        camera.setPreviewCallback(null);
        camera.release();
        checkCameraType();
        
        camera = getCameraInstance(currentCameraId);
        startCamera(currentCameraId);
    }
    
    public void checkCameraType(){
        if(currentCamInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT){
            currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        } else {
            currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        }
    }
    
    public void changeCameraType(){
        if(cameraFront) {
            int cameraId = findBackFacingCamera();
            if(cameraId >= 0) {
                autofitSurfaceView.surfaceDestroyed(autofitSurfaceView.getHolder());
                autofitSurfaceView.getHolder().removeCallback(autofitSurfaceView);
                autofitSurfaceView.destroyDrawingCache();
                camera.stopPreview();
                camera.setPreviewCallback(null);
                camera.release();
                camera = getCameraInstance(cameraId);
                startCamera(cameraId);
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if(cameraId >= 0) {
                autofitSurfaceView.surfaceDestroyed(autofitSurfaceView.getHolder());
                autofitSurfaceView.getHolder().removeCallback(autofitSurfaceView);
                autofitSurfaceView.destroyDrawingCache();
                camera.stopPreview();
                camera.setPreviewCallback(null);
                camera.release();
                camera = getCameraInstance(cameraId);
                startCamera(cameraId);
            }
        }
    }
    
    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            getCameraInfo(i, currentCamInfo);
            if (currentCamInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;
            }
        }
        return cameraId;
    }
    
    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            getCameraInfo(i, currentCamInfo);
            if (currentCamInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;
    }
    
    public void setCameraDisplayOrientation(Activity activity,
                                            int cameraId, android.hardware.Camera camera) {
        Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }
        
        int result = 0;
        
        if(cameraFront) {
            cameraId = findFrontFacingCamera();
            if(cameraId >= 0) {
                result = (info.orientation + degrees) % 360;
                result = (360 - result) % 360;  // compensate the mirror
            }
        }
        else {
            cameraId = findBackFacingCamera();
            if(cameraId >= 0) {
                // back-facing
                result = (info.orientation - degrees + 360) % 360;
            }
        }
        
        camera.setDisplayOrientation(result);
    }
    
    public int setPhotoOrientation(Activity activity, int cameraId) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        
        int result;
        // do something for phones running an SDK before lollipop
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        
        return result;
    }
    
    //* Create a file Uri for saving an image or video
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }
    
    //* Create a File for saving an image or video
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Circle App");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }
        
        return mediaFile;
    }
    
}
