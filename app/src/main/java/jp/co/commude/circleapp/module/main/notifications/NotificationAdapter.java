package jp.co.commude.circleapp.module.main.notifications;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    
    Context context;
    ArrayList<NotificationItem> notificationItems;
    
    public interface EventListener {
        void onSwipe(int position);
        void gotoEvent(int position);
        void gotoProfile(int position);
        
    }
    EventListener eventListener;
    
    public NotificationAdapter(Context context, ArrayList<NotificationItem> notificationItems, EventListener eventListener) {
        this.context = context;
        this.notificationItems = notificationItems;
        this.eventListener = eventListener;
        
    }
    
    public class ViewHolder extends RecyclerView.ViewHolder {
        
        public TextView tvUserName,tvUserDesc, tvDateTime;
        public ImageView ivUserProfilePic, ivNotifIndicator;
        public ViewHolder(View itemView) {
            super(itemView);
            tvUserName = (TextView) itemView.findViewById(R.id.tvUserName);
            tvUserDesc = (TextView) itemView.findViewById(R.id.tvUserDetails);
            tvDateTime = (TextView) itemView.findViewById(R.id.tvNotifAbove);
            ivUserProfilePic = (ImageView) itemView.findViewById(R.id.ivUserProfile);
            ivNotifIndicator = (ImageView) itemView.findViewById(R.id.ivNotificationIndicator);
        }
    }
    
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.view_notification_item,parent,false));
    }
    
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        
        NotificationItem notificationItem = notificationItems.get(position);
        
        Glide.with(context)
                .load(notificationItem.getUserProfilePic())
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivUserProfilePic);
        
        holder.tvUserName.setText(notificationItem.getUserName());
        holder.tvUserDesc.setText(notificationItem.getUserDetails());
        holder.tvDateTime.setText(notificationItem.getDate());
        
        if(notificationItem.isRead()){
            holder.ivNotifIndicator.setVisibility(View.INVISIBLE);
        } else {
            holder.ivNotifIndicator.setVisibility(View.VISIBLE);
        }
        
        holder.ivUserProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventListener.gotoProfile(position);
            }
        });
        
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventListener.gotoEvent(position);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return notificationItems.size();
    }
    
    public void addItem(NotificationItem notificationItem) {
        notificationItems.add(notificationItem);
        notifyItemInserted(notificationItems.size());
    }
    
    public void removeItem(int position) {
        notificationItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, notificationItems.size());
    }
    
    
}
    