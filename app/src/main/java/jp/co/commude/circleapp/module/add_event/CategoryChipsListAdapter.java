package jp.co.commude.circleapp.module.add_event;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.category.CategoryItem;

/**
 * Created by vic_villanueva on 22/02/2018.
 */

public class CategoryChipsListAdapter extends RecyclerView.Adapter<CategoryChipsListAdapter.CategoryViewHolder> {
    
    Context context;
    ArrayList<CategoryItem> categoryItems;
    SparseBooleanArray selectedItems;
    private ChipSelectListener chipSelectListener;
    // index is used to animate only the selected row
    // dirty fix, find a better solution
    private static int currentSelectedIndex = -1;
    
    public interface ChipSelectListener {
        void onChipSelected(int position);
    }
    
    public CategoryChipsListAdapter(Context context, ArrayList<CategoryItem> categoryItems, ChipSelectListener chipSelectListener) {
        this.context = context;
        this.categoryItems = categoryItems;
        this.chipSelectListener = chipSelectListener;
        selectedItems = new SparseBooleanArray();
    }
    
    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategoryChoice;
        public CategoryViewHolder(View view) {
            super(view);
            tvCategoryChoice = (TextView) view.findViewById(R.id.tvCategoryChoice);
        }
    }
    
    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(context).inflate(R.layout.category_list_choices_item,parent,false));
    }
    
    @Override
    public void onBindViewHolder(CategoryViewHolder holder, final int position) {
        CategoryItem categoryItem = categoryItems.get(position);
        
        holder.tvCategoryChoice.setText(categoryItem.getCategoryName());
        
        holder.itemView.setActivated(selectedItems.get(position, false));
    
        isSelected(categoryItem,holder);
        
        holder.tvCategoryChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chipSelectListener.onChipSelected(position);
            }
        });
    }
    
    private void isSelected(CategoryItem categoryItem, CategoryViewHolder holder){
    
        if(categoryItem.isSelected()){
            holder.tvCategoryChoice.setVisibility(View.GONE);
        } else {
            holder.tvCategoryChoice.setVisibility(View.VISIBLE);
        }
    }
    
    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }
    
    
    @Override
    public int getItemCount() {
        return categoryItems.size();
    }
    
    
    public int getSelectedItemCount() {
        return selectedItems.size();
    }
    
    public ArrayList<Integer> getSelectedItems() {
        ArrayList<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }
    
    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }
    
    public void removeData(int position) {
        categoryItems.remove(position);
        resetCurrentIndex();
    }
    
    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }
}
