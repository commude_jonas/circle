package jp.co.commude.circleapp.module.profile;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;
import jp.co.commude.circleapp.module.events.EventDetailsActivity;
import jp.co.commude.circleapp.module.main.feeds.PublicEventItem;
import jp.co.commude.circleapp.module.main.profile.ProfileEventListAdapter;

public class UserEventFragment extends Fragment implements
        ProfileEventListAdapter.onSelectEventListener {
    
        RecyclerView rvEvents;
        ProfileEventListAdapter profileEventListAdapter;
        ArrayList<PublicEventItem> publicEventItems;
        
        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_user_events,container,false);
        }
    
        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            
            rvEvents = (RecyclerView) view.findViewById(R.id.rvEvents);
    
            addRecords();
            
            profileEventListAdapter = new ProfileEventListAdapter(getActivity(), publicEventItems,this);
    
            RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvEvents.setLayoutManager(layoutManager2);
            rvEvents.setHasFixedSize(true);
            rvEvents.setItemAnimator(new DefaultItemAnimator());
            rvEvents.setAdapter(profileEventListAdapter);
            
            profileEventListAdapter.notifyDataSetChanged();
        }
        
        public void addRecords(){
            publicEventItems = new ArrayList<PublicEventItem>();
            Globals.PublicEventItems(getActivity(),publicEventItems);
        }
    
        @Override
        public void onSelectEvent(View v, int position) {
            PublicEventItem publicEventItem = publicEventItems.get(position);
            Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
            intent.putExtra("intentImage", position);
        
            View view = v;
            String transitionName = "myCover";
        
            ActivityOptions transiActivityOptions = ActivityOptions.makeSceneTransitionAnimation(getActivity(), view, transitionName);
        
            Explode explode = new Explode();
            explode.excludeTarget(android.R.id.statusBarBackground, true);
            explode.excludeTarget(android.R.id.navigationBarBackground, true);
            explode.excludeTarget(R.id.coordinatorLayout, true);
            explode.excludeTarget(R.id.customtoolbar, true);
            explode.excludeTarget(R.id.ivBackpress, true);
            explode.excludeTarget(R.id.ivComment, true);
            explode.excludeTarget(R.id.ivShare, true);
            explode.excludeTarget(R.id.frameLike, true);
            explode.excludeTarget(R.id.ivLike, true);
            explode.excludeTarget(R.id.otherdetails, true);
            explode.excludeTarget(R.id.tvDate, true);
            explode.excludeTarget(R.id.tvDesc, true);
            explode.excludeTarget(R.id.otherdetails, true);
        
            explode.excludeTarget(R.id.frameLayout2, true);
            explode.excludeTarget(R.id.customtoolbar, true);
            explode.excludeTarget(R.id.tvEventType, true);
            explode.excludeTarget(R.id.eventCard, true);
            explode.excludeTarget(R.id.map_container, true);
            explode.excludeTarget(R.id.tvMapAddress, true);
            explode.excludeTarget(R.id.firstborder, true);
            explode.excludeTarget(R.id.secondborder, true);
            explode.excludeTarget(R.id.thirdborder, true);
            explode.excludeTarget(R.id.rvHorizontalAlbum, true);
            explode.excludeTarget(R.id.rvHorizontalAlbumParticipant, true);
            explode.excludeTarget(R.id.albumcontainer, true);
            explode.excludeTarget(R.id.participantcontainer, true);
            explode.excludeTarget(R.id.commentcontainer, true);
        
            explode.excludeTarget(R.id.creator, true);
            explode.excludeTarget(R.id.ivMessage, true);
            explode.excludeTarget(R.id.tvUsername, true);
            explode.excludeTarget(R.id.tvDescription, true);
            explode.excludeTarget(R.id.ivProfilePicture, true);
            explode.excludeTarget(R.id.album, true);
            explode.excludeTarget(R.id.buttonStart, true);
        
            getActivity().getWindow().setEnterTransition(new Explode());
            getActivity().getWindow().setExitTransition(new Slide());
        
            getActivity().startActivity(intent, transiActivityOptions.toBundle());
        }
    
        @Override
        public void onSelectCommentToEvent(int position) {
        
        }
    
        @Override
        public void shareEventtoSocial(int position) {
        
        }
    }