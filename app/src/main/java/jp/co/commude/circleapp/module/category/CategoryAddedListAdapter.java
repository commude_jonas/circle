package jp.co.commude.circleapp.module.category;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.UIControlsUtils;

/**
 * Created by vic_villanueva on 27/02/2018.
 */

public class CategoryAddedListAdapter extends RecyclerView.Adapter<CategoryAddedListAdapter.CategoryAddedViewHolder> {
    
    Context context;
    ArrayList<CategoryItem> categoryItems;
    
    private SparseBooleanArray selectedItems;
    
    private static int currentSelectedIndex = -1;
    
    public interface CategoryAddedListener{
        void onAddedCategory(int position);
    }
    
    CategoryAddedListener categoryAddedListener;
    private int layoutRes = 0;
    
    public CategoryAddedListAdapter(Context context, ArrayList<CategoryItem> categoryItems) {
        this.context = context;
        this.categoryItems = categoryItems;
        selectedItems = new SparseBooleanArray();
        layoutRes = 0;
    }
    
    public CategoryAddedListAdapter(Context context, ArrayList<CategoryItem> categoryItems, CategoryAddedListener categoryAddedListener) {
        this.context = context;
        this.categoryItems = categoryItems;
        this.categoryAddedListener = categoryAddedListener;
        selectedItems = new SparseBooleanArray();
        layoutRes = 0;
    }
    
    public CategoryAddedListAdapter(Context context, ArrayList<CategoryItem> categoryItems, CategoryAddedListener categoryAddedListener, int layoutRes) {
        this.context = context;
        this.categoryItems = categoryItems;
        selectedItems = new SparseBooleanArray();
        this.categoryAddedListener = categoryAddedListener;
        this.layoutRes = layoutRes;
    }
    
    public class CategoryAddedViewHolder extends RecyclerView.ViewHolder {
    
        public ImageView ivCategoryPic;
        public TextView tvCategoryName;
        public ImageView ivToggle;
        
        public RelativeLayout categoryContainer;
        
        public CategoryAddedViewHolder(View view) {
            super(view);
            ivCategoryPic = (ImageView) view.findViewById(R.id.itemImage);
            tvCategoryName = (TextView) view.findViewById(R.id.itemText);
            ivToggle = (ImageView) view.findViewById(R.id.itemToggle);
            categoryContainer = (RelativeLayout) view.findViewById(R.id.category_container);
        }
    }
    
    @Override
    public CategoryAddedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        
        if(layoutRes == 0) {
            return new CategoryAddedViewHolder(LayoutInflater.from(context).inflate(R.layout.view_category_item, parent, false));
        } else if(layoutRes == 1){
            return new CategoryAddedViewHolder(LayoutInflater.from(context).inflate(R.layout.view_category_item, parent, false));
        }else {
            return new CategoryAddedViewHolder(LayoutInflater.from(context).inflate(R.layout.view_category_item_remove,parent,false));
        }
    }
    
    @Override
    public void onBindViewHolder(CategoryAddedViewHolder holder, final int position) {
        CategoryItem categoryItem = categoryItems.get(position);
    
        holder.tvCategoryName.setText(categoryItem.getCategoryName());
    
        Bitmap ShrinkedImage = new UIControlsUtils().generateScaledBitmap(context,categoryItem.getCategoryImgrsc());
        
        Glide.with(context)
                .load(ShrinkedImage)
                .thumbnail(0.5f)
                .apply(RequestOptions.noTransformation())
                .into(holder.ivCategoryPic);
    
        holder.ivCategoryPic.setColorFilter(ContextCompat.getColor(context, R.color.color_gray_4011), PorterDuff.Mode.SRC_ATOP);
    
        holder.itemView.setActivated(selectedItems.get(position, false));
        
        if(categoryAddedListener != null) {
            holder.ivCategoryPic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        categoryAddedListener.onAddedCategory(position);
                    }
                });
        }
        
        if(categoryItem.isSelected()){
            holder.categoryContainer.setVisibility(View.GONE);
        } else {
            holder.categoryContainer.setVisibility(View.VISIBLE);
        }
        
        applySelected(holder, position);
    
    }
    
    private void applySelected(CategoryAddedViewHolder holder, int position) {
        if(layoutRes == 0) {
            holder.ivToggle.setVisibility(View.GONE);
        } else {
            holder.ivToggle.setVisibility(View.VISIBLE);
        }
    }
    
    @Override
    public int getItemCount() {
        return categoryItems.size();
    }
    
    public int getSelectedItemCount(){
        return selectedItems.size();
    }
    
    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }
    
    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }
    
    public void removeData(int position) {
        categoryItems.remove(position);
        resetCurrentIndex();
        notifyItemChanged(position);
    }
    
    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }
}
