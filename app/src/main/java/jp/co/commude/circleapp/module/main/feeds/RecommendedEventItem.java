package jp.co.commude.circleapp.module.main.feeds;

/**
 * Created by vic_villanueva on 23/01/2018.
 */

public class RecommendedEventItem {

    String eventDate;
    String eventName;
    String eventDesc;
    int eventImg;
    
    public RecommendedEventItem(String eventDate, String eventDesc, int eventImg) {
        this.eventDate = eventDate;
        this.eventDesc = eventDesc;
        this.eventImg = eventImg;
    }
    
    public RecommendedEventItem(String eventDate, String eventName, String eventDesc, int eventImg) {
        this.eventDate = eventDate;
        this.eventName = eventName;
        this.eventDesc = eventDesc;
        this.eventImg = eventImg;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public int getEventImg() {
        return eventImg;
    }

    public void setEventImg(int eventImg) {
        this.eventImg = eventImg;
    }
}
