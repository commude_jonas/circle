package jp.co.commude.circleapp.module.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.category.UserAddCategoryActivity;
import jp.co.commude.circleapp.module.followers.FollowersActivity;
import jp.co.commude.circleapp.module.followers.FollowingActivity;
import jp.co.commude.circleapp.module.message.MessageActivity;

/**
 * Created by vic_villanueva on 20/02/2018.
 */

public class UserProfileActivity extends AppCompatActivity
        implements View.OnClickListener, ViewPager.OnPageChangeListener {
    
    AppBarLayout appBarLayout;
    ViewPager vpEventPager;
    ImageView ivBackPress, ivMessage, ivReport;
    
    ImageView ivProfilePicture,ivProfilePictureBig;
    
    LinearLayout conFollowing, conFollowers, conEvents;
    
    HorizontalScrollView hvEventsTypes;
    TextView tvPage1,tvPage2,tvPage3,tvPage4;
    
    NavigationFeedCategoryAdapter mAdapter;
    View vBorder;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        init();
        profileHeader();
        
    }
    
    private void init(){
    
        vBorder = findViewById(R.id.vBorder);
    
        appBarLayout = (AppBarLayout) findViewById(R.id.apladdevent);
        ivBackPress = (ImageView) findViewById(R.id.ivBackpress);
        ivMessage = (ImageView) findViewById(R.id.ivMessage);
        ivReport = (ImageView) findViewById(R.id.ivReport);
    
        ivProfilePicture = (ImageView) findViewById(R.id.ivProfilePicture);
        ivProfilePictureBig = (ImageView) findViewById(R.id.ivProfilePictureBig);
        
        conFollowing = (LinearLayout) findViewById(R.id.conFollowing);
        conFollowers = (LinearLayout) findViewById(R.id.conFollowers);
        conEvents = (LinearLayout) findViewById(R.id.conCategories);
    
        hvEventsTypes = (HorizontalScrollView) findViewById(R.id.hvEventsTypes);
    
        tvPage1 = (TextView) findViewById(R.id.tvPage1);
        tvPage2 = (TextView) findViewById(R.id.tvPage2);
        tvPage3 = (TextView) findViewById(R.id.tvPage3);
        tvPage4 = (TextView) findViewById(R.id.tvPage4);
        
        conFollowers.setOnClickListener(this);
        conFollowing.setOnClickListener(this);
        conEvents.setOnClickListener(this);
        
        tvPage1.setOnClickListener(this);
        tvPage2.setOnClickListener(this);
        tvPage3.setOnClickListener(this);
        tvPage4.setOnClickListener(this);
        
        vpEventPager = (ViewPager) findViewById(R.id.vpEventPager);
    
        ivBackPress.setOnClickListener(this);
        ivMessage.setOnClickListener(this);
        ivReport.setOnClickListener(this);
        
        mAdapter = new NavigationFeedCategoryAdapter(getSupportFragmentManager());
        
        vpEventPager.setAdapter(mAdapter);
        vpEventPager.setOnPageChangeListener(this);
    }
   
    private void profileHeader(){
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    ivBackPress.setImageTintList(null);
                    ivBackPress.setImageTintList(getResources().getColorStateList(R.color.colorBlack2));
    
                    ivMessage.setImageTintList(null);
                    ivMessage.setImageTintList(getResources().getColorStateList(R.color.colorBlack2));
    
                    ivReport.setImageTintList(null);
                    ivReport.setImageTintList(getResources().getColorStateList(R.color.colorGray1));
                    
                    ivProfilePicture.setVisibility(View.VISIBLE);
                    vBorder.setBackgroundColor(getResources().getColor(R.color.colorGray8));
    
                } else if (verticalOffset == 0) {
                    // Expanded
                    ivBackPress.setImageTintList(null);
                    ivBackPress.setImageTintList(getResources().getColorStateList(R.color.colorWhite0));
    
                    ivMessage.setImageTintList(null);
                    ivMessage.setImageTintList(getResources().getColorStateList(R.color.colorWhite0));
                    
                    ivReport.setImageTintList(null);
                    ivReport.setImageTintList(getResources().getColorStateList(R.color.colorWhite0));
    
                    ivProfilePicture.setVisibility(View.GONE);
                    vBorder.setBackgroundColor(getResources().getColor(R.color.transparent));
    
    
                } else {
                    // Somewhere in between
                }
            }
        });
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.ivMessage:
                MessageActivity.open(UserProfileActivity.this);
                break;
            case R.id.ivReport:
//                MessageActivity.open(UserProfileActivity.this);
                Toast.makeText(this, "Report", Toast.LENGTH_SHORT).show();
                break;
            case R.id.conFollowers:
                FollowersActivity.open(this);
                break;
            case R.id.conFollowing:
                FollowingActivity.open(this);
                break;
            case R.id.conCategories:
                UserAddCategoryActivity.open(this);
                break;
            case R.id.tvPage1:
                vpEventPager.setCurrentItem(0,true);
                break;
            case R.id.tvPage2:
                vpEventPager.setCurrentItem(1,true);
                break;
            case R.id.tvPage3:
                vpEventPager.setCurrentItem(2,true);
                break;
            case R.id.tvPage4:
                vpEventPager.setCurrentItem(3,true);
                break;
        }
        
    }
    
    private void OnScrollPosition(int position){
        final View child = ((LinearLayout) hvEventsTypes.getChildAt(0)).getChildAt(position);
        int scrollTo = child.getLeft();
        hvEventsTypes.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        hvEventsTypes.setSmoothScrollingEnabled(true);
        hvEventsTypes.scrollTo(scrollTo,0);
    }
    
    
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        switch (position){
            case 0:
                OnScrollPosition(0);
                tvPage1.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage2.setBackgroundResource(R.drawable.tab_bg);
                tvPage3.setBackgroundResource(R.drawable.tab_bg);
                tvPage4.setBackgroundResource(R.drawable.tab_bg);
            
                tvPage1.setTextColor(getResources().getColor(R.color.colorWhite0));
                tvPage2.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage3.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getResources().getColor(R.color.colorBlack3));
                break;
            case 1:
                OnScrollPosition(1);
                tvPage2.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage1.setBackgroundResource(R.drawable.tab_bg);
                tvPage3.setBackgroundResource(R.drawable.tab_bg);
                tvPage4.setBackgroundResource(R.drawable.tab_bg);
            
                tvPage1.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage2.setTextColor(getResources().getColor(R.color.colorWhite0));
                tvPage3.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getResources().getColor(R.color.colorBlack3));
                break;
            case 2:
                OnScrollPosition(2);
                tvPage3.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage1.setBackgroundResource(R.drawable.tab_bg);
                tvPage2.setBackgroundResource(R.drawable.tab_bg);
                tvPage4.setBackgroundResource(R.drawable.tab_bg);
            
                tvPage1.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage3.setTextColor(getResources().getColor(R.color.colorWhite0));
                tvPage2.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getResources().getColor(R.color.colorBlack3));
                break;
            case 3:
                OnScrollPosition(3);
                tvPage4.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage1.setBackgroundResource(R.drawable.tab_bg);
                tvPage2.setBackgroundResource(R.drawable.tab_bg);
                tvPage3.setBackgroundResource(R.drawable.tab_bg);
            
                tvPage1.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getResources().getColor(R.color.colorWhite0));
                tvPage2.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage3.setTextColor(getResources().getColor(R.color.colorBlack3));
                break;
            default:
                OnScrollPosition(0);
                tvPage1.setBackgroundResource(R.drawable.tab_bg_color);
                tvPage2.setBackgroundResource(R.drawable.tab_bg);
                tvPage3.setBackgroundResource(R.drawable.tab_bg);
                tvPage4.setBackgroundResource(R.drawable.tab_bg);
            
                tvPage2.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage1.setTextColor(getResources().getColor(R.color.colorWhite0));
                tvPage3.setTextColor(getResources().getColor(R.color.colorBlack3));
                tvPage4.setTextColor(getResources().getColor(R.color.colorBlack3));
                break;
        }
    }
    
    @Override
    public void onPageSelected(int position) {
    
    }
    
    @Override
    public void onPageScrollStateChanged(int state) {
    
    }
    
    public static void open(Activity act){
        act.startActivity(new Intent(act, UserProfileActivity.class));
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    
    public class NavigationFeedCategoryAdapter extends FragmentStatePagerAdapter {
        private final int PAGE_NUM = 4;
        
        public NavigationFeedCategoryAdapter(FragmentManager fm) {
            super(fm);
        }
    
        @Override
        public Fragment getItem(int position) {
            return new UserEventFragment();
        }
    
        @Override
        public int getCount() {
            return PAGE_NUM;
        }
    }
    
}
