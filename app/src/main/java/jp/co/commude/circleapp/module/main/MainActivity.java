package jp.co.commude.circleapp.module.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.add_event.AddEventActivity;
import jp.co.commude.circleapp.module.main.feeds.FeedFragment;
import jp.co.commude.circleapp.module.main.friends.FriendFragment;
import jp.co.commude.circleapp.module.main.notifications.NotificationFragment;
import jp.co.commude.circleapp.module.main.profile.ProfileFragment;
import jp.co.commude.circleapp.utility.CommonUtils;

import static jp.co.commude.circleapp.model.Globals.EVENTS;
import static jp.co.commude.circleapp.model.Globals.FEEDS;
import static jp.co.commude.circleapp.model.Globals.FRIENDS;
import static jp.co.commude.circleapp.model.Globals.NOTIFICATIONS;
import static jp.co.commude.circleapp.model.Globals.PROFILE;

/**
 * Created by vic_villanueva on 22/01/2018.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    
    
    
    private static int navItemIndex = 0;
    
    private ImageView ivFeeds,ivFriends,ivNotification,ivProfile;
    private FloatingActionButton fabAddevent;
    
    public static String CURRENT_TAG = FEEDS;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        
        ivFeeds = (ImageView) findViewById(R.id.ivFeeds);
        ivFriends = (ImageView) findViewById(R.id.ivFriends);
        ivNotification = (ImageView) findViewById(R.id.ivNotification);
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        fabAddevent = (FloatingActionButton) findViewById(R.id.fabAddevent);
        
        ivFeeds.setOnClickListener(this);
        ivFriends.setOnClickListener(this);
        ivNotification.setOnClickListener(this);
        ivProfile.setOnClickListener(this);
        fabAddevent.setOnClickListener(this);
        
        if(savedInstanceState == null) {
            
            Intent intent = getIntent();
            if(intent == null){
                navItemIndex = 0;
                CURRENT_TAG = FEEDS;
            } else {
                int fragmentIndex = intent.getIntExtra("fragment", 0);
                if (fragmentIndex != 0) {
                    navItemIndex = fragmentIndex;
                    CURRENT_TAG = intent.getStringExtra("fragmentName");
                } else {
                    navItemIndex = 0;
                    CURRENT_TAG = FEEDS;
                }
            }
            
            loadFragment();
            applyColorTabIconTint();
        }
        
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivFeeds:
                navItemIndex = 0;
                CURRENT_TAG = FEEDS;
                break;
            case R.id.ivFriends:
                navItemIndex = 1;
                CURRENT_TAG = FRIENDS;
                break;
            case R.id.ivNotification:
                navItemIndex = 3;
                CURRENT_TAG = NOTIFICATIONS;
                break;
            case R.id.ivProfile:
                navItemIndex = 4;
                CURRENT_TAG = PROFILE;
                break;
            case R.id.fabAddevent:
                navItemIndex = 2;
                CURRENT_TAG = EVENTS;
                break;
        }
        loadFragment();
        applyColorTabIconTint();
        Log.d("Log nav Item index ", navItemIndex+ "");
    }
    
    private Fragment getFragment(){
        switch (navItemIndex){
            case 0:
                FeedFragment feedFragment = new FeedFragment();
                return feedFragment;
            case 1:
                FriendFragment friendFragment = new FriendFragment();
                return friendFragment;
            case 2:
                AddEventActivity.open(MainActivity.this);
                finish();
            case 3:
                NotificationFragment notificationFragment = new NotificationFragment();
                return notificationFragment;
            case 4:
                ProfileFragment profileFragment = new ProfileFragment();
                return profileFragment;
        }
        return null;
    }
    
    private void loadFragment(){
        CommonUtils.loadFragment(this, R.id.fragmentframe, getFragment(), CURRENT_TAG);
    }
    
    private void applyColorTabIconTint(){
        switch (navItemIndex){
            case 0:
                //Feed selected
                ivFeeds.setImageResource(R.drawable.ic_navigation_feed_selected);
                ivFriends.setImageResource(R.drawable.ic_navigation_friend_normal);
                ivNotification.setImageResource(R.drawable.ic_navigation_notification_normal_with_dot);
                ivProfile.setImageResource(R.drawable.ic_navigation_me_normal);
                break;
            case 1:
                //Find Friends selected
                ivFeeds.setImageResource(R.drawable.ic_navigation_feed_normal);
                ivFriends.setImageResource(R.drawable.ic_navigation_friend_selected);
                ivNotification.setImageResource(R.drawable.ic_navigation_notification_normal_with_dot);
                ivProfile.setImageResource(R.drawable.ic_navigation_me_normal);
                break;
            case 2:
                //Add Event selected
                ivFeeds.setImageResource(R.drawable.ic_navigation_feed_normal);
                ivFriends.setImageResource(R.drawable.ic_navigation_friend_normal);
                ivNotification.setImageResource(R.drawable.ic_navigation_notification_normal_without_dot);
                ivProfile.setImageResource(R.drawable.ic_navigation_me_normal);
                break;
            case 3:
                //Notification selected
                ivFeeds.setImageResource(R.drawable.ic_navigation_feed_normal);
                ivFriends.setImageResource(R.drawable.ic_navigation_friend_normal);
                ivNotification.setImageResource(R.drawable.ic_navigation_notification_selected_with_dot);
                ivProfile.setImageResource(R.drawable.ic_navigation_me_normal);
                break;
            case 4:
                //Profile selected
                ivFeeds.setImageResource(R.drawable.ic_navigation_feed_normal);
                ivFriends.setImageResource(R.drawable.ic_navigation_friend_normal);
                ivNotification.setImageResource(R.drawable.ic_navigation_notification_normal_without_dot);
                ivProfile.setImageResource(R.drawable.ic_navigation_me_selected);
                break;
            default:
                //Feed selected
                ivFeeds.setImageResource(R.drawable.ic_navigation_feed_selected);
                ivFriends.setImageResource(R.drawable.ic_navigation_friend_normal);
                ivNotification.setImageResource(R.drawable.ic_navigation_notification_normal_with_dot);
                ivProfile.setImageResource(R.drawable.ic_navigation_me_normal);
                break;
        }
    }
    
    public static void openDefault(Activity act, int value, String pageName){
        Intent intent = new Intent(act, MainActivity.class);
        intent.putExtra("fragment",value);
        intent.putExtra("fragmentName",pageName);
        act.startActivity(intent);
        act.overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        act.finishAfterTransition();
    }
}
