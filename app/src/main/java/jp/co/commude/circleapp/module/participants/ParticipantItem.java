package jp.co.commude.circleapp.module.participants;

/**
 * Created by vic_villanueva on 06/02/2018.
 */

public class ParticipantItem {
    
    int participantProfile;
    
    public ParticipantItem(int participantProfile) {
        this.participantProfile = participantProfile;
    }
    
    public int getParticipantProfile() {
        return participantProfile;
    }
    
    public void setParticipantProfile(int participantProfile) {
        this.participantProfile = participantProfile;
    }
}
