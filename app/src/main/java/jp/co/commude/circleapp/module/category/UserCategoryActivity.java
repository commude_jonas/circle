package jp.co.commude.circleapp.module.category;

import android.animation.Animator;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.transition.Slide;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;
import jp.co.commude.circleapp.module.confirm.UserPasswordConfirmationPageActivity;
import jp.co.commude.circleapp.module.main.MainActivity;

/**
 * Created by vic_villanueva on 19/01/2018.
 */

public class UserCategoryActivity extends AppCompatActivity
        implements View.OnClickListener
        , CategoryListAdapter.CategoryListener {

    RecyclerView recyclerView;
    LinearLayout llStart;
    ImageView btStart;
    ImageView ivBackpress;
    
    CategoryListAdapter categoryListAdapter;

    ArrayList<CategoryItem> categoryItems;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_category);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        ivBackpress.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        llStart = (LinearLayout) findViewById(R.id.llStart);
        btStart = (ImageView) findViewById(R.id.btContinue);
        btStart.setOnClickListener(this);
    
        categoryItems = new ArrayList<CategoryItem>();
        categoryListAdapter = new CategoryListAdapter(UserCategoryActivity.this,categoryItems,UserCategoryActivity.this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(categoryListAdapter);
        categoryListAdapter.notifyDataSetChanged();
    
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                new CategoryImageLoaderView().execute();
            }
        }, 300);
    
        categoryListAdapter.setOnBottomReachedListener(new CategoryListAdapter.OnBottomReachListener() {
            @Override
            public void onBottomReached(int position) {
    
                TransitionManager.beginDelayedTransition(llStart, new Slide(Gravity.TOP).setDuration(200).setStartDelay(500));
                llStart.setVisibility(View.VISIBLE);
                
            }
        });
        
    }

    @Override
    public void onCategoryClicked(int position) {
        categoryListAdapter.toggleSelection(position);
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btContinue:
                startActivity(new Intent(UserCategoryActivity.this,MainActivity.class));
                finish();
                return;
            case R.id.ivBackpress:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UserCategoryActivity.this,UserPasswordConfirmationPageActivity.class));
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finishAfterTransition();
    }
    
    public class CategoryImageLoaderView extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params) {
            Globals.CategoryItems(UserCategoryActivity.this,categoryItems, R.array.str_res);
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            categoryListAdapter.notifyDataSetChanged();
        }
    }

}
