package jp.co.commude.circleapp.model;

public class Resolution {

    public int MaxX;
    public int MaxY;
    public double dX;
    public double dY;

    public Resolution(int maxX, int maxY, double dX, double dY) {
        MaxX = maxX;
        MaxY = maxY;
        this.dX = dX;
        this.dY = dY;
    }

    /*
    *
    * 720 x 1280
    * 1440 x 2392
    *
    * */
    
}