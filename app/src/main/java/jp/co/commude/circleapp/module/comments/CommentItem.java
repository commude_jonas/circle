package jp.co.commude.circleapp.module.comments;

/**
 * Created by vic_villanueva on 06/02/2018.
 */

public class CommentItem {
    
    String userName;
    String userComment;
    String userCommentTime;
    int userPicture;
    
    
    public CommentItem(String userName, String userComment, String userCommentTime, int userPicture) {
        this.userName = userName;
        this.userComment = userComment;
        this.userCommentTime = userCommentTime;
        this.userPicture = userPicture;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getUserComment() {
        return userComment;
    }
    
    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }
    
    public String getUserCommentTime() {
        return userCommentTime;
    }
    
    public void setUserCommentTime(String userCommentTime) {
        this.userCommentTime = userCommentTime;
    }
    
    public int getUserPicture() {
        return userPicture;
    }
    
    public void setUserPicture(int userPicture) {
        this.userPicture = userPicture;
    }
}
