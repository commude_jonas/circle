package jp.co.commude.circleapp.module.comments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 26/01/2018.
 */

public class EventCommentsListActivity extends AppCompatActivity
        implements View.OnClickListener{
    
    RecyclerView rvCommentList;
    ArrayList<CommentItem> commentItems;
    CommentListAdapter commentListAdapter;
    EditText  etComment;
    FrameLayout fmCommentBox;
    RelativeLayout constraintParent;
    
    ImageView ivBackpress;
    ImageView ivCancel;
    TextView tvComment;
    
    ImageView ivCommentorPicture,ivCommentorPicture2;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_comment);
        
        rvCommentList = (RecyclerView) findViewById(R.id.rvCommentList);
        
        commentItems = new ArrayList<CommentItem>();
        Globals.CommentItems(this,commentItems,R.array.str_com_name,R.array.str_com_comment,R.array.str_com_date);
        
        commentListAdapter = new CommentListAdapter(this,commentItems);
    
        LinearLayoutManager albumManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        rvCommentList.setLayoutManager(albumManager);
        rvCommentList.setItemAnimator(new DefaultItemAnimator());
        rvCommentList.setHasFixedSize(true);
        rvCommentList.setAdapter(commentListAdapter);
        commentListAdapter.setDisplaySize(2);
        commentListAdapter.notifyDataSetChanged();
        
        fmCommentBox = (FrameLayout) findViewById(R.id.fmCommentBox);
        constraintParent = (RelativeLayout) findViewById(R.id.constraintParent);
        
        etComment = (EditText) findViewById(R.id.etComment);
        etComment.setClickable(true);
        etComment.setFocusable(false);
        etComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUpCommentBox();
            }
        });
        
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        ivCancel = (ImageView) findViewById(R.id.ivCancel);
        tvComment = (TextView) findViewById(R.id.tvComment);
    
        ivBackpress.setOnClickListener(this);
        ivCancel.setOnClickListener(this);
        tvComment.setOnClickListener(this);
    
        ivCommentorPicture = (ImageView) findViewById(R.id.circleImageView);
        ivCommentorPicture2 = (ImageView) findViewById(R.id.circleImageView2);
        
        Glide.with(this)
                .load(R.drawable.profile_picture)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(ivCommentorPicture);
    
        Glide.with(this)
                .load(R.drawable.profile_picture)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(ivCommentorPicture2);
        
    }
    
    private void slideUpCommentBox(){
    
        constraintParent.clearAnimation();
        fmCommentBox.setVisibility(View.VISIBLE);
        constraintParent.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                // fromYDelta
                constraintParent.getHeight(),
                0); // toYDelta
        
                    // toYDelta
        animate.setDuration(200);
        constraintParent.startAnimation(animate);
        
    }
    
    private void slideDownCommentBox(){
        constraintParent.clearAnimation();
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,  // fromYDelta
                constraintParent.getHeight());
        
        animate.setDuration(200);
    
        constraintParent.startAnimation(animate);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
        
            }
    
            @Override
            public void onAnimationEnd(Animation animation) {
    
                fmCommentBox.setVisibility(View.GONE);
                constraintParent.setVisibility(View.GONE);
            }
    
            @Override
            public void onAnimationRepeat(Animation animation) {
        
            }
        });
        
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCancel:
                slideDownCommentBox();
                break;
            case R.id.tvComment:
                onBackPressed();
                break;
            case R.id.ivBackpress:
                onBackPressed();
                break;
        }
        
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
}
