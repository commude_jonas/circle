package jp.co.commude.circleapp.module.add_event;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.category.CategoryItem;

/**
 * Created by vic_villanueva on 22/02/2018.
 */

public class CategoryChipsAddedAdapter extends RecyclerView.Adapter<CategoryChipsAddedAdapter.CategoryViewHolder> {
    Context context;
    ArrayList<CategoryItem> categoryItems;
    SparseBooleanArray selectedItems;
    private ChipUnselectListener chipUnselectListener;
    // index is used to animate only the selected row
    // dirty fix, find a better solution
    private static int currentSelectedIndex = -1;
    
    public interface ChipUnselectListener {
        void onChipUnselected(int position);
    }
    public CategoryChipsAddedAdapter(Context context, ArrayList<CategoryItem> categoryItems, ChipUnselectListener chipUnselectListener) {
        this.context = context;
        this.categoryItems = categoryItems;
        this.chipUnselectListener = chipUnselectListener;
        selectedItems = new SparseBooleanArray();
    }
    
    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvChips;
        ImageView ivRemoveChips;
        
        public CategoryViewHolder(View view) {
            super(view);
            
            tvChips = (TextView) view.findViewById(R.id.tvCategoryChip);
            //ivRemoveChips = (ImageView) view.findViewById(R.id.ivRemoveChip);
        }
    }
    
    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(context).inflate(R.layout.category_chips_item,parent,false));
    }
    
    @Override
    public void onBindViewHolder(CategoryViewHolder holder, final int position) {
        CategoryItem categoryItem = categoryItems.get(position);
        
        holder.tvChips.setText(categoryItem.getCategoryName());
        
        holder.tvChips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chipUnselectListener.onChipUnselected(position);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return categoryItems.size();
    }

}