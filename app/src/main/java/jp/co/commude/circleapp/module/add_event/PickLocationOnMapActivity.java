package jp.co.commude.circleapp.module.add_event;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.AndroidMapUtils;
import jp.co.commude.circleapp.utility.PermissionUtils;

public class PickLocationOnMapActivity extends AppCompatActivity
        implements GoogleMap.OnMapClickListener, OnMapReadyCallback, GoogleMap.OnCameraIdleListener {
    
    private GoogleMap mMap;
    
    LinearLayout llLocationDetails;
    TextView tvSelectedPlace, tvSelectedPlaceCameraPosition;
    ImageView ivGetPlace;
    
    private UiSettings mUiSettings;
    
    private FusedLocationProviderClient mFusedLocationClient;
    
    double lat, lng;
    LatLngBounds bound;
    
    protected GeoDataClient mGeoDataClient;
    
    public static final String TAG = "SearchOnMapActivity";
    public static final String MAP_LOCATION = "place_on_map";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_map_pick_location);
        
        /** Instantiate services  **/
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        
        mapFragment.getMapAsync(this);
        
        // Construct a GeoDataClient for the Google Places API for Android.
        mGeoDataClient = Places.getGeoDataClient(this, null);
        
        init();
    }
    
    void init(){
    
        llLocationDetails = (LinearLayout) findViewById(R.id.llLocationDetails);
        tvSelectedPlace = (TextView) findViewById(R.id.tvSelectedPlace);
        tvSelectedPlaceCameraPosition = (TextView) findViewById(R.id.tvSelectedPlaceCameraPosition);
        ivGetPlace = (ImageView) findViewById(R.id.ivGetPlace);
        ivGetPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                String selected_place = tvSelectedPlace.getText().toString();
                intent.putExtra(MAP_LOCATION,selected_place);
                setResult(AddEventActivity.REQUEST_MAP_LOCATION,intent);
                finish();
            }
        });
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        if(tvSelectedPlace.getText().toString()=="" && tvSelectedPlaceCameraPosition.getText().toString() == ""){
            llLocationDetails.setVisibility(View.GONE);
        } else {
            llLocationDetails.setVisibility(View.VISIBLE);
        }
    }
    
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
    
        mMap.setOnMapClickListener(this);
        
        mUiSettings = mMap.getUiSettings();
        
        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            /** Create permission prompt **/
    
            PermissionUtils.checkMultiplePermission(this, PermissionUtils.MULTIPLE_PERMISSION_LOCATION_REQUEST_CODE,getString(R.string.dialog_location_permission_message),
                    getString(R.string.dialog_location_permission_message), new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION});
            
        }   else if (googleMap != null ){
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
            
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                lat = location.getLatitude();
                                lng = location.getLongitude();
                                bound = AndroidMapUtils.boundsWithCenterAndLatLngDistance(new LatLng(lat,lng), 10000,10000);
                            }
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng), 13));
                        }
                    });
        }
    }
    
    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        mMap.addMarker(new MarkerOptions()
                .snippet("")
                .position(latLng)
                .icon(AndroidMapUtils.vectorToBitmap(this,
                                            R.drawable.ic_maps_and_flags,
                                            getResources().getColor(R.color.colorRed0))));
        
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = new ArrayList<>();
        String address = null;
        final String city;
        final String state;
        final String country;
        String knownName = null;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude,latLng.longitude,1);
            address = addresses.get(0).getAddressLine(0).toString();
            city = addresses.get(0).getLocality().toString();
            state = addresses.get(0).getAdminArea().toString();
        
            country = addresses.get(0).getCountryName().toString();
            knownName = addresses.get(0).getFeatureName().toString();
        
            llLocationDetails.setVisibility(View.VISIBLE);
            tvSelectedPlace.setText(address +", "+knownName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    @Override
    public void onCameraIdle() {
        tvSelectedPlaceCameraPosition.setText(mMap.getCameraPosition().toString());
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mMap.clear();
        
        if(tvSelectedPlace.getText().toString()=="" && tvSelectedPlaceCameraPosition.getText().toString() == ""){
            llLocationDetails.setVisibility(View.GONE);
        } else {
            llLocationDetails.setVisibility(View.VISIBLE);
        }
    
        Intent intent = getIntent();
        String selected_place = "";
        intent.putExtra(MAP_LOCATION,selected_place);
        setResult(AddEventActivity.REQUEST_MAP_LOCATION,intent);
        finish();
    
    }
}
