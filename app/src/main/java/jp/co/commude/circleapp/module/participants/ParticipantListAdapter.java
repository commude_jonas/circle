package jp.co.commude.circleapp.module.participants;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.UIControlsUtils;

/**
 * Created by vic_villanueva on 06/02/2018.
 */

public class ParticipantListAdapter extends RecyclerView.Adapter<ParticipantListAdapter.ParticipantViewHolder> {
    
    Context context;
    ArrayList<ParticipantItem> participantItems;
    
    OnParticipantsListeners onParticipantsListeners;
    int displaySize;
    
    public ParticipantListAdapter(Context context, ArrayList<ParticipantItem> participantItems, OnParticipantsListeners onParticipantsListeners) {
        this.context = context;
        this.participantItems = participantItems;
        this.onParticipantsListeners = onParticipantsListeners;
    }
    
    public static class ParticipantViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivParticipantProfile;
        public TextView tvParticipantCount;
        public RelativeLayout squareLayout;
        
        public ParticipantViewHolder(View view) {
            super(view);
            ivParticipantProfile = (ImageView) view.findViewById(R.id.ivParticipantProfile);
            tvParticipantCount = (TextView) view.findViewById(R.id.tvParticipant);
            squareLayout =(RelativeLayout) view.findViewById(R.id.constraints);
        }
    }
    
    @Override
    public ParticipantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_user_participant_item, parent, false);
            
            return new ParticipantViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(ParticipantViewHolder holder, int position) {
        ParticipantItem participantItem = participantItems.get(position);
        
        Bitmap ShrinkedImage = new UIControlsUtils().generateScaledBitmap2(context,participantItem.getParticipantProfile());
        
        //new UIControlsUtils().resizeImageByResolution(context, holder.squareLayout, 0.095,0.095);
        Glide.get(context).onLowMemory();
        Glide.with(context)
                .load(ShrinkedImage)
                .thumbnail(0.5f)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivParticipantProfile);
        
        if (position == displaySize-2){
            Glide.with(context)
                    .load(R.drawable.more_button)
                    .thumbnail(0.5f)
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.ivParticipantProfile);
        }
        
        if (position == displaySize-1){
            int numbersOfparticipants = participantItems.size() - (displaySize-2);
            holder.ivParticipantProfile.setVisibility(View.GONE);
            holder.tvParticipantCount.setVisibility(View.VISIBLE);
            holder.tvParticipantCount.setText("+"+numbersOfparticipants);
        }
        
        applyClickEvent(holder,position);
    }
    
    private void applyClickEvent(final ParticipantViewHolder holder, final int position) {
    
        holder.ivParticipantProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onParticipantsListeners.viewParticipants(position);
            }
        });
        
    }
    
    @Override
    public int getItemCount() {
        if(displaySize > participantItems.size()) {
            return participantItems.size();
        }
            return displaySize;
    }
    
    public void setDisplaySize(int displaySize) {
        this.displaySize = displaySize;
        notifyDataSetChanged();
    }
    
    public interface OnParticipantsListeners{
        void viewParticipants(int position);
    }
}
