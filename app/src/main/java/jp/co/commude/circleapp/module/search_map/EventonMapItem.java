package jp.co.commude.circleapp.module.search_map;

/**
 * Created by vic_villanueva on 21/02/2018.
 */

public class EventonMapItem {
    
    public int colorCategory;
    public String categoryName;
    public String eventName;
    public String eventDate;
    
    public EventonMapItem(int colorCategory, String categoryName, String eventName, String eventDate) {
        this.colorCategory = colorCategory;
        this.categoryName = categoryName;
        this.eventName = eventName;
        this.eventDate = eventDate;
    }
    
    
    public int getColorCategory() {
        return colorCategory;
    }
    
    public void setColorCategory(int colorCategory) {
        this.colorCategory = colorCategory;
    }
    
    public String getCategoryName() {
        return categoryName;
    }
    
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
    public String getEventName() {
        return eventName;
    }
    
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
    
    public String getEventDate() {
        return eventDate;
    }
    
    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }
}
