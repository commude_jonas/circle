package jp.co.commude.circleapp.module.main.feeds;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.UIControlsUtils;

/**
 * Created by vic_villanueva on 23/01/2018.
 */

public class RecommendedEventListAdapter extends RecyclerView.Adapter<RecommendedEventListAdapter.EventViewHolder> {

    Context context;
    ArrayList<RecommendedEventItem> recommendedEventItems;

    OnSelectRecommendedEventListener onSelectRecommendedEventListener;

    public RecommendedEventListAdapter(Context context, ArrayList<RecommendedEventItem> recommendedEventItems, OnSelectRecommendedEventListener onSelectRecommendedEventListener) {
        this.context = context;
        this.recommendedEventItems = recommendedEventItems;
        this.onSelectRecommendedEventListener = onSelectRecommendedEventListener;
    }

    public static class EventViewHolder extends RecyclerView.ViewHolder {
        ImageView ivRecommendedEventCover;
        TextView tvRecommendedEventTime, tvRecommendedEventPlace;
        ConstraintLayout constraintLayout;
        FrameLayout frameLayout2;
        public EventViewHolder(View view) {
            super(view);
            ivRecommendedEventCover = (ImageView) view.findViewById(R.id.ivCover);
            tvRecommendedEventTime = (TextView) view.findViewById(R.id.tvRecommendedEventTime);
            tvRecommendedEventPlace = (TextView) view.findViewById(R.id.tvRecommendedEventPlace);
            frameLayout2 = (FrameLayout) view.findViewById(R.id.frameLayout2);
            constraintLayout = (ConstraintLayout) view.findViewById(R.id.constraintLayout);
        }
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.horizontal_eventview_item,parent,false);

        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        RecommendedEventItem recommendedEventItem = recommendedEventItems.get(position);

        holder.tvRecommendedEventTime.setText(recommendedEventItem.getEventDate());
        holder.tvRecommendedEventPlace.setText(recommendedEventItem.getEventDesc());
    
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT
        );
        
        Bitmap ShrinkedImage = new UIControlsUtils().generateScaledBitmap2(context,recommendedEventItem.getEventImg());

        new UIControlsUtils().resizeByResolution(context, holder.constraintLayout);

        Glide.with(context)
                .load(ShrinkedImage)
                .thumbnail(0.5f)
                .apply(RequestOptions.noTransformation())
                .into(holder.ivRecommendedEventCover);
    
        applyClickEvent(holder,position);
    }
    
    private void applyClickEvent(final EventViewHolder holder, final int position) {
        
        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectRecommendedEventListener.onSelectRecommendedEvent(holder.ivRecommendedEventCover,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recommendedEventItems.size();
    }
    
    public interface OnSelectRecommendedEventListener {
        void onSelectRecommendedEvent(View v, int position);
    }
}
