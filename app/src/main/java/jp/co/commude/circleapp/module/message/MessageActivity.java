package jp.co.commude.circleapp.module.message;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by vic_villanueva on 09/02/2018.
 */

public class MessageActivity extends AppCompatActivity implements View.OnClickListener {
    
    RecyclerView rvMessage;
    ChatMessageAdapter chatMessageAdapter;
    ArrayList<ChatMessageItem> chatMessageItems;
    
    EditText etMessage;
    TextView tvSendMessage;
    LinearLayoutManager layoutManager;
    
    ImageView ivProfilePicture;
    ImageView ivBackpress;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        setContentView(R.layout.activity_message);
        init();
    }
    
    void init(){
    
        rvMessage = (RecyclerView) findViewById(R.id.rvMessages);
        chatMessageItems = new ArrayList<>();
    
        Globals.ChatMessageItems(this,chatMessageItems,R.array.str_messages,R.array.str_message_time);
        
        chatMessageAdapter = new ChatMessageAdapter(this, chatMessageItems);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);
        rvMessage.setLayoutManager(layoutManager);
        rvMessage.setItemAnimator(new DefaultItemAnimator());
        rvMessage.setHasFixedSize(true);
        rvMessage.setAdapter(chatMessageAdapter);
        chatMessageAdapter.notifyDataSetChanged();
        
        etMessage = (EditText) findViewById(R.id.etMessage);
        tvSendMessage = (TextView) findViewById(R.id.tvSendMessage);
        tvSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dummyInsertMessage();
            }
        });
    
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        ivProfilePicture = (ImageView) findViewById(R.id.ivProfilePicture);
        
        ivBackpress.setOnClickListener(this);
        Glide.with(this)
                .load(R.drawable.profile_picture)
                .thumbnail(0.5f)
                .transition(withCrossFade())
                .apply(RequestOptions.circleCropTransform())
                .into(ivProfilePicture);
    
        etMessage.setHint(getString(R.string.str_hint_display));
        
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
        }
    }
    
    private void dummyInsertMessage(){
        chatMessageItems.add(new ChatMessageItem(0, 1, "event1", etMessage.getText().toString(), "02/27", "Jerome"));
        chatMessageAdapter.notifyDataSetChanged();
        rvMessage.smoothScrollToPosition(chatMessageItems.size());
        layoutManager.scrollToPosition(chatMessageItems.size());
    }
    
    public static void open(Activity activity){
        activity.startActivity(new Intent(activity,MessageActivity.class));
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
}
