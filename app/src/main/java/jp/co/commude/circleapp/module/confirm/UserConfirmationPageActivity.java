package jp.co.commude.circleapp.module.confirm;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.registration.UserRegistrationPageActivity;
import jp.co.commude.circleapp.utility.UIControlsUtils;

/**
 * Created by vic_villanueva on 31/01/2018.
 */

public class UserConfirmationPageActivity extends AppCompatActivity implements View.OnClickListener {
    
    ImageView ivBackPress;
    ImageView ivCircleApp;
    ConstraintLayout confirm;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_confirmation_page);
        
        ivBackPress = (ImageView) findViewById(R.id.ivBackpress);
        ivCircleApp = (ImageView) findViewById(R.id.ivCircleApp);
    
        new UIControlsUtils().resizeImageByResolution(this, ivCircleApp, 0.15, 0.15);
    
        confirm = (ConstraintLayout) findViewById(R.id.confirm);
        
        ivBackPress.setOnClickListener(this);
    
        confirm.setOnClickListener(this);
        
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.confirm:
    
                startActivity(new Intent(UserConfirmationPageActivity.this,UserPasswordConfirmationPageActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finishAfterTransition();
                break;
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UserConfirmationPageActivity.this,UserRegistrationPageActivity.class));
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
        finishAfterTransition();
    }
}
