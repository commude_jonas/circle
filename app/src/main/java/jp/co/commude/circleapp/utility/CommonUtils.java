package jp.co.commude.circleapp.utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.camera.CustomCameraActivity;
import jp.co.commude.circleapp.module.gallery.GalleryActivity;

/**
 * Created by vic_villanueva on 27/02/2018.
 */

public class CommonUtils {
    
    public static void showBottomSheetCameraLibrary(final Activity activity){
        //R.layout.bottomsheet_add_photo_event
        View view = activity.getLayoutInflater().inflate(R.layout.bottomsheet_add_photo_event, null);
        
        final BottomSheetDialog dialog = new BottomSheetDialog(activity, R.style.TransparentTheme);
        dialog.setContentView(view);
        dialog.show();
        
        TextView tvGoCamera = (TextView) dialog.findViewById(R.id.tvGoCamera);
        TextView tvGoLibrary = (TextView) dialog.findViewById(R.id.tvGoLibrary);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        
        tvGoCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CameraUtils.checkCameraHardware(activity)) {
                    Intent intent = new Intent(activity, CustomCameraActivity.class);
                    activity.startActivityForResult(intent, 0);
                } else {
                    Toast.makeText(activity, "Camera is not available for now", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        
        tvGoLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, GalleryActivity.class);
                activity.startActivityForResult(intent, 0);
                dialog.dismiss();
            }
        });
        
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        
    }
    
    public static void showDefaultDialog(final Activity activity){
        //R.layout.dialog
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_join_confirmation, null);
        
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(view);
        dialog.show();
    
        TextView tvJoin = (TextView) dialog.findViewById(R.id.tvJoin);
        TextView tvFollow = (TextView) dialog.findViewById(R.id.tvFollow);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        
        tvJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "Joined", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        
        tvFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "Follow", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "Cancel", Toast.LENGTH_SHORT).show();
            }
        });
    }
    
    public static void loadFragment(final FragmentActivity activity, final int layoutResId, final Fragment fragment, final String CURRENT_TAG){
    
        final Handler mHandler = new Handler();
        
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment mFragment = fragment;
                FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(layoutResId, mFragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
                
            }
        };
        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
        
    }
    
    public static void showBottomShareEventToSocial(final Context context, int position){
        final BottomSheetDialog dialog;
    
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        View view = layoutInflater.inflate(R.layout.fragment_bottom_sheet_social, null);
    
        ImageView ivTwitter, ivFacebook, ivGooglePlus;
        TextView btCancel;
    
        ivTwitter = (ImageView)view.findViewById(R.id.ibTwitter);
        ivFacebook = (ImageView)view.findViewById(R.id.ibFacebook);
        ivGooglePlus = (ImageView)view.findViewById(R.id.ibGooglePlus);
        btCancel = (TextView) view.findViewById(R.id.btCancel);
    
        dialog = new BottomSheetDialog(context,R.style.TransparentTheme);
        dialog.setContentView(view);
        dialog.show();
    
        ivTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context.getApplicationContext(), "Twitter shared", Toast.LENGTH_SHORT).show();
            }
        });
    
        ivFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context.getApplicationContext(), "Facebook shared", Toast.LENGTH_SHORT).show();
            }
        });
    
        ivGooglePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context.getApplicationContext(), "Google Plus shared", Toast.LENGTH_SHORT).show();
            }
        });
    
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(context.getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
            }
        });
    }
    
}
