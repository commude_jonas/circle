package jp.co.commude.circleapp.model;

import android.content.Context;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.album.AlbumPictureItem;
import jp.co.commude.circleapp.module.category.CategoryItem;
import jp.co.commude.circleapp.module.comments.CommentItem;
import jp.co.commude.circleapp.module.followers.FollowersItem;
import jp.co.commude.circleapp.module.main.feeds.PublicEventItem;
import jp.co.commude.circleapp.module.main.feeds.RecommendedEventItem;
import jp.co.commude.circleapp.module.main.notifications.NotificationItem;
import jp.co.commude.circleapp.module.message.ChatMessageItem;
import jp.co.commude.circleapp.module.participants.ParticipantItem;

/**
 * Created by vic_villanueva on 14/02/2018.
 */
public class Globals {
    
    public static final String FEEDS =  "feeds";
    public static final String FRIENDS =  "friends";
    public static final String EVENTS =  "event";
    public static final String NOTIFICATIONS =  "notifications";
    public static final String PROFILE =  "profile";
    
    public static final int[] IMG_RESOURCES = {R.drawable.pic1,
            R.drawable.pic2,
            R.drawable.pic3,
            R.drawable.pic4,
            R.drawable.pic5,
            R.drawable.pic6,
            R.drawable.pic7,
            R.drawable.pic8,
            R.drawable.pic9,
            R.drawable.pic10,
            R.drawable.pic11,
            R.drawable.pic12,
            R.drawable.pic13,
            R.drawable.pic14,
            R.drawable.pic15,
            R.drawable.pic16,
            R.drawable.pic17,
            R.drawable.pic18,
            R.drawable.pic19,
            R.drawable.pic20,
            R.drawable.pic21};
    
    public static final int[] IMG_RESOURCES2 = {R.drawable.sample_pic14,
            R.drawable.kabute,
            R.drawable.lente,
            R.drawable.palay,
            R.drawable.strawberry,
            R.drawable.summer};
    
    public static final int[] IMG_RESOURCES3 = {
            R.drawable.sample_pic14,
            R.drawable.kabute,
            R.drawable.lente,
            R.drawable.summer,
            R.drawable.palay,
            R.drawable.summer,
            R.drawable.strawberry,
            R.drawable.palay,
            R.drawable.summer,
            R.drawable.lente,
            R.drawable.palay,
            R.drawable.summer,
            R.drawable.palay,
            R.drawable.summer,
            R.drawable.palay };
    
    public static final int[] IMG_RESOURCES4 = {
            R.drawable.luis,
            R.drawable.rico};
    
    public static final int[] IMG_RECOMMENDED = {R.drawable.sample_pic13,
            R.drawable.sample_pic1,
            R.drawable.sample_pic2,
            R.drawable.sample_pic5,
            R.drawable.sample_pic6,
            R.drawable.sample_pic7,
            R.drawable.sample_pic8,
            R.drawable.sample_pic9,
            R.drawable.sample_pic10,
            R.drawable.sample_pic11,
            R.drawable.sample_pic10,
            R.drawable.sample_pic12};
    
    
    
    public static String[] string_array_resources(Context context, int resStringArray){
        //return context.getResources().getStringArray(R.array.str_res);
        return context.getResources().getStringArray(resStringArray);
    }
    public static String string_resource(Context context, int resStringArray){
        //return context.getResources().getStringArray(R.array.str_res);
        return context.getResources().getString(resStringArray);
    }
    
    public static ArrayList<CategoryItem> CategoryItems(Context context, ArrayList<CategoryItem> categoryItems, int resStringArray){
        String[] img_str = string_array_resources(context, resStringArray);
        for(int i=0;i<IMG_RESOURCES.length;i++){
            categoryItems.add(new CategoryItem(img_str[i].toString(),IMG_RESOURCES[i]));
        }
        return categoryItems;
    }
    public static ArrayList<CategoryItem> CategoryItems(Context context, ArrayList<CategoryItem> categoryItems, int resStringArray, int num, int start){
        String[] img_str = string_array_resources(context, resStringArray);
        for(int i=start;i<num;i++){
            categoryItems.add(new CategoryItem(img_str[i].toString(),IMG_RESOURCES[i]));
        }
        return categoryItems;
    }
    
    public static ArrayList<AlbumPictureItem> AlbumPictures(Context context, ArrayList<AlbumPictureItem> albumPictureItems){
        for(int i=0;i<IMG_RESOURCES2.length;i++){
            albumPictureItems.add(new AlbumPictureItem(IMG_RESOURCES2[i]));
        }
        return albumPictureItems;
    }
    
    public static ArrayList<ParticipantItem> ParticipantItems(ArrayList<ParticipantItem> participantItems){
        for(int i=0;i<IMG_RESOURCES3.length;i++){
            participantItems.add(new ParticipantItem(IMG_RESOURCES3[i]));
        }
        return participantItems;
    }
    
    public static ArrayList<CommentItem> CommentItems(Context context, ArrayList<CommentItem> commentItems, int nameArray, int commentArray, int dateArray){
        String[] arry_name = string_array_resources(context,nameArray);
        String[] arry_comment = string_array_resources(context,commentArray);
        String[] arry_date = string_array_resources(context,dateArray);
        for(int i=0;i<IMG_RESOURCES4.length;i++){
            commentItems.add(new CommentItem(arry_name[i],arry_comment[i],arry_date[i],IMG_RESOURCES4[i]));
        }
        return commentItems;
    }
    
    
    public static ArrayList<ChatMessageItem> ChatMessageItems(Context context, ArrayList<ChatMessageItem> chatMessageItems, int messageArray, int dateArray){
        int[] array_type = {0,1};
        int[] array_id = {1,2};
        String[] array_message = string_array_resources(context,messageArray);
        String[] arry_date = string_array_resources(context,dateArray);
        for(int i=0;i<array_message.length;i++){
            chatMessageItems.add(new ChatMessageItem(array_type[i],array_id[i],array_message[i],arry_date[i]));
        }
        return chatMessageItems;
    }
    
    public static ArrayList<RecommendedEventItem> RecommendedEventItems(Context context, ArrayList<RecommendedEventItem> recommendedEventItems){
        String date = string_resource(context,R.string.str_time_default);
        String desc = string_resource(context,R.string.str_event_title_in_recommended);
        
        for(int i=0;i<IMG_RECOMMENDED.length;i++){
            recommendedEventItems.add(new RecommendedEventItem(date,desc,IMG_RECOMMENDED[i]));
        }
        return recommendedEventItems;
    }
    public static ArrayList<PublicEventItem> PublicEventItems(Context context, ArrayList<PublicEventItem> publicEventItems){
        String name = string_resource(context,R.string.str_name_event);
        String time = string_resource(context,R.string.str_time_default);
        String place = string_resource(context,R.string.str_place_default);
        String participants = string_resource(context,R.string.str_participants_default);
        String fee = string_resource(context,R.string.str_entry_fee_default);
        for(int i=0;i<IMG_RECOMMENDED.length;i++){
            publicEventItems.add(new PublicEventItem(IMG_RECOMMENDED[i],name,place,time,participants,fee));
        }
        return publicEventItems;
    }
    
    public static ArrayList<FollowersItem> FollowerItems(ArrayList<FollowersItem> followersItems, boolean isFriend){
        String[] name = {"Clyde Lynch", "Winifred Boone","Mable Martinez","Willie Mitchell","Alan Gibson"};
        
        for(int i=0;i<name.length;i++){
            followersItems.add(new FollowersItem(R.drawable.profile_picture,name[i],isFriend));
        }
        return followersItems;
    }
    
    public static final String[] name1 = {"Tinablock","jlo"};
    public static final String[] name2 = {"管理者","jlo","管理者"};
    public static final String[] desc1 = {"Live Androidを参加しました。","ユッキーをフォローしました。"};
    public static final String[] desc2 = {"あなたのLive iOSのイベントに参加者jloさんが追 加されました。", "ユッキーさんからメッセージがありま す。","Live Androidのイベントが今日あります。"};
    
    public static final String[] date1 = {"今","3時間前"};
    public static final String[] date2 = {"2017/12/12","2017/12/11","2017/12/10"};
    
    public static final int[] notifimage1 = {R.drawable.luis,R.drawable.yassi};
    public static final int[] notifimage2 = {R.drawable.luis,R.drawable.yassi,R.drawable.rico};
    
    public static ArrayList<NotificationItem> NotificationItems(ArrayList<NotificationItem> notificationItems, String[] name, String[] desc, String[] date, boolean read, int[] image){
        for(int i=0; i<name.length;i++){
            notificationItems.add(new NotificationItem(name[i],desc[i],date[i],read,image[i]));
        }
        return notificationItems;
    }
}