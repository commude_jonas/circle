package jp.co.commude.circleapp.module.followers;

/**
 * Created by vic_villanueva on 19/02/2018.
 */

public class FollowersItem {
    
    int followerImg;
    String followerName;
    boolean isFriend;
    
    public FollowersItem(int followerImg, String followerName, boolean isFriend) {
        this.followerImg = followerImg;
        this.followerName = followerName;
        this.isFriend = isFriend;
    }
    
    public int getFollowerImg() {
        return followerImg;
    }
    
    public void setFollowerImg(int followerImg) {
        this.followerImg = followerImg;
    }
    
    public String getFollowerName() {
        return followerName;
    }
    
    public void setFollowerName(String followerName) {
        this.followerName = followerName;
    }
    
    public boolean isFriend() {
        return isFriend;
    }
    
    public void setFriend(boolean friend) {
        isFriend = friend;
    }
}
