package jp.co.commude.circleapp.module.splash_first_screen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.module.login.UserLoginPageActivity;
import jp.co.commude.circleapp.module.main.MainActivity;
import jp.co.commude.circleapp.utility.PermissionUtils;

/**
 * Created by vic_villanueva on 31/01/2018.
 */

public class FirstTimeUserActivity extends AppCompatActivity implements View.OnClickListener {
    
    ImageView ivFirstUiExp;
    
    ConstraintLayout firstuser;
    
    TextView btNotMemberFirstSee;
    
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_first_page);
        
        ivFirstUiExp = (ImageView) findViewById(R.id.ivFirstUiExp);
        
        firstuser = (ConstraintLayout) findViewById(R.id.firstuser);
        
        btNotMemberFirstSee = (TextView) findViewById(R.id.btNotMemberFirstSee);
        
        firstuser.setOnClickListener(this);
        btNotMemberFirstSee.setOnClickListener(this);
    
        PermissionUtils.checkPermission(this,
                PermissionUtils.LOCATION_PERMISSION_REQUEST_CODE,
                Manifest.permission.ACCESS_FINE_LOCATION, getString(R.string.dialog_location_permission_message),
                getString(R.string.dialog_location_permission_message));
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        //checkPermission();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.firstuser:
                startActivity(new Intent(FirstTimeUserActivity.this,UserLoginPageActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finishAfterTransition();

                //FillGapScrollViewActivity.open(this);
                //SampleParallaxActivity.open(this);
                return;
            case R.id.btNotMemberFirstSee:
    
                startActivity(new Intent(FirstTimeUserActivity.this,MainActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finishAfterTransition();
                
                return;
        }
    }
}
