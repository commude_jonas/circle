package jp.co.commude.circleapp.module.gallery;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.CustomBounceInterpolator;

/**
 * Created by vic_villanueva on 23/02/2018.
 */

public class GalleryImageListAdapter extends RecyclerView.Adapter<GalleryImageListAdapter.GalleryViewHolder> {
    Context context;
    Cursor cursor;
    int columnIndex;
    
    private SparseBooleanArray selectedItems;
    
    private int currentSelectedIndex = -1;
    
    OnSelectImageListener onSelectImageListener;
    
    public GalleryImageListAdapter(Context context, Cursor cursor, int columnIndex, OnSelectImageListener onSelectImageListener) {
        this.context = context;
        this.cursor = cursor;
        this.columnIndex = columnIndex;
        this.onSelectImageListener = onSelectImageListener;
        selectedItems = new SparseBooleanArray();
    }
    
    public class GalleryViewHolder extends RecyclerView.ViewHolder {
    
        ImageView ivCheckAdd;
        ImageView ivGalleryPicture;
        RelativeLayout picGallery;
        
        public GalleryViewHolder(View itemView) {
            super(itemView);
            ivCheckAdd = (ImageView) itemView.findViewById(R.id.ivCheckAdd);
            ivGalleryPicture = (ImageView) itemView.findViewById(R.id.ivGalleryPicture);
            picGallery = (RelativeLayout) itemView.findViewById(R.id.picGallery);
        }
    }
    
    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GalleryViewHolder(LayoutInflater.from(context).inflate(R.layout.gallery_item,parent,false));
    }
    
    @Override
    public void onBindViewHolder(final GalleryViewHolder holder, final int position) {
        //holder.ivGalleryPicture.setImageResource();
        cursor.moveToPosition(position);
        int imageID = cursor.getInt(columnIndex);
        
        holder.ivGalleryPicture.setImageURI(Uri.withAppendedPath(
                MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "" + imageID));
        
        holder.itemView.setActivated(selectedItems.get(position, false));
        holder.ivGalleryPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectImageListener.onImageSelected(position);
    
                ObjectAnimator animX = ObjectAnimator.ofFloat(holder.picGallery, "scaleX", 0.8f, 1f);
                ObjectAnimator animY = ObjectAnimator.ofFloat(holder.picGallery, "scaleY", 0.8f, 1f);
                ObjectAnimator pivX = ObjectAnimator.ofFloat(holder.picGallery, "pivotX", 100);
                ObjectAnimator pivY = ObjectAnimator.ofFloat(holder.picGallery, "pivotY", 100);
                
                CustomBounceInterpolator customBounceInterpolator = new CustomBounceInterpolator(0.5,5);
    
                AnimatorSet animSetXY = new AnimatorSet();
                animSetXY.setInterpolator(customBounceInterpolator);
                animSetXY.setDuration(500);//1sec
                animSetXY.playTogether(animX,animY,pivX,pivY);
                animSetXY.setTarget(holder.picGallery);
                animSetXY.start();
            }
        });
        
        applySelected(holder,position);
    }
    @Override
    public int getItemCount() {
        return cursor.getCount();
    }
    
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
    
    public int getSelectedItemCount(){
        return selectedItems.size();
    }
    
    private void applySelected(GalleryViewHolder holder, int position) {
        if (selectedItems.get(position, false)) {
            holder.ivCheckAdd.setVisibility(View.VISIBLE);
        } else {
            holder.ivCheckAdd.setVisibility(View.GONE);
        }
    }
    
    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            if(getSelectedItemCount() < 20) {
                selectedItems.put(pos, true);
            }
        }
        notifyItemChanged(pos);
    }
    
    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }
    
    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }
    
    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }
    
    public interface OnSelectImageListener {
        void onImageSelected(int position);
    }
}
