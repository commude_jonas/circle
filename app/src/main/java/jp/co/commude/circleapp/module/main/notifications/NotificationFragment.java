package jp.co.commude.circleapp.module.main.notifications;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;
import jp.co.commude.circleapp.module.events.EventDetailsActivity;
import jp.co.commude.circleapp.module.profile.UserProfileActivity;

/**
 * Created by vic_villanueva on 25/01/2018.
 */

public class NotificationFragment extends Fragment implements NotificationAdapter.EventListener {
    
    public NotificationFragment() {
    }
    
    public static NotificationFragment newInstance() {
        
        Bundle args = new Bundle();
        
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    RecyclerView rvNewNotif, rvOldNotif;
    NotificationAdapter notificationAdapter, notificationAdapter2;
    ArrayList<NotificationItem> notificationItems1, notificationItems2;
    
    private Paint p = new Paint();
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification,container,false);
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvNewNotif = (RecyclerView) view.findViewById(R.id.rvNewNotif);
        rvOldNotif = (RecyclerView) view.findViewById(R.id.rvOldNotif);
        
        notificationItems1 = new ArrayList<>();
        notificationItems2 = new ArrayList<>();
    
        Globals.NotificationItems(notificationItems1,Globals.name1,Globals.desc1,Globals.date1, false, Globals.notifimage1);
        Globals.NotificationItems(notificationItems2,Globals.name2,Globals.desc2,Globals.date2, true, Globals.notifimage2);
        
        notificationAdapter = new NotificationAdapter(getActivity(), notificationItems1,this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvNewNotif.setLayoutManager(layoutManager);
        rvNewNotif.setItemAnimator(new DefaultItemAnimator());
        rvNewNotif.setHasFixedSize(true);
        rvNewNotif.setAdapter(notificationAdapter);
        notificationAdapter.notifyDataSetChanged();
        
        notificationAdapter2 = new NotificationAdapter(getActivity(), notificationItems2,this);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        rvOldNotif.setLayoutManager(layoutManager2);
        rvOldNotif.setItemAnimator(new DefaultItemAnimator());
        rvOldNotif.setHasFixedSize(true);
        rvOldNotif.setAdapter(notificationAdapter2);
        notificationAdapter2.notifyDataSetChanged();
     
        initSwipe();
    }
    
    private void initSwipe(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                
                if (direction == ItemTouchHelper.LEFT){
                    notificationAdapter2.removeItem(position);
                    notificationAdapter2.notifyDataSetChanged();
                } else {
                    //notificationAdapter2.removeItem(position);
                }
            }
            
            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                
                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
                    
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    
                    if(dX > 0){
                        p.setColor(getResources().getColor(R.color.colorRed0));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_remove_item);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    } else if (dX == 0) {
                    
                    } else {
                        p.setColor(getResources().getColor(R.color.colorRed0));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background,p);
                        
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_remove_item2);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rvOldNotif);
        
    }
    
    @Override
    public void gotoEvent(int position) {
        startActivity(new Intent(getActivity(), EventDetailsActivity.class));
        getActivity().finish();
    }
    
    @Override
    public void gotoProfile(int position) {
        startActivity(new Intent(getActivity(), UserProfileActivity.class));
        getActivity().finish();
    }
    
    @Override
    public void onSwipe(int position) {
    
    }
}
