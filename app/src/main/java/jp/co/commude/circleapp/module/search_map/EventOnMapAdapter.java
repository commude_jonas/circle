package jp.co.commude.circleapp.module.search_map;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.UIControlsUtils;

/**
 * Created by vic_villanueva on 21/02/2018.
 */

public class EventOnMapAdapter extends RecyclerView.Adapter<EventOnMapAdapter.EventonMapViewHolder> {
    
    Context context;
    ArrayList<EventonMapItem> onEventonMapItems;
    
    OnSelectEventListener onSelectEventListener;
    
    public EventOnMapAdapter(Context context, ArrayList<EventonMapItem> onEventonMapItems, OnSelectEventListener onSelectEventListener) {
        this.context = context;
        this.onEventonMapItems = onEventonMapItems;
        this.onSelectEventListener = onSelectEventListener;
    }
    
    public class EventonMapViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    
        public TextView tvEventType;
        public TextView tvEventName;
        public TextView tvEventDate;
        public FrameLayout cardContainer;
        public CoordinatorLayout container;
        public EventonMapViewHolder(View view) {
            super(view);
            tvEventType = view.findViewById(R.id.tvEventType);
            tvEventName = view.findViewById(R.id.tvEventName);
            tvEventDate = view.findViewById(R.id.tvEventDate);
            cardContainer = view.findViewById(R.id.cvCard);
            container = view.findViewById(R.id.container);
            cardContainer.setOnClickListener(this);
        }
    
        @Override
        public void onClick(View v) {
            onSelectEventListener.onSelectEvent(tvEventType,getAdapterPosition());
        }
    }
    
    @Override
    public EventonMapViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventonMapViewHolder(LayoutInflater.from(context).inflate(R.layout.event_card_on_map_item,parent,false));
    }
    
    @Override
    public void onBindViewHolder(EventonMapViewHolder holder, int position) {
        EventonMapItem eventonMapItem = onEventonMapItems.get(position);
    
        new UIControlsUtils().resizeOnMap(context, holder.container);
        
        holder.tvEventType.setText(eventonMapItem.getCategoryName());
        holder.tvEventName.setText(eventonMapItem.getEventName());
        holder.tvEventDate.setText(eventonMapItem.getEventDate());
        
        holder.tvEventType.getBackground().setColorFilter(context.getResources().getColor(eventonMapItem.getColorCategory()), PorterDuff.Mode.SRC);
       
    }
    
    void applyListener(final EventonMapViewHolder holder, final int position){
    
        holder.tvEventType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectEventListener.onSelectEvent(holder.tvEventType, position);
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return onEventonMapItems.size();
    }
    
    public interface OnSelectEventListener{
        void onSelectEvent(View v, int position);
    }
}
