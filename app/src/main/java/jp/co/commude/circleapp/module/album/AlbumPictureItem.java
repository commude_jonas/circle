package jp.co.commude.circleapp.module.album;

/**
 * Created by vic_villanueva on 05/02/2018.
 */

public class AlbumPictureItem {
    
    int pictureImg;
    boolean selected;
    
    public AlbumPictureItem(int pictureImg) {
        this.pictureImg = pictureImg;
    }
    
    public int getPictureImg() {
        return pictureImg;
    }
    
    public void setPictureImg(int pictureImg) {
        this.pictureImg = pictureImg;
    }
    
    public boolean isSelected() {
        return selected;
    }
    
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
