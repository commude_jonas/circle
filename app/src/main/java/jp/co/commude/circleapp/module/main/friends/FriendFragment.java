package jp.co.commude.circleapp.module.main.friends;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.utility.PermissionUtils;

/**
 * Created by vic_villanueva on 25/01/2018.
 */

public class FriendFragment extends Fragment implements RecommendedFriendsAdapter.RecommendedFriendSelectListener, PhoneContactsAdapter.PhoneContactSelectListener {
    
    RecyclerView rvRecommendedFriends, rvPhoneContacts;
    AutoCompleteTextView acSearchFriends;
    
    RecommendedFriendsAdapter recommendedFriendsAdapter;
    ArrayList<RecommendedFriendsItem> recommendedFriendsItems;
    
    PhoneContactsAdapter phoneContactsAdapters;
    ArrayList<PhoneContactsItem> phoneContactsItems;
    
    TextView btConnectContact;
    FrameLayout contactbutton;
    
    public FriendFragment() {
    }
    
    public static FriendFragment newInstance() {
        
        Bundle args = new Bundle();
        
        FriendFragment fragment = new FriendFragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_find_friends,container,false);
    }
    
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvRecommendedFriends = (RecyclerView) view.findViewById(R.id.rvRecommendedFriends);
        rvPhoneContacts = (RecyclerView) view.findViewById(R.id.rvPhoneContacts);
        
        recommendedFriendsItems = new ArrayList<>();
        recommendedFriendsItems.add(new RecommendedFriendsItem(R.drawable.profile_picture, "yennychristiscute","23件", false));
        recommendedFriendsItems.add(new RecommendedFriendsItem(R.drawable.profile_picture, "jilo","20件",true));
        recommendedFriendsItems.add(new RecommendedFriendsItem(R.drawable.profile_picture, "fit_yoga_girl","12件",false));
        recommendedFriendsItems.add(new RecommendedFriendsItem(R.drawable.profile_picture, "jonaskie","25件",false));
    
        recommendedFriendsAdapter = new RecommendedFriendsAdapter(getActivity(),recommendedFriendsItems,this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvRecommendedFriends.setLayoutManager(layoutManager);
        rvRecommendedFriends.setHasFixedSize(true);
        rvRecommendedFriends.setItemAnimator(new DefaultItemAnimator());
        rvRecommendedFriends.setAdapter(recommendedFriendsAdapter);
        recommendedFriendsAdapter.notifyDataSetChanged();
        
        phoneContactsItems = new ArrayList<>();
        phoneContactsItems.add(new PhoneContactsItem(R.drawable.profile_picture,"Tinablock", true));
        phoneContactsItems.add(new PhoneContactsItem(R.drawable.profile_picture,"Taro Yamata", true));
        phoneContactsItems.add(new PhoneContactsItem(R.drawable.profile_picture,"elenama yoga", false));
        phoneContactsItems.add(new PhoneContactsItem(R.drawable.profile_picture,"Tsutsiu", true));
        phoneContactsItems.add(new PhoneContactsItem(R.drawable.profile_picture,"ymenedettops",true));
        
        phoneContactsAdapters = new PhoneContactsAdapter(getActivity(),phoneContactsItems, this);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvPhoneContacts.setLayoutManager(layoutManager2);
        rvPhoneContacts.setItemAnimator(new DefaultItemAnimator());
        rvPhoneContacts.setHasFixedSize(true);
        rvPhoneContacts.setAdapter(phoneContactsAdapters);
        phoneContactsAdapters.notifyDataSetChanged();
        
        btConnectContact = (TextView) view.findViewById(R.id.btConnectContacts);
        contactbutton = (FrameLayout) view.findViewById(R.id.contactbutton);
        btConnectContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestContactConnect();
            }
        });
        shownContacts();
    }
    
    private void shownContacts() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
        } else {
            if(contactbutton.isShown()) {
                requestContactConnect();
            }
        }
    }
    private void requestContactConnect(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
        
            PermissionUtils.checkPermission(getActivity(), PermissionUtils.PHONE_CONTACTS_PERMISSION_REQUEST_CODE, Manifest.permission.READ_CONTACTS,
                    getString(R.string.dialog_phone_contacts_permission_message),getString(R.string.dialog_phone_contacts_permission_message));
       
        } else {
            if(!rvPhoneContacts.isShown()) {
                rvPhoneContacts.setVisibility(View.VISIBLE);
                contactbutton.setVisibility(View.GONE);
            }
        }
    }
    @Override
    public void onRecommendedFriendSelect(View v, View v2, View v3, int position) {
    
        RecommendedFriendsItem recommendedFriendsItem = recommendedFriendsItems.get(position);
        
        TextView tvMutualStatus = (TextView) v2;
        ImageView ivMutualStatus = (ImageView) v3;
        
        if(recommendedFriendsItem.isFriend()) {
            recommendedFriendsItem.setFriend(false);
            v.setBackground(getActivity().getResources().getDrawable(R.drawable.following_user_bg));
            tvMutualStatus.setText(getActivity().getString(R.string.str_following_text));
            tvMutualStatus.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            ivMutualStatus.setImageResource(R.drawable.ic_check_followers);
            ivMutualStatus.setColorFilter(getActivity().getResources().getColor(R.color.colorPrimary));
        } else {
            recommendedFriendsItem.setFriend(true);
            v.setBackground(getActivity().getResources().getDrawable(R.drawable.follow_user_bg));
            tvMutualStatus.setText(getActivity().getString(R.string.str_follow_text));
            tvMutualStatus.setTextColor(getActivity().getResources().getColor(R.color.colorRed0));
            ivMutualStatus.setImageResource(R.drawable.ic_icon_plus_pink);
        }
    }
    
    @Override
    public void onPhoneContactSelect(View v, View v2, View v3,  int position) {
        PhoneContactsItem contactsItem = phoneContactsItems.get(position);
    
        TextView tvMutualStatus = (TextView) v2;
        ImageView ivMutualStatus = (ImageView) v3;
    
        if(contactsItem.isUser()){
            contactsItem.setUser(false);
            v.setBackground(getActivity().getResources().getDrawable(R.drawable.follow_user_bg));
            tvMutualStatus.setTextColor(getActivity().getResources().getColor(R.color.colorRed0));
            tvMutualStatus.setText(getString(R.string.str_follow_text2));
            ivMutualStatus.setImageResource(R.drawable.ic_icon_plus_pink);
        } else {
            contactsItem.setUser(true);
            v.setBackground(getActivity().getResources().getDrawable(R.drawable.following_user_bg));
            tvMutualStatus.setTextColor(getActivity().getResources().getColor(R.color.colorGray3));
            tvMutualStatus.setText(getString(R.string.str_following_text));
            ivMutualStatus.setImageResource(R.drawable.ic_icon_plus_pink);
            ivMutualStatus.setColorFilter(getActivity().getResources().getColor(R.color.colorGray3));
        }
    }
    
}
