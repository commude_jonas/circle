package jp.co.commude.circleapp.module.category;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jp.co.commude.circleapp.R;
import jp.co.commude.circleapp.model.Globals;

/**
 * Created by vic_villanueva on 23/02/2018.
 */

public class UserAddCategoryActivity extends AppCompatActivity
        implements View.OnClickListener, CategoryAddedListAdapter.CategoryAddedListener {
    
    private static final String TAG = "UserAddCategoryActivity";
    
    RecyclerView rvCurrentCategory,rvAdditionalCategory;
    CategoryAddedListAdapter categoryAddedListAdapter, categoryAddedListAdapter2;
    ArrayList<CategoryItem> categoryItems, categoryItems2;
    
    ImageView ivBackpress;
    TextView tvEdit,tvSave;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        setContentView(R.layout.activity_user_category_edit);
        
        ivBackpress = (ImageView) findViewById(R.id.ivBackpress);
        tvEdit = (TextView) findViewById(R.id.tvEdit);
        tvSave = (TextView) findViewById(R.id.tvSave);
    
        ivBackpress.setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        tvSave.setOnClickListener(this);
    
        rvCurrentCategory = (RecyclerView) findViewById(R.id.rvCurrentCategory);
        rvAdditionalCategory = (RecyclerView) findViewById(R.id.rvAdditionalCategory);
    
        categoryItems = new ArrayList<CategoryItem>();
        categoryItems2 = new ArrayList<CategoryItem>();
        Globals.CategoryItems(this,categoryItems, R.array.str_res, 4,0);
    
        Globals.CategoryItems(this,categoryItems2, R.array.str_res, 15, 4);
    
        GridLayoutManager layoutManager = new GridLayoutManager(this,3);
        GridLayoutManager layoutManager2 = new GridLayoutManager(this,3);
    
        categoryAddedListAdapter = new CategoryAddedListAdapter(this,categoryItems);
        rvCurrentCategory.setLayoutManager(layoutManager);
        rvCurrentCategory.setHasFixedSize(true);
        rvCurrentCategory.setItemAnimator(new DefaultItemAnimator());
        rvCurrentCategory.setAdapter(categoryAddedListAdapter);
        categoryAddedListAdapter.notifyDataSetChanged();
    
        categoryAddedListAdapter2 = new CategoryAddedListAdapter(this,categoryItems2,this, 1);
        rvAdditionalCategory.setLayoutManager(layoutManager2);
        rvAdditionalCategory.setHasFixedSize(true);
        rvAdditionalCategory.setItemAnimator(new DefaultItemAnimator());
        rvAdditionalCategory.setAdapter(categoryAddedListAdapter2);
        categoryAddedListAdapter2.notifyDataSetChanged();
    
    }
    
    @Override
    public void onAddedCategory(int position) {
        
        if (categoryAddedListAdapter2.getSelectedItemCount() > 0) {
        
        } else {
            CategoryItem categoryItem2 = categoryItems2.get(position);
            
            categoryItems.add(new CategoryItem(categoryItem2.getCategoryName(),categoryItem2.getCategoryImgrsc()));
            categoryAddedListAdapter.notifyItemInserted(categoryItems.size());
            
            categoryItems2.remove(position);
            
            categoryAddedListAdapter2.notifyItemRemoved(position);
            categoryAddedListAdapter2.notifyItemRangeChanged(position,categoryItems2.size());
    
            Log.d(TAG, "new size of category "+categoryItems2.size());
            
        }
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackpress:
                onBackPressed();
                break;
            case R.id.tvEdit:
                UserEditCategoryActivity.open(UserAddCategoryActivity.this);
                break;
            case R.id.tvSave:
                break;
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
    
    public static void open(Activity activity){
        Intent intent = new Intent(activity,UserAddCategoryActivity.class);
        activity.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        activity.startActivity(intent);
    }
    
}